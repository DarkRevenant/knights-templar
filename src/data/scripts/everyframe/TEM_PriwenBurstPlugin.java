package data.scripts.everyframe;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.input.InputEventType;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.hullmods.TEM_ExcaliburDrive;
import data.scripts.shipsystems.TEM_HeavySchismDriveStats;
import data.scripts.shipsystems.TEM_HolyChargeStats;
import data.scripts.util.TEM_AnamorphicFlare;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.dark.shaders.distortion.WaveDistortion;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.json.JSONException;
import org.json.JSONObject;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_PriwenBurstPlugin extends BaseEveryFrameCombatPlugin {

    public static final float FLUX_COST = 0.15f;
    public static int PRIWEN_BUTTON = 1;
    public static int PRIWEN_KEY = 48;
    public static boolean PRIWEN_MOUSE = true;

    private static final Color ACTIVE = new Color(51, 255, 255);
    private static final Color COLOR1 = new Color(150, 200, 255);
    private static final Color COLOR2 = new Color(50, 200, 255);
    private static final Color COLOR3 = new Color(50, 200, 255, 100);
    private static final Color COLOR4 = new Color(100, 200, 255, 150);
    private static final Color COLOR5 = new Color(65, 205, 255);
    private static final Color COLOR6 = new Color(100, 200, 255);
    private static final Color COLOR7 = new Color(100, 255, 255);
    private static final Color COLOR8 = new Color(255, 255, 255);
    private static final Color COLOR_JITTER = new Color(50, 200, 255, 35);
    private static final Color COOLDOWN;
    private static final String DATA_KEY = "TEM_PriwenBurst";
    private static final Map<HullSize, Float> PITCH_BEND = new HashMap<>(4);
    private static final Color READY = new Color(0, 255, 255, 217);
    private static final String SETTINGS_FILE = "TEMPLAR_OPTIONS.ini";

    private static final Vector2f ZERO = new Vector2f();

    static {
        COOLDOWN = Global.getSettings().getColor("textEnemyColor");
    }

    static {
        PITCH_BEND.put(HullSize.FRIGATE, 1f);
        PITCH_BEND.put(HullSize.DESTROYER, 0.9f);
        PITCH_BEND.put(HullSize.CRUISER, 0.825f);
        PITCH_BEND.put(HullSize.CAPITAL_SHIP, 0.775f);
    }

    public static void activate(ShipAPI ship) {
        float scale = TEM_ExcaliburDrive.getDModScale(ship);
        if (scale <= 0f) {
            return;
        }

        if (TEM_PriwenBurstPlugin.cooldownTime(ship) <= 0f && (ship.getCurrentCR() > 0f || ship.isFighter())) {
            TEM_PriwenBurstPlugin.addBursting(ship);
        }
    }

    public static void addBursting(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return;
        }

        final Map<ShipAPI, PriwenBurstData> bursting = localData.bursting;

        bursting.put(ship, new PriwenBurstData());
    }

    public static float chargeLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> chargingLevel = localData.chargingLevel;

        if (chargingLevel.containsKey(ship)) {
            return chargingLevel.get(ship);
        } else {
            return 0f;
        }
    }

    public static float cooldownTime(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;

        if (coolingDown.containsKey(ship)) {
            return Global.getCombatEngine().getTotalElapsedTime(false) - coolingDown.get(ship);
        } else {
            return 0f;
        }
    }

    public static float effectLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, PriwenBurstData> bursting = localData.bursting;

        if (bursting.containsKey(ship)) {
            return bursting.get(ship).currentLevel;
        } else {
            return 0f;
        }
    }

    public static float getExpectedArea(ShipAPI ship) {
        float powerLevel = ship.getFluxTracker().getFluxLevel() + 0.5f * ship.getFluxTracker().getHardFlux()
                / ship.getFluxTracker().getMaxFlux();
        return (float) Math.sqrt(powerLevel) * (float) Math.sqrt(ship.getCollisionRadius()) * 70f;
    }

    public static float getExpectedDamage(ShipAPI ship, float distance) {
        float falloff = Math.max(0f, 1f - distance / getExpectedArea(ship));
        float powerLevel = ship.getFluxTracker().getFluxLevel() + 0.5f * ship.getFluxTracker().getHardFlux()
                / ship.getFluxTracker().getMaxFlux();
        return falloff * powerLevel * 12f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());
    }

    public static float getExpectedFlux(ShipAPI ship, float distance) {
        float falloff = Math.max(0f, 1f - distance / getExpectedArea(ship));
        float powerLevel = ship.getFluxTracker().getFluxLevel() + 0.5f * ship.getFluxTracker().getHardFlux()
                / ship.getFluxTracker().getMaxFlux();
        return falloff * powerLevel * 12f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());
    }

    public static float getExpectedInvulnerabilityTime(ShipAPI ship) {
        float powerLevel = ship.getFluxTracker().getFluxLevel() + 0.5f * ship.getFluxTracker().getHardFlux()
                / ship.getFluxTracker().getMaxFlux();
        return (float) Math.sqrt(powerLevel / 2f) / PITCH_BEND.get(ship.getHullSize());
    }

    public static float powerLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> priwenLevel = localData.priwenLevel;

        if (priwenLevel.containsKey(ship)) {
            return priwenLevel.get(ship);
        } else {
            return 0f;
        }
    }

    public static void reloadSettings() throws IOException, JSONException {
        JSONObject settings = Global.getSettings().loadJSON(SETTINGS_FILE);

        PRIWEN_MOUSE = settings.getBoolean("priwenMouse");
        TEM_Util.OFFSCREEN = settings.getBoolean("drawOffscreenParticles");

        if (PRIWEN_MOUSE) {
            PRIWEN_BUTTON = settings.getInt("priwenButton");
        } else {
            PRIWEN_KEY = settings.getInt("priwenKey");
        }
    }

    public static void removeFromCooldown(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return;
        }

        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;
        final Map<ShipAPI, Float> trueCoolingDown = localData.trueCoolingDown;

        if (coolingDown.containsKey(ship)) {
            coolingDown.remove(ship);
        }
        if (trueCoolingDown.containsKey(ship)) {
            trueCoolingDown.remove(ship);
        }
    }

    public static float trueCooldownTime(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return -1f;
        }

        final Map<ShipAPI, Float> trueCoolingDown = localData.trueCoolingDown;

        if (trueCoolingDown.containsKey(ship)) {
            return Global.getCombatEngine().getTotalElapsedTime(false) - trueCoolingDown.get(ship);
        } else {
            return -1f;
        }
    }

    private CombatEngineAPI engine;
    private final IntervalUtil interval = new IntervalUtil(0.015f, 0.015f);
    private boolean activated = false;
    private final IntervalUtil inactiveInterval = new IntervalUtil(1f, 2f);

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        if (engine == null) {
            return;
        }

        if (!activated) {
            inactiveInterval.advance(amount);
            if (!inactiveInterval.intervalElapsed()) {
                return;
            }
        }

        processInput(events);

        if (engine.isUIShowingHUD()) {
            ShipAPI ship = engine.getPlayerShip();
            if (ship != null && engine.isEntityInPlay(ship)) {
                float scale = TEM_ExcaliburDrive.getDModScale(ship);
                if (scale > 0f) {
                    switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                        case "tem_jesuit":
                        case "tem_crusader":
                        case "tem_paladin":
                        case "tem_chevalier":
                        case "tem_archbishop":
                        case "tem_boss_paladin":
                        case "tem_boss_archbishop": {
                            Color statusColor;
                            float fill;
                            if (TEM_PriwenBurstPlugin.chargeLevel(ship) > 0f
                                    && TEM_PriwenBurstPlugin.trueCooldownTime(ship) <= 0f) {
                                statusColor = ACTIVE;
                                fill = 1f;
                            } else if (TEM_PriwenBurstPlugin.cooldownTime(ship) > 0f) {
                                statusColor = COOLDOWN;
                                if (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_boss_paladin")) {
                                    fill = TEM_PriwenBurstPlugin.cooldownTime(ship) / 4.3f;
                                } else {
                                    fill = TEM_PriwenBurstPlugin.cooldownTime(ship) / 9.3f;
                                }
                            } else {
                                statusColor = READY;
                                fill = 1f;
                            }
                            TEM_Util.drawSystemUI(ship, statusColor, fill);
                            activated = true;
                            break;
                        }
                        case "tem_martyr":
                        case "tem_teuton": {
                            Color statusColor;
                            float fill;
                            if (TEM_AlmaceBurstPlugin.chargeLevel(ship) > 0f
                                    && TEM_AlmaceBurstPlugin.trueCooldownTime(ship) <= 0f) {
                                statusColor = ACTIVE;
                                fill = 1f;
                            } else if (TEM_AlmaceBurstPlugin.cooldownTime(ship) > 0f) {
                                statusColor = COOLDOWN;
                                fill = TEM_AlmaceBurstPlugin.cooldownTime(ship) / 9.3f;
                            } else {
                                statusColor = READY;
                                fill = 1f;
                            }
                            TEM_Util.drawSystemUI(ship, statusColor, fill);
                            activated = true;
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
        }

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, PriwenBurstData> bursting = localData.bursting;
        final Map<ShipAPI, Float> chargingLevel = localData.chargingLevel;
        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;
        final Map<ShipAPI, Float> priwenLevel = localData.priwenLevel;
        final Map<ShipAPI, Float> trueCoolingDown = localData.trueCoolingDown;
        final Map<ShipAPI, Object> uiKey = localData.uiKey;

        interval.advance(amount);
        boolean intervalElapsed = interval.intervalElapsed();

        Iterator<Map.Entry<ShipAPI, PriwenBurstData>> iter = bursting.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<ShipAPI, PriwenBurstData> entry = iter.next();
            ShipAPI ship = entry.getKey();
            PriwenBurstData data = entry.getValue();

            activated = true;

            float scale = TEM_ExcaliburDrive.getDModScale(ship);
            if (scale <= 0f) {
                continue;
            }

            float shipRadius = TEM_Util.effectiveRadius(ship);

            if (ship == engine.getPlayerShip()) {
                if (!uiKey.containsKey(ship)) {
                    uiKey.put(ship, new Object());
                }
                engine.maintainStatusForPlayerShip(uiKey.get(ship), "graphics/icons/tactical/overloaded2.png",
                        "Priwen Burst", "Smiting nearby hostile ships", false);
            }

            if (engine.isPaused()) {
                continue;
            }

            if (data.chargeLevel < 1f && !data.done) {
                if (!data.started) {
                    if (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_boss_paladin")) {
                        ship.getFluxTracker().increaseFlux(
                                ship.getMutableStats().getPhaseCloakActivationCostBonus().computeEffective(
                                        ship.getMutableStats().getFluxCapacity().getBaseValue() * FLUX_COST * 0.5f),
                                true);
                    } else {
                        ship.getFluxTracker().increaseFlux(
                                ship.getMutableStats().getPhaseCloakActivationCostBonus().computeEffective(
                                        ship.getMutableStats().getFluxCapacity().getBaseValue() * FLUX_COST), true);
                    }
                    if (ship.getFluxTracker().isOverloadedOrVenting()) {
                        iter.remove();
                        continue;
                    }
                    data.powerLevel = (ship.getFluxTracker().getFluxLevel() + 0.5f * ship.getFluxTracker().getHardFlux() / ship.getFluxTracker().getMaxFlux()) * scale;
                    StandardLight light = new StandardLight(ZERO, ZERO, ZERO, ship);
                    light.setColor(COLOR1);
                    light.setSize(data.powerLevel * shipRadius * 4f);
                    light.setIntensity(data.powerLevel);
                    light.fadeIn((float) Math.sqrt(data.powerLevel / 2f) / PITCH_BEND.get(ship.getHullSize()));
                    light.setLifetime(0f);
                    LightShader.addLight(light);
                    float time = (float) Math.sqrt(data.powerLevel / 2f) / PITCH_BEND.get(ship.getHullSize());
                    if (time <= 0.4f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_activate_400ms", 0.4f / time, 1f,
                                ship.getLocation(), ship.getVelocity());
                    } else if (time <= 0.6f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_activate_600ms", 0.6f / time, 1f,
                                ship.getLocation(), ship.getVelocity());
                    } else if (time <= 0.9f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_activate_900ms", 0.9f / time, 1f,
                                ship.getLocation(), ship.getVelocity());
                    } else {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_activate_1200ms", 1.2f / time, 1f,
                                ship.getLocation(), ship.getVelocity());
                    }
                    coolingDown.put(ship, engine.getTotalElapsedTime(false));
                    priwenLevel.put(ship, data.powerLevel);
                }

                float apparentPower = data.powerLevel * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux() + 5000f);

                ship.setCollisionClass(CollisionClass.NONE);
                float chargingTime = (float) Math.sqrt(data.powerLevel / 2f) / PITCH_BEND.get(ship.getHullSize());
                data.chargeLevel = Math.min(data.chargeLevel + amount
                        * ship.getMutableStats().getTimeMult().getModifiedValue() / chargingTime, 1f);
                chargingLevel.put(ship, data.chargeLevel);

                if (intervalElapsed) {
                    if (apparentPower >= 50f) {
                        if (TEM_Util.isOnscreen(ship.getLocation(),
                                (chargingTime * ship.getCollisionRadius() * (1f + data.powerLevel)
                                + 2f * ship.getCollisionRadius() * (1f + data.powerLevel)))) {
                            for (int i = 0;
                                    i <= (int) (data.powerLevel * interval.getIntervalDuration()
                                    * ship.getMutableStats().getTimeMult().getModifiedValue()
                                    * ship.getCollisionRadius() * 3f);
                                    i++) {
                                float radius = 2f * shipRadius * (1f + data.powerLevel)
                                        * ((float) Math.random() * 0.5f + 0.5f);
                                Vector2f direction = MathUtils.getRandomPointOnCircumference(null,
                                        shipRadius
                                        * (1f + data.powerLevel));
                                Vector2f point = MathUtils.getPointOnCircumference(ship.getLocation(), radius,
                                        VectorUtils.getFacing(direction));
                                direction.scale(-1.25f);
                                Vector2f.add(ship.getVelocity(), direction, direction);
                                engine.addSmoothParticle(point, direction, 10f * (float) Math.sqrt(data.powerLevel), 1f,
                                        chargingTime, COLOR2);
                            }
                        }
                    }
                    if (apparentPower >= 150f) {
                        for (int i = 0; i <= data.powerLevel * ship.getCollisionRadius() * 0.01f; i++) {
                            if (Math.random() > 0.5) {
                                Vector2f point1 = MathUtils.getRandomPointInCircle(
                                        ship.getLocation(),
                                        (float) Math.random() * data.chargeLevel * shipRadius
                                        * (0.4f + data.powerLevel) * 3f);
                                Vector2f point2 = MathUtils.getRandomPointInCircle(ship.getLocation(),
                                        ship.getCollisionRadius());
                                int bound = 100;
                                while (bound > 0) {
                                    bound--;
                                    point2 = MathUtils.getRandomPointInCircle(ship.getLocation(),
                                            ship.getCollisionRadius());
                                    if (CollisionUtils.isPointWithinBounds(point2, ship)) {
                                        break;
                                    }
                                }
                                engine.spawnEmpArc(ship, point2, new SimpleEntity(point2), new SimpleEntity(point1),
                                        DamageType.ENERGY, 0f, 0f, 1000f, null,
                                        data.powerLevel * (data.chargeLevel * 20f + 10f), COLOR3, COLOR4);
                            }
                        }
                    }
                }

                float alphaMult;
                if (data.chargeLevel < 0.3f) {
                    alphaMult = 1f - data.chargeLevel * 2f;
                } else if (data.chargeLevel < 0.7f) {
                    alphaMult = 0.4f;
                } else {
                    alphaMult = 0.4f + (data.chargeLevel - 0.7f) * 2f;
                }
                float jitterLevel = 1.25f / alphaMult;
                ship.setExtraAlphaMult(alphaMult);
                ship.setJitter(this, COLOR_JITTER, data.powerLevel * jitterLevel,
                        Math.max(2, Math.round(data.powerLevel * jitterLevel * 4f)),
                        (float) Math.sqrt(jitterLevel) * shipRadius * 0.02f,
                        jitterLevel * shipRadius * 0.04f);

                if (!data.started) {
                    data.started = true;
                    if (apparentPower >= 200f) {
                        WaveDistortion wave = new WaveDistortion(ship.getLocation(), ZERO);
                        wave.setSize(shipRadius * (0.4f + data.powerLevel));
                        wave.setIntensity(shipRadius * (0.4f + data.powerLevel) * 0.1f);
                        wave.flip(true);
                        wave.fadeInSize(chargingTime);
                        wave.fadeInIntensity(chargingTime);
                        wave.setLifetime(0f);
                        wave.setAutoFadeIntensityTime(chargingTime / 3f);
                        wave.setAutoFadeSizeTime(chargingTime / 3f);
                        DistortionShader.addDistortion(wave);
                    }
                }
            } else {
                float unchargingTime = (float) Math.sqrt(Math.sqrt(data.powerLevel / 2f)) / PITCH_BEND.get(
                        ship.getHullSize());
                float area = (float) Math.sqrt(data.powerLevel) * (float) Math.sqrt(ship.getCollisionRadius()) * 70f;
                float damage = data.powerLevel * 12f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());
                float maxDamage = damage * 5f;
                float emp = data.powerLevel * 4f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());
                float flux = data.powerLevel * 12f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());
                float force = data.powerLevel * 6f * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux());

                float apparentPower = data.powerLevel * (float) Math.sqrt(ship.getFluxTracker().getMaxFlux() + 5000f);

                data.chargeLevel = Math.max(data.chargeLevel - amount
                        * ship.getMutableStats().getTimeMult().getModifiedValue() / unchargingTime, 0f);
                chargingLevel.put(ship, data.chargeLevel);

                if (intervalElapsed) {
                    float expansionLevel = 1f - ((1f - data.chargeLevel) * 2f);
                    if (expansionLevel > 0f && apparentPower >= 150f) {
                        for (int i = 0; i <= data.powerLevel * (float) Math.sqrt(ship.getCollisionRadius()) * 0.2f; i++) {
                            float angle = (float) Math.random() * 360f;
                            float distance = (float) Math.random() * area * 0.3f * (1f - expansionLevel)
                                    + area * 0.85f * (1f - expansionLevel);
                            Vector2f point1 = MathUtils.getPointOnCircumference(ship.getLocation(), distance, angle);
                            Vector2f point2 = MathUtils.getPointOnCircumference(ship.getLocation(), distance, angle
                                    + 45f * (float) Math.random());
                            engine.spawnEmpArc(ship, point1, new SimpleEntity(point1), new SimpleEntity(point2),
                                    DamageType.ENERGY, 0f, 0f, 1000f, null,
                                    data.powerLevel * (data.chargeLevel * 20f + 20f), COLOR5, COLOR6);
                        }
                        for (int i = 0; i <= data.powerLevel * ship.getCollisionRadius() * 0.01f; i++) {
                            if (Math.random() > 0.5) {
                                Vector2f point1 = MathUtils.getRandomPointInCircle(ship.getLocation(),
                                        (float) Math.random() * area * 0.3f
                                        * (1f - expansionLevel)
                                        + area * 0.85f * (1f - expansionLevel));
                                Vector2f point2 = MathUtils.getRandomPointInCircle(ship.getLocation(),
                                        ship.getCollisionRadius());
                                int bound = 100;
                                while (bound > 0) {
                                    bound--;
                                    point2 = MathUtils.getRandomPointInCircle(ship.getLocation(),
                                            ship.getCollisionRadius());
                                    if (CollisionUtils.isPointWithinBounds(point2, ship)) {
                                        break;
                                    }
                                }
                                engine.spawnEmpArc(ship, point2, new SimpleEntity(point2), new SimpleEntity(point1),
                                        DamageType.ENERGY, 0f, 0f, 1000f, null,
                                        data.powerLevel * (data.chargeLevel * 20f + 20f), COLOR5, COLOR6);
                            }
                        }
                    }
                }

                if (!data.done) {
                    if (((ship.getSystem() == null || !ship.getSystem().isActive())
                            && (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_jesuit")
                            || TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_boss_paladin")))
                            || (!TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_jesuit")
                            && !TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_boss_paladin"))) {
                        ship.setCollisionClass(CollisionClass.SHIP);
                    }
                    trueCoolingDown.put(ship, engine.getTotalElapsedTime(false));
                    data.done = true;
                    engine.spawnExplosion(ship.getLocation(), ZERO, new Color(50, 255, 255,
                            TEM_Util.clamp255((int) (100f * data.powerLevel))), area, 0.15f);
                    engine.addHitParticle(ship.getLocation(), ZERO, area * 2f, 10f, unchargingTime, COLOR5);
                    TEM_AnamorphicFlare.createStripFlare(ship, new Vector2f(ship.getLocation()), engine, 1f, (int) (data.powerLevel * 40f), 1f, data.powerLevel * 40f, 0f, 0f, 2f,
                            COLOR2, COLOR5, false);
                    if (apparentPower >= 50f) {
                        if (TEM_Util.isOnscreen(ship.getLocation(), ((float) Math.sqrt(data.powerLevel) * 0.5f
                                * ship.getCollisionRadius() * 10f
                                * (1f + data.powerLevel) + ship.getCollisionRadius()))) {
                            for (int i = 0; i <= (int) (data.powerLevel * ship.getCollisionRadius() * 1.5f); i++) {
                                float radius = shipRadius * (float) Math.random();
                                Vector2f direction = MathUtils.getRandomPointOnCircumference(null,
                                        shipRadius
                                        * ((float) Math.random() * 8f
                                        + 2f)
                                        * (1f + data.powerLevel));
                                Vector2f point = MathUtils.getPointOnCircumference(ship.getLocation(), radius,
                                        VectorUtils.getFacing(direction));
                                engine.addSmoothParticle(point, direction, 15f * (float) Math.sqrt(data.powerLevel), 1f,
                                        (float) Math.sqrt(data.powerLevel)
                                        * 0.5f, COLOR5);
                            }
                        }
                    }

                    float totalDamage = 0f;
                    List<ShipAPI> nearbyEnemies = CombatUtils.getShipsWithinRange(ship.getLocation(), area);

                    TEM_Util.filterObscuredTargets(null, ship.getLocation(), nearbyEnemies, false, true, false);

                    for (ShipAPI thisEnemy : nearbyEnemies) {
                        if (thisEnemy == ship
                                || ((thisEnemy.isFighter() || thisEnemy.isDrone())
                                && thisEnemy.getOwner() == ship.getOwner())) {
                            continue;
                        }

                        float contribution = 1f;
                        if (thisEnemy.isFighter() || thisEnemy.isDrone()) {
                            contribution *= 0.25f;
                        }
                        if (thisEnemy.getOwner() == ship.getOwner()) {
                            contribution *= 0.5f;
                        }

                        float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
                        if (thisEnemy.getOwner() == ship.getOwner()) {
                            falloff *= 0.25f;
                        }
                        if (thisEnemy.getCollisionClass() == CollisionClass.NONE) {
                            falloff *= 0.5f;
                        } else {
                            totalDamage += damage * falloff * contribution;
                        }

                        for (int i = 0; i <= (int) (damage * falloff / 125f); i++) {
                            totalDamage += damage * falloff * 0.25f * contribution;
                        }
                    }

                    for (DamagingProjectileAPI thisProj : engine.getProjectiles()) {
                        if (thisProj.getOwner() != ship.getOwner()) {
                            if (thisProj instanceof MissileAPI) {
                                float contribution = 0.1f;

                                float falloff = 1f - MathUtils.getDistance(ship, thisProj) / area;
                                if (thisProj.getCollisionClass() == CollisionClass.NONE) {
                                    falloff *= 0.5f;
                                }

                                totalDamage += Math.min(thisProj.getHitpoints(), damage * 0.25f * falloff)
                                        * contribution;
                            }
                        }
                    }

                    float attenuation = 1f;
                    if (totalDamage > maxDamage) {
                        attenuation *= maxDamage / totalDamage;
                    }
                    for (ShipAPI thisEnemy : nearbyEnemies) {
                        if (thisEnemy == ship
                                || ((thisEnemy.isFighter() || thisEnemy.isDrone())
                                && thisEnemy.getOwner() == ship.getOwner())) {
                            continue;
                        }

                        Vector2f projection = VectorUtils.getDirectionalVector(ship.getLocation(),
                                thisEnemy.getLocation());
                        projection.scale(thisEnemy.getCollisionRadius());
                        Vector2f.add(projection, thisEnemy.getLocation(), projection);
                        Vector2f damagePoint = CollisionUtils.getCollisionPoint(ship.getLocation(), projection,
                                thisEnemy);
                        if (damagePoint == null) {
                            damagePoint = ship.getLocation();
                        }
                        float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
                        if (thisEnemy.getOwner() == ship.getOwner()) {
                            falloff *= 0.25f;
                        }
                        if (thisEnemy.getCollisionClass() == CollisionClass.NONE) {
                            falloff *= 0.5f;
                        } else {
                            engine.applyDamage(thisEnemy, damagePoint, damage * falloff * attenuation,
                                    DamageType.KINETIC, emp * falloff * attenuation, false, false, ship, false);
                        }
                        if (thisEnemy.getShield() != null && thisEnemy.getShield().isWithinArc(damagePoint)) {
                            thisEnemy.getFluxTracker().increaseFlux(flux * falloff * attenuation, true);
                        } else {
                            thisEnemy.getFluxTracker().increaseFlux(flux * falloff * attenuation, false);
                        }

                        ShipAPI empTarget = thisEnemy;
                        for (int i = 0; i <= Math.round(attenuation * damage * falloff / 125f); i++) {
                            Vector2f point;
                            if (Math.random() > 0.5 || apparentPower >= 150f) {
                                point = MathUtils.getRandomPointInCircle(thisEnemy.getLocation(),
                                        TEM_Util.effectiveRadius(thisEnemy) * 1.5f);
                            } else {
                                point = MathUtils.getRandomPointInCircle(ship.getLocation(), shipRadius);
                            }
                            engine.spawnEmpArc(ship, point, empTarget, empTarget, DamageType.KINETIC, damage * falloff
                                    * 0.25f, emp * falloff * 0.25f, 10000f,
                                    null, data.powerLevel * (float) Math.sqrt(damage), COLOR7, COLOR8);
                        }

                        TEM_Util.applyForce(thisEnemy, VectorUtils.getDirectionalVector(
                                ship.getLocation(), thisEnemy.getLocation()), force * falloff * attenuation);
                    }

                    List<CombatEntityAPI> nearbyAsteroids
                            = CombatUtils.getAsteroidsWithinRange(ship.getLocation(), area);
                    for (CombatEntityAPI asteroid : nearbyAsteroids) {
                        TEM_Util.applyForce(asteroid, VectorUtils.getDirectionalVector(ship.getLocation(), asteroid.getLocation()),
                                force * (1f - MathUtils.getDistance(ship, asteroid) / area));
                        float falloff = 1f - MathUtils.getDistance(ship, asteroid) / area;
                        if (asteroid.getCollisionClass() == CollisionClass.NONE) {
                            falloff *= 0.5f;
                        }
                        engine.applyDamage(asteroid, asteroid.getLocation(), damage * 0.25f * falloff,
                                DamageType.KINETIC, emp * 0.25f * falloff, false,
                                false, ship, false);
                    }

                    int numRepulseParticles = 0;
                    for (DamagingProjectileAPI thisProj : engine.getProjectiles()) {
                        if (thisProj.getOwner() != ship.getOwner()) {
                            Vector2f thisProjLoc = thisProj.getLocation();
                            Vector2f thisProjVel = thisProj.getVelocity();
                            if (MathUtils.getDistanceSquared(ship.getLocation(), thisProjLoc) > (area * area)) {
                                continue;
                            }

                            if (numRepulseParticles < 20) {
                                numRepulseParticles++;
                                float scaleFactor;
                                if (thisProj.getDamageType() == DamageType.FRAGMENTATION) {
                                    scaleFactor = (float) Math.sqrt(thisProj.getDamageAmount() * 0.25f);
                                } else {
                                    scaleFactor = (float) Math.sqrt(thisProj.getDamageAmount() * 1f);
                                }

                                scaleFactor = Math.min(scaleFactor, 40f);

                                engine.addHitParticle(thisProjLoc, ship.getVelocity(), data.powerLevel * 3f
                                        * scaleFactor, 1f, unchargingTime, COLOR5);
                            }

                            float returnAngle = VectorUtils.getAngle(ship.getLocation(), thisProjLoc);
                            thisProjVel.set(MathUtils.getPointOnCircumference(null, thisProjVel.length(), returnAngle));

                            thisProj.setFacing(returnAngle);
                            if (!(thisProj instanceof MissileAPI)) {
                                thisProj.setOwner(ship.getOwner());
                                thisProj.setSource(ship);
                            } else {
                                float falloff = 1f - MathUtils.getDistance(ship, thisProj) / area;
                                if (thisProj.getCollisionClass() == CollisionClass.NONE) {
                                    falloff *= 0.5f;
                                }
                                engine.applyDamage(thisProj, thisProj.getLocation(), damage * 0.25f * falloff,
                                        DamageType.KINETIC, emp * 0.25f * falloff,
                                        false, false, ship, false);
                            }
                        }
                    }

                    StandardLight light = new StandardLight(ship.getLocation(), ZERO, ZERO, null);
                    light.setColor(COLOR7);
                    light.setSize(area * 1.5f);
                    light.setIntensity(data.powerLevel);
                    light.fadeOut(unchargingTime);
                    LightShader.addLight(light);

                    if (apparentPower >= 100f) {
                        RippleDistortion ripple = new RippleDistortion(ship.getLocation(), ZERO);
                        ripple.setSize(area);
                        ripple.setIntensity(area * 0.1f);
                        ripple.setFrameRate(60f / (unchargingTime));
                        ripple.fadeInSize(unchargingTime);
                        ripple.fadeOutIntensity(unchargingTime);
                        DistortionShader.addDistortion(ripple);
                    }

                    if (apparentPower < 50f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_blast_weak", PITCH_BEND.get(
                                ship.getHullSize()), 1f, ship.getLocation(),
                                ZERO);
                    } else if (apparentPower < 100f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_blast_mid", (PITCH_BEND.get(
                                ship.getHullSize()) + 0.5f)
                                / 1.5f, 1f,
                                ship.getLocation(), ZERO);
                    } else if (apparentPower < 150f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_blast_strong", (PITCH_BEND.get(
                                ship.getHullSize())
                                + 1f) / 2f, 1f,
                                ship.getLocation(), ZERO);
                    } else if (apparentPower < 200f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_blast_ultra", (PITCH_BEND.get(
                                ship.getHullSize())
                                + 1.5f) / 2.5f, 1f,
                                ship.getLocation(), ZERO);
                    } else {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_blast_mega", (PITCH_BEND.get(
                                ship.getHullSize()) + 2f)
                                / 3f, 1f,
                                ship.getLocation(), ZERO);
                    }

                    if (apparentPower >= 75f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_underlay_short", PITCH_BEND.get(
                                ship.getHullSize()), data.powerLevel,
                                ship.getLocation(), ZERO);
                    } else if (apparentPower >= 125f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_underlay_med", PITCH_BEND.get(
                                ship.getHullSize()), data.powerLevel,
                                ship.getLocation(), ZERO);
                    } else if (apparentPower >= 175f) {
                        Global.getSoundPlayer().playSound("tem_priwenburstshield_underlay_long", PITCH_BEND.get(
                                ship.getHullSize()), data.powerLevel,
                                ship.getLocation(), ZERO);
                    }
                }
            }

            if (data.chargeLevel <= 0f) {
                if (((ship.getSystem() == null || !ship.getSystem().isActive())
                        && (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_jesuit")
                        || TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_boss_paladin")))
                        || (!TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_jesuit")
                        && !TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_boss_paladin"))) {
                    ship.setCollisionClass(CollisionClass.SHIP);
                }
                priwenLevel.remove(ship);
                chargingLevel.remove(ship);
                uiKey.remove(ship);
                iter.remove();
            } else {
                data.currentLevel = Math.min(data.chargeLevel * data.powerLevel, 1f);
            }
        }
    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
        Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
    }

    @Override
    public void renderInWorldCoords(ViewportAPI viewport) {
        if (engine == null) {
            return;
        }

        List<ShipAPI> ships = engine.getShips();
        int size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.isAlive()) {
                continue;
            }

            if (ship.getHullSpec().getHullId().startsWith("tem_")) {
                float cooldown;
                switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                    case "tem_teuton":
                    case "tem_martyr":
                        cooldown = TEM_AlmaceBurstPlugin.cooldownTime(ship);
                        if (cooldown >= 9.3f) {
                            TEM_AlmaceBurstPlugin.removeFromCooldown(ship);
                        }
                        break;
                    case "tem_boss_paladin":
                        cooldown = TEM_PriwenBurstPlugin.cooldownTime(ship);
                        if (cooldown >= 4.3f) {
                            TEM_PriwenBurstPlugin.removeFromCooldown(ship);
                        }
                        break;
                    default:
                        cooldown = TEM_PriwenBurstPlugin.cooldownTime(ship);
                        if (cooldown >= 9.3f) {
                            TEM_PriwenBurstPlugin.removeFromCooldown(ship);
                        }
                        break;
                }

                switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                    case "tem_paladin":
                        cooldown = TEM_HolyChargeStats.cooldownTime(ship);
                        if (cooldown >= 27f) {
                            TEM_HolyChargeStats.removeFromCooldown(ship);
                        }
                        break;
                    case "tem_boss_paladin":
                        cooldown = TEM_HeavySchismDriveStats.cooldownTime(ship);
                        if (cooldown >= 15f) {
                            TEM_HeavySchismDriveStats.removeFromCooldown(ship);
                        }
                        break;
                }
            }
        }
    }

    private void processInput(List<InputEventAPI> events) {
        if (engine.getCombatUI() == null) {
            return;
        }

        if (engine.getCombatUI().isShowingCommandUI()) {
            return;
        }

        ShipAPI player = engine.getPlayerShip();
        if (player == null || !engine.isEntityInPlay(player)) {
            return;
        }

        float scale = TEM_ExcaliburDrive.getDModScale(player);
        if (scale <= 0f) {
            return;
        }

        switch (TEM_Util.getNonDHullId(player.getHullSpec())) {
            case "tem_jesuit":
            case "tem_crusader":
            case "tem_paladin":
            case "tem_chevalier":
            case "tem_archbishop":
            case "tem_boss_paladin":
            case "tem_boss_archbishop":
                break;
            default:
                return;
        }

        activated = true;

        for (InputEventAPI event : events) {
            if (event.isConsumed()) {
                continue;
            }

            if (PRIWEN_MOUSE) {
                if (event.getEventType() == InputEventType.MOUSE_DOWN) {
                    if (event.getEventValue() == PRIWEN_BUTTON) {
                        event.consume();
                        activate(player);
                    }
                }
            } else {
                if (event.getEventType() == InputEventType.KEY_DOWN) {
                    if (event.getEventValue() == PRIWEN_KEY) {
                        event.consume();
                        activate(player);
                    }
                }
            }
        }
    }

    private static final class LocalData {

        final Map<ShipAPI, PriwenBurstData> bursting = new LinkedHashMap<>(50);
        final Map<ShipAPI, Float> chargingLevel = new HashMap<>(50);
        final Map<ShipAPI, Float> coolingDown = new HashMap<>(50);
        final Map<ShipAPI, Float> priwenLevel = new HashMap<>(50);
        final Map<ShipAPI, Float> trueCoolingDown = new HashMap<>(50);
        final Map<ShipAPI, Object> uiKey = new HashMap<>(50);
    }

    private static final class PriwenBurstData {

        float chargeLevel = 0f;
        float currentLevel = 0f;
        boolean done = false;
        float powerLevel = 0f;
        boolean started = false;
    }
}
