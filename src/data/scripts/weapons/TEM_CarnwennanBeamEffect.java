package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_CarnwennanBeamEffect implements BeamEffectPlugin {

    private static final Color COLOR_MAIN = new Color(75, 210, 255);
    private static final float MUZZLE_OFFSET = 0.0f;

    private final IntervalUtil interval = new IntervalUtil(0.015f, 0.015f);

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        Vector2f origin = new Vector2f(beam.getWeapon().getLocation());
        Vector2f offset = new Vector2f(MUZZLE_OFFSET, 0f);
        VectorUtils.rotate(offset, beam.getWeapon().getCurrAngle(), offset);
        Vector2f.add(offset, origin, origin);

        float level = beam.getBrightness();

        interval.advance(amount);
        if (interval.intervalElapsed()) {
            Global.getCombatEngine().addHitParticle(origin, beam.getSource().getVelocity(), level * 20f, 0.5f, 0.2f,
                                                    COLOR_MAIN);
        }
    }
}
