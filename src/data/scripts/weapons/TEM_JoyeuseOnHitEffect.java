package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatAsteroidAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.util.TEM_AnamorphicFlare;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_JoyeuseOnHitEffect implements OnHitEffectPlugin {

    private static final int NUM_PARTICLES = 20;
    private static final Color PARTICLE_COLOR = new Color(255, 50, 50, 255);
    private static final Vector2f ZERO = new Vector2f();

    public static void explode(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, CombatEngineAPI engine) {
        if (point == null) {
            return;
        }
        engine.spawnExplosion(point, ZERO, PARTICLE_COLOR, 400f, 0.1f);
        engine.addSmoothParticle(point, ZERO, 500f, 1f, 0.75f, PARTICLE_COLOR);
        float speed = projectile.getVelocity().length();
        for (int x = 0; x < NUM_PARTICLES; x++) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null, MathUtils.getRandomNumberInRange(speed * 0.15f, speed * 0.4f),
                    (float) Math.random() * 360f), 8f, 1f, MathUtils.getRandomNumberInRange(0.1f, 0.3f), PARTICLE_COLOR);
        }

        if (projectile.getSource() != null) {
            TEM_AnamorphicFlare.createStripFlare(projectile.getSource(), new Vector2f(projectile.getLocation()), engine, 1f, 10, 0.4f, 10f, projectile.getFacing(), 30f, 1f, PARTICLE_COLOR, PARTICLE_COLOR, true);
        }

        Global.getSoundPlayer().playSound("tem_joyeuse_impact", 1f, 1f, point, ZERO);
    }

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (target instanceof ShipAPI || target instanceof CombatAsteroidAPI) {
            explode(projectile, target, point, engine);
        }
    }
}
