package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatAsteroidAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.everyframe.TEM_WeaponScriptPlugin;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.List;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_JugerOnHitEffect implements OnHitEffectPlugin {

    private static final float AREA_DAMAGE = 2000f;
    private static final float AREA_EFFECT = 250f;
    private static final float AREA_EFFECT_INNER = 150f;
    private static final int NUM_PARTICLES = 50;
    private static final Color PARTICLE_COLOR = new Color(246, 238, 139, 200);
    private static final Vector2f ZERO = new Vector2f();

    public static void explode(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, CombatEngineAPI engine) {
        if (point == null) {
            return;
        }
        engine.spawnExplosion(point, ZERO, PARTICLE_COLOR, 450f, 0.15f);
        engine.addSmoothParticle(point, ZERO, 500f, 1f, 0.6f, PARTICLE_COLOR);
        float speed = projectile.getVelocity().length();
        for (int x = 0; x < NUM_PARTICLES; x++) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null, MathUtils.getRandomNumberInRange(speed * 0.25f, speed),
                    (float) Math.random() * 360f), 7f, 1f, MathUtils.getRandomNumberInRange(0.5f, 1.5f), PARTICLE_COLOR);
        }
//        for (int x = 0; x < NUM_PARTICLES; x++) {
//            engine.spawnExplosion(point, MathUtils.getPointOnCircumference(null, MathUtils.getRandomNumberInRange(
//                    speed * 0.25f, speed * 0.5f),
//                    (float) Math.random() * 360f), PARTICLE_COLOR,
//                    100f,
//                    MathUtils.getRandomNumberInRange(0.5f, 1f));
//        }

        List<ShipAPI> targets = CombatUtils.getShipsWithinRange(point, AREA_EFFECT);
        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            targets.remove(ship);
        }

        TEM_Util.filterObscuredTargets(target, point, targets, true, true, false);

        for (ShipAPI tgt : targets) {
            float distance = TEM_Util.getActualDistance(point, tgt, true);
            float reduction = 1f;
            if (distance > AREA_EFFECT_INNER) {
                reduction = (AREA_EFFECT - distance) / (AREA_EFFECT - AREA_EFFECT_INNER);
            }

            if (reduction <= 0f) {
                continue;
            }

            boolean shieldHit = false;
            if (tgt.getShield() != null && tgt.getShield().isWithinArc(point)) {
                shieldHit = true;
            }

            Vector2f damagePoint;
            if (shieldHit) {
                damagePoint = MathUtils.getPointOnCircumference(null, tgt.getShield().getRadius(), VectorUtils.getAngle(tgt.getShield().getLocation(), point));
                Vector2f.add(damagePoint, tgt.getLocation(), damagePoint);
            } else {
                Vector2f projection = VectorUtils.getDirectionalVector(point, tgt.getLocation());
                projection.scale(tgt.getCollisionRadius());
                Vector2f.add(projection, tgt.getLocation(), projection);
                damagePoint = CollisionUtils.getCollisionPoint(point, projection, tgt);
            }
            if (damagePoint == null) {
                damagePoint = point;
            }
            engine.applyDamage(projectile, tgt, damagePoint, AREA_DAMAGE * reduction, DamageType.ENERGY, 0f, false, false, projectile.getSource(), false);
        }

        if (target != null) {
            TEM_WeaponScriptPlugin.genJugerBlast(point, new Vector2f(target.getVelocity().x * 0.45f, target.getVelocity().y * 0.45f));
        } else {
            TEM_WeaponScriptPlugin.genJugerBlast(point, ZERO);
        }

        Global.getSoundPlayer().playSound("tem_juger_impact", 1f, 1f, point, ZERO);
    }

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (target instanceof ShipAPI || target instanceof CombatAsteroidAPI) {
            if (point == null) {
                return;
            }
            explode(projectile, target, point, engine);
        }
    }
}
