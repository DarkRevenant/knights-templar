package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_LonginusBeamEffect implements BeamEffectPlugin {

    private static final Color COLOR = new Color(255, 25, 25, 255);
    private static final float MUZZLE_OFFSET_HARDPOINT = 4.0f;
    private static final float MUZZLE_OFFSET_TURRET = 7.0f;
    private static final Vector2f ZERO = new Vector2f();
    private final IntervalUtil interval = new IntervalUtil(0.1f, 0.1f);
    private final IntervalUtil interval2 = new IntervalUtil(0.015f, 0.015f);

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        Vector2f origin = new Vector2f(beam.getWeapon().getLocation());
        Vector2f offset = beam.getWeapon().getSlot().isHardpoint() ? new Vector2f(MUZZLE_OFFSET_HARDPOINT, 0f) :
                          new Vector2f(MUZZLE_OFFSET_TURRET, 0f);
        VectorUtils.rotate(offset, beam.getWeapon().getCurrAngle(), offset);
        Vector2f.add(offset, origin, origin);

        float level = beam.getBrightness();

        interval2.advance(amount);
        if (interval2.intervalElapsed()) {
            Global.getCombatEngine().addHitParticle(origin, ZERO, level * 100f, 0.2f,
                                                    6f * interval2.getIntervalDuration(), COLOR);
        }

        interval.advance(amount);
        if (interval.intervalElapsed()) {
            if (beam.getDamageTarget() != null) {
                Global.getCombatEngine().spawnExplosion(new Vector2f(beam.getTo()), ZERO,
                                                        new Color(255,
                                                                  50 + (int) ((float) Math.random() * 50f),
                                                                  50 + (int) ((float) Math.random() * 25f),
                                                                  100),
                                                        level * 40f, 0.2f);
                for (int x = 0; x < 6; x++) {
                    float angle = VectorUtils.getAngle(beam.getTo(), beam.getDamageTarget().getLocation()) + 180f +
                          (float) Math.random() * 210f - 105f;
                    if (angle >= 360f) {
                        angle -= 360f;
                    } else if (angle < 0f) {
                        angle += 360f;
                    }
                    engine.addHitParticle(new Vector2f(beam.getTo()),
                                          MathUtils.getPointOnCircumference(
                                                  null, (float) Math.random() * 250f + 250f, angle), 10f,
                                          1f, (float) Math.random() * 0.2f + 0.2f,
                                          new Color(255,
                                                    50 + (int) ((float) Math.random() * 50f),
                                                    50 + (int) ((float) Math.random() * 25f),
                                                    255));
                }
            }
        }
    }
}
