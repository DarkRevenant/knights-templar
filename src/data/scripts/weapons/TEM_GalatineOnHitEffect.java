package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatAsteroidAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.everyframe.TEM_WeaponScriptPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.List;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_GalatineOnHitEffect implements OnHitEffectPlugin {

    private static final float AREA_EFFECT = 300f;
    private static final float AREA_EFFECT_INNER = 150f;
    private static final float AREA_EMP = 2000f;
    private static final float AREA_FLUX_DAMAGE = 500f;
    private static final Color COLOR1 = new Color(75, 225, 255);
    private static final Color COLOR2 = new Color(200, 225, 255);
    private static final Color COLOR3 = new Color(25, 100, 155, 255);
    private static final Color COLOR4 = new Color(255, 255, 255, 255);
    private static final Color EXPLOSION_COLOR = new Color(100, 255, 255, 255);
    private static final float FLUX_DAMAGE = 750f;
    private static final int NUM_PARTICLES = 30;
    private static final Color PARTICLE_COLOR = new Color(50, 255, 255, 255);
    private static final Vector2f ZERO = new Vector2f();

    public static void explode(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, CombatEngineAPI engine, boolean useSounds) {
        if (point == null) {
            return;
        }
        engine.spawnExplosion(point, ZERO, EXPLOSION_COLOR, 400f, 0.15f);
        engine.addSmoothParticle(point, ZERO, 500f, 1f, 0.6f, EXPLOSION_COLOR);
        float speed = projectile.getVelocity().length();
        for (int x = 0; x < NUM_PARTICLES; x++) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null, MathUtils.getRandomNumberInRange(speed * 0.05f, speed * 0.3f),
                    (float) Math.random() * 360f), 12f, 1f, MathUtils.getRandomNumberInRange(0.4f, 0.8f), PARTICLE_COLOR);
        }

        for (int i = 0; i < 3; i++) {
            float angle = (float) Math.random() * 360f;
            float distance = (float) Math.random() * 150f + 75f;
            Vector2f point1 = MathUtils.getPointOnCircumference(point, distance, angle);
            Vector2f point2 = new Vector2f(point);
            engine.spawnEmpArc(projectile.getSource(), point1, new SimpleEntity(point1), new SimpleEntity(point2),
                    DamageType.ENERGY, 0f, 0f, 1000f, null, 15f, COLOR1, COLOR2);
        }

        if (!projectile.didDamage()) {
            StandardLight light = new StandardLight(point, ZERO, ZERO, null);
            light.setColor(EXPLOSION_COLOR);
            light.setSize(AREA_EFFECT * 1.5f);
            light.setIntensity(0.7f);
            light.fadeOut(0.6f);
            LightShader.addLight(light);
        }

        List<ShipAPI> targets = CombatUtils.getShipsWithinRange(point, AREA_EFFECT);
        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            targets.remove(ship);
        }

        TEM_Util.filterObscuredTargets(target, point, targets, true, true, false);

        for (ShipAPI ship : targets) {
            float distance = TEM_Util.getActualDistance(point, ship, true);
            float reduction = 1f;
            if (distance > AREA_EFFECT_INNER) {
                reduction = (AREA_EFFECT - distance) / (AREA_EFFECT - AREA_EFFECT_INNER);
            }

            if (reduction <= 0f) {
                continue;
            }

            boolean hitShields = false;
            if (ship.getShield() != null) {
                hitShields = ship.getShield().isWithinArc(point);
            }
            if (!hitShields) {
                if (ship.getVariant().getHullMods().contains("tem_latticeshield")
                        && TEM_LatticeShield.shieldLevel(ship) > 0f) {
                    hitShields = true;
                }
            }

            ship.getFluxTracker().increaseFlux(AREA_FLUX_DAMAGE * reduction, hitShields);
            engine.spawnEmpArc(projectile.getSource(), point, ship, ship, DamageType.ENERGY, 0f, AREA_EMP * reduction, AREA_EFFECT, null, 40f, COLOR3, COLOR4);
        }

        if (target != null) {
            TEM_WeaponScriptPlugin.genGalatineBlast(point, new Vector2f(target.getVelocity().x * 0.45f, target.getVelocity().y * 0.45f));
        } else {
            TEM_WeaponScriptPlugin.genGalatineBlast(point, ZERO);
        }

        if (useSounds) {
            Global.getSoundPlayer().playSound("tem_galatine_impact", 1f, 1f, point, ZERO);
        }
    }

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (target instanceof ShipAPI || target instanceof CombatAsteroidAPI) {
            boolean hitShields = shieldHit;
            if (point == null) {
                return;
            }
            explode(projectile, target, point, engine, true);
            if (target instanceof ShipAPI) {
                ShipAPI ship = (ShipAPI) target;

                if (!hitShields) {
                    if (ship.getVariant().getHullMods().contains("tem_latticeshield")
                            && TEM_LatticeShield.shieldLevel(ship) > 0f) {
                        hitShields = true;
                    }
                }

                ship.getFluxTracker().increaseFlux(FLUX_DAMAGE, hitShields);
            }
        }
    }
}
