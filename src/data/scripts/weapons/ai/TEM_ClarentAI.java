package data.scripts.weapons.ai;

import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_ClarentAI extends TEM_BaseMissile {

    private static final float ANTI_CLUMP_RANGE = 125f;
    private static final float VELOCITY_DAMPING_FACTOR = 0.15f;
    private static final Vector2f ZERO = new Vector2f();

    private final IntervalUtil antiClumpInterval = new IntervalUtil(0.1f, 0.25f);
    private boolean aspectLocked = true;
    private float nearestClarentAngle = 180f;
    private float nearestClarentDistance = Float.MAX_VALUE;

    public TEM_ClarentAI(MissileAPI missile, ShipAPI launchingShip) {
        super(missile, launchingShip);
        missile.setEmpResistance(missile.getEmpResistance() + 2);
    }

    @Override
    public void advance(float amount) {
        if (missile.isFizzling() || missile.isFading()) {
            return;
        }

        if (!acquireTarget(amount)) {
            missile.giveCommand(ShipCommand.ACCELERATE);
            return;
        }

        antiClumpInterval.advance(amount);

        float distance = MathUtils.getDistance(target.getLocation(), missile.getLocation());
        float acceleration = missile.getAcceleration();
        float maxSpeed = missile.getMaxSpeed();

        Vector2f calculationVelocity = new Vector2f(missile.getVelocity());
        if (calculationVelocity.length() <= maxSpeed * 0.5f) {
            if (calculationVelocity.length() <= maxSpeed * 0.25f) {
                calculationVelocity.set(maxSpeed * 0.5f, 0f);
                VectorUtils.rotate(calculationVelocity, missile.getFacing(), calculationVelocity);
            } else {
                calculationVelocity.scale((maxSpeed * 0.5f) / calculationVelocity.length());
            }
        }

        Vector2f guidedTarget = interceptAdvanced(missile.getLocation(), calculationVelocity.length(), acceleration,
                                                  maxSpeed, target.getLocation(), target.getVelocity());
        if (guidedTarget == null) {
            Vector2f projection = new Vector2f(target.getVelocity());
            float scalar = distance / (calculationVelocity.length() + 1f);
            projection.scale(scalar);
            guidedTarget = Vector2f.add(target.getLocation(), projection, null);
        }

        if (antiClumpInterval.intervalElapsed()) {
            nearestClarentDistance = Float.MAX_VALUE;
            nearestClarentAngle = 180f;
            List<MissileAPI> nearbyClarents =
                             CombatUtils.getMissilesWithinRange(missile.getLocation(), ANTI_CLUMP_RANGE);
            for (MissileAPI nearbyClarent : nearbyClarents) {
                if (nearbyClarent == missile) {
                    continue;
                }

                if (nearbyClarent.getProjectileSpecId() != null && missile.getProjectileSpecId() != null &&
                        nearbyClarent.getProjectileSpecId().contentEquals(
                                missile.getProjectileSpecId())) {
                    float clarentDistance = MathUtils.getDistance(missile.getLocation(), nearbyClarent.getLocation());
                    if (clarentDistance < nearestClarentDistance) {
                        nearestClarentDistance = clarentDistance;
                        nearestClarentAngle = VectorUtils.getAngle(missile.getLocation(), nearbyClarent.getLocation());
                    }
                }
            }
        }

        float velocityFacing = VectorUtils.getFacing(calculationVelocity);
        float absoluteDistance = MathUtils.getShortestRotation(velocityFacing, VectorUtils.getAngle(
                                                               missile.getLocation(), guidedTarget));
        float angularDistance = MathUtils.getShortestRotation(missile.getFacing(), VectorUtils.getAngle(
                                                              missile.getLocation(), guidedTarget));
        float nearestClarentAngularDistance = MathUtils.getShortestRotation(missile.getFacing(), nearestClarentAngle);
        if (nearestClarentDistance <= ANTI_CLUMP_RANGE && Math.abs(nearestClarentAngularDistance) <= 135f && distance >
                750f) {
            if (nearestClarentAngularDistance <= 0f) {
                angularDistance += 0.75f * (1f - nearestClarentDistance / ANTI_CLUMP_RANGE) * (135f +
                                                                                               nearestClarentAngularDistance);
            } else {
                angularDistance += 0.75f * (1f - nearestClarentDistance / ANTI_CLUMP_RANGE) * (-135f +
                                                                                               nearestClarentAngularDistance);
            }
        }

        float compensationDifference = MathUtils.getShortestRotation(angularDistance, absoluteDistance);
        float compensationFactor = Math.min(1f, distance / 500f);
        if (Math.abs(compensationDifference) <= 75f) {
            angularDistance += compensationFactor * compensationDifference;
        }
        float absDAng = Math.abs(angularDistance);

        if (aspectLocked && absDAng > 45f * compensationFactor + 45f) {
            aspectLocked = false;
        }

        if (!aspectLocked && absDAng <= 15f * compensationFactor + 15f) {
            aspectLocked = true;
        }

        missile.giveCommand(angularDistance < 0 ? ShipCommand.TURN_RIGHT : ShipCommand.TURN_LEFT);
        if (aspectLocked || distance > 500f) {
            missile.giveCommand(ShipCommand.ACCELERATE);
        }

        if (absDAng < 5) {
            float MFlightAng = VectorUtils.getAngle(ZERO, calculationVelocity);
            float MFlightCC = MathUtils.getShortestRotation(missile.getFacing(), MFlightAng);
            if (Math.abs(MFlightCC) > 20) {
                missile.giveCommand(MFlightCC < 0 ? ShipCommand.STRAFE_LEFT : ShipCommand.STRAFE_RIGHT);
            }
        }

        if (absDAng < Math.abs(missile.getAngularVelocity()) * VELOCITY_DAMPING_FACTOR) {
            missile.setAngularVelocity(angularDistance / VELOCITY_DAMPING_FACTOR);
        }
    }

    @Override
    protected boolean acquireTarget(float amount) {
        if (!isTargetValidAlternate(target)) {
            if (target instanceof ShipAPI) {
                ShipAPI ship = (ShipAPI) target;
                if (ship.isPhased() && ship.isAlive()) {
                    return false;
                }
            }
            setTarget(findBestTarget());
            if (target == null) {
                setTarget(findBestTargetAlternate());
            }
            if (target == null) {
                return false;
            }
        }
        return true;
    }

    protected ShipAPI findBestTargetAlternate() {
        ShipAPI closest = null;
        float range = getRemainingRange();
        float distance, closestDistance = getRemainingRange() + missile.getMaxSpeed() * 2f;
        List<ShipAPI> ships = AIUtils.getEnemiesOnMap(missile);
        int size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI tmp = ships.get(i);
            float mod = 0f;
            if (tmp.isFighter() || tmp.isDrone()) {
                mod = range / 2f;
            }
            if (!isTargetValidAlternate(tmp)) {
                mod = range;
            }
            distance = MathUtils.getDistance(tmp, missile.getLocation()) + mod;
            if (distance < closestDistance) {
                closest = tmp;
                closestDistance = distance;
            }
        }
        return closest;
    }

    @Override
    protected boolean isTargetValid(CombatEntityAPI target) {
        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (ship.isFighter() || ship.isDrone()) {
                return false;
            }
        }
        return super.isTargetValid(target);
    }

    protected boolean isTargetValidAlternate(CombatEntityAPI target) {
        return super.isTargetValid(target);
    }
}
