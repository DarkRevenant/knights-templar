package data.scripts.weapons.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import data.scripts.weapons.TEM_CrucifixOnHitEffect;
import java.awt.Color;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_CrucifixAI extends TEM_BaseMissile {

    private static final float AIM_FUDGE_TIME = 0.5f;
    private static final float ENGINE_DEAD_TIME_MAX = 1.25f; // Max time until engine burn starts
    private static final float ENGINE_DEAD_TIME_MIN = 0.75f; // Min time until engine burn starts
    private static final float FIRE_INACCURACY = 0.75f;
    private static final Color GLOW_COLOR = new Color(10, 200, 255, 100);
    private static final String START_FLY_SOUND_ID = "tem_crucifix_boost";
    private static final float VELOCITY_DAMPING_FACTOR = 0.5f;
    private static final Vector2f ZERO = new Vector2f();

    static List<ShipAPI> getSortedDirectTargets(ShipAPI launchingShip) {
        List<ShipAPI> directTargets = CombatUtils.getShipsWithinRange(launchingShip.getMouseTarget(), 500f);
        if (!directTargets.isEmpty()) {
            Collections.sort(directTargets, new CollectionUtils.SortEntitiesByDistance(launchingShip.getMouseTarget()));
        }
        return directTargets;
    }

    private float aimFudgeTimer;
    private float engineDeadTimer;
    private boolean flying = false;
    private final float inaccuracyFactor;
    private boolean readyToFly = false;

    public TEM_CrucifixAI(MissileAPI missile, ShipAPI launchingShip) {
        super(missile, launchingShip);

        aimFudgeTimer = AIM_FUDGE_TIME;
        engineDeadTimer = MathUtils.getRandomNumberInRange(ENGINE_DEAD_TIME_MIN, ENGINE_DEAD_TIME_MAX);
        inaccuracyFactor = MathUtils.getRandomNumberInRange(-FIRE_INACCURACY, FIRE_INACCURACY);
        missile.setArmedWhileFizzling(true);
    }

    @Override
    public void advance(float amount) {
        if (missile.getHitpoints() < missile.getMaxHitpoints() * 0.5f) {
            TEM_CrucifixOnHitEffect.explode(missile, null, missile.getLocation(), false, Global.getCombatEngine());
            Global.getCombatEngine().applyDamage(missile, missile, missile.getLocation(), missile.getHitpoints() * 10f,
                    DamageType.FRAGMENTATION, 0f, false, false, missile, false);
        }

        if (missile.isFading()) {
            return;
        }

        if (!readyToFly) {
            if (missile.isFizzling()) {
                return;
            }

            if (engineDeadTimer > 0f) {
                engineDeadTimer -= amount;
                if (engineDeadTimer <= 0f) {
                    readyToFly = true;
                }
            }

            if (acquireTarget(amount)) {
                float distance = MathUtils.getDistance(target.getLocation(), missile.getLocation());
                float maxSpeed = missile.getMaxSpeed();

                Vector2f guidedTarget = intercept(missile.getLocation(), maxSpeed, target.getLocation(),
                        target.getVelocity());
                if (guidedTarget == null) {
                    Vector2f projection = new Vector2f(target.getVelocity());
                    float scalar = distance / (missile.getVelocity().length() + 1f);
                    projection.scale(scalar);
                    guidedTarget = Vector2f.add(target.getLocation(), projection, null);
                }

                float inaccuracy = getCrossSectionalRadius(missile.getLocation(), target) * inaccuracyFactor;
                float inaccuracyDeg = (float) Math.toDegrees(Math.atan2(inaccuracy, distance));
                float angularDistance = MathUtils.getShortestRotation(missile.getFacing(),
                        MathUtils.clampAngle(VectorUtils.getAngle(
                                missile.getLocation(),
                                guidedTarget) + inaccuracyDeg));
                float absDAng = Math.abs(angularDistance);

                missile.giveCommand(angularDistance < 0 ? ShipCommand.TURN_RIGHT : ShipCommand.TURN_LEFT);

                if (absDAng < Math.abs(missile.getAngularVelocity()) * VELOCITY_DAMPING_FACTOR) {
                    missile.setAngularVelocity(angularDistance / VELOCITY_DAMPING_FACTOR);
                }
            }

            if (Math.random() > 0.5) {
                missile.giveCommand(ShipCommand.DECELERATE);
            }
        } else {
            if (!flying) {
                flying = true;
                Global.getSoundPlayer().playSound(START_FLY_SOUND_ID, 1f, 0.9f, missile.getLocation(), ZERO);
                Global.getCombatEngine().addHitParticle(missile.getLocation(), missile.getVelocity(), 125f, 0.5f, 0.25f,
                        GLOW_COLOR);
            }

            if (aimFudgeTimer > 0f) {
                aimFudgeTimer -= amount;

                if (missile.isFizzling()) {
                    return;
                }

                if (acquireTarget(amount)) {
                    float distance = MathUtils.getDistance(target.getLocation(), missile.getLocation());
                    float maxSpeed = missile.getMaxSpeed();

                    Vector2f guidedTarget = intercept(missile.getLocation(), maxSpeed, target.getLocation(),
                            target.getVelocity());
                    if (guidedTarget == null) {
                        Vector2f projection = new Vector2f(target.getVelocity());
                        float scalar = distance / (missile.getVelocity().length() + 1f);
                        projection.scale(scalar);
                        guidedTarget = Vector2f.add(target.getLocation(), projection, null);
                    }

                    float inaccuracy = getCrossSectionalRadius(missile.getLocation(), target) * inaccuracyFactor;
                    float inaccuracyDeg = (float) Math.toDegrees(Math.atan2(inaccuracy, distance));
                    float angularDistance = MathUtils.getShortestRotation(missile.getFacing(),
                            MathUtils.clampAngle(VectorUtils.getAngle(
                                    missile.getLocation(),
                                    guidedTarget) + inaccuracyDeg));
                    float absDAng = Math.abs(angularDistance);

                    missile.giveCommand(angularDistance < 0 ? ShipCommand.TURN_RIGHT : ShipCommand.TURN_LEFT);

                    if (absDAng < Math.abs(missile.getAngularVelocity()) * VELOCITY_DAMPING_FACTOR) {
                        missile.setAngularVelocity(angularDistance / VELOCITY_DAMPING_FACTOR);
                    }
                }
            }

            if (!missile.isFizzling()) {
                missile.giveCommand(ShipCommand.ACCELERATE);
            }
        }
    }

    @Override
    protected boolean acquireTarget(float amount) {
        if (!isTargetValidAlternate(target)) {
            if (target instanceof ShipAPI) {
                ShipAPI ship = (ShipAPI) target;
                if (ship.isPhased() && ship.isAlive()) {
                    return false;
                }
            }
            setTarget(findBestTarget());
            if (target == null) {
                setTarget(findBestTargetAlternate());
            }
            if (target == null) {
                return false;
            }
        }
        return true;
    }

    // This is some bullshit weighted random picker that favors larger ships
    @Override
    protected ShipAPI findBestTarget() {
        ShipAPI best = null;
        float weight, bestWeight = 0f;
        List<ShipAPI> ships = AIUtils.getEnemiesOnMap(missile);
        float range = getRemainingRange() + 300f;
        int size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI tmp = ships.get(i);
            float mod;
            if (!isTargetValid(tmp)) {
                mod = 0f;
            } else {
                switch (tmp.getHullSize()) {
                    default:
                    case FIGHTER:
                        mod = 1f;
                        break;
                    case FRIGATE:
                        mod = 10f;
                        break;
                    case DESTROYER:
                        mod = 50f;
                        break;
                    case CRUISER:
                        mod = 100f;
                        break;
                    case CAPITAL_SHIP:
                        mod = 125f;
                        break;
                }
            }
            if (MathUtils.getDistance(tmp, missile.getLocation()) > range) {
                mod = 0f;
            }
            weight = (2500f / Math.max(MathUtils.getDistance(tmp, missile.getLocation()), 200f)) * mod;
            if (weight > bestWeight) {
                best = tmp;
                bestWeight = weight;
            }
        }
        return best;
    }

    protected ShipAPI findBestTargetAlternate() {
        ShipAPI best = null;
        float weight, bestWeight = 0f;
        List<ShipAPI> ships = AIUtils.getEnemiesOnMap(missile);
        float range = getRemainingRange();
        int size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI tmp = ships.get(i);
            float mod;
            if (!isTargetValidAlternate(tmp)) {
                mod = 0f;
            } else {
                switch (tmp.getHullSize()) {
                    default:
                    case FIGHTER:
                        mod = 1f;
                        break;
                    case FRIGATE:
                        mod = 10f;
                        break;
                    case DESTROYER:
                        mod = 50f;
                        break;
                    case CRUISER:
                        mod = 100f;
                        break;
                    case CAPITAL_SHIP:
                        mod = 125f;
                        break;
                }
            }
            if (MathUtils.getDistance(tmp, missile.getLocation()) > range) {
                mod = 0f;
            }
            weight = (2500f / Math.max(MathUtils.getDistance(tmp, missile.getLocation()), 200f)) * mod;
            if (weight > bestWeight) {
                best = tmp;
                bestWeight = weight;
            }
        }
        return best;
    }

    @Override
    protected CombatEntityAPI getMouseTarget(ShipAPI launchingShip) {
        ListIterator<ShipAPI> iter = getSortedDirectTargets(launchingShip).listIterator();
        while (iter.hasNext()) {
            ShipAPI tmp = iter.next();
            if (isTargetValid(tmp)) {
                return tmp;
            }
        }
        return null;
    }

    @Override
    protected boolean isTargetValid(CombatEntityAPI target) {
        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (ship.isFighter() || ship.isDrone()) {
                return false;
            }
        }
        return super.isTargetValid(target);
    }

    protected boolean isTargetValidAlternate(CombatEntityAPI target) {
        return super.isTargetValid(target);
    }

}
