package data.scripts.weapons.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import data.scripts.util.TEM_AnamorphicFlare;
import java.awt.Color;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_RolandAI extends TEM_BaseMissile {

    private static final Color COLOR1 = new Color(255, 150, 255, 255);
    private static final float MIRV_DISTANCE = 1000f;
    private static final float VELOCITY_DAMPING_FACTOR = 0.45f;
    private static final Vector2f ZERO = new Vector2f();

    public static void mirv(MissileAPI missile) {
        if (missile.getSource() != null) {
            TEM_AnamorphicFlare.createFlare(missile.getSource(), new Vector2f(missile.getLocation()),
                    Global.getCombatEngine(), 1f, 0.1f,
                    (float) Math.random()
                    * 15f - 7.5f,
                    10f, 2f, COLOR1, COLOR1);
        }

        Global.getSoundPlayer().playSound("tem_clarent_impact", 1.25f, 0.75f, missile.getLocation(), ZERO);

        Global.getCombatEngine().applyDamage(missile, missile.getLocation(), missile.getHitpoints() * 100f,
                DamageType.FRAGMENTATION, 0f, false, false, missile, false);
        for (int i = 0; i < 4; i++) {
            float angle = missile.getFacing() + (i - 1) * 60f - 30f;
            if (angle < 0f) {
                angle += 360f;
            } else if (angle >= 360f) {
                angle -= 360f;
            }
            Vector2f location = MathUtils.getPointOnCircumference(missile.getLocation(), 5f, angle);
            MissileAPI newMissile = (MissileAPI) Global.getCombatEngine().spawnProjectile(missile.getSource(),
                    missile.getWeapon(),
                    "tem_clarent_single",
                    location, angle, null);
            newMissile.setFromMissile(true);
        }
    }

    private boolean aspectLocked = true;
    private float retargetTimer = 1f;
    private float timeLive = 0f;

    public TEM_RolandAI(MissileAPI missile, ShipAPI launchingShip) {
        super(missile, launchingShip);
        missile.setEmpResistance(missile.getEmpResistance() + 4);
    }

    @Override
    public void advance(float amount) {
        if (missile.isFizzling() || missile.isFading()) {
            if (target == null) {
                return;
            }
            float distance = MathUtils.getDistance(target, missile);
            if (distance <= MIRV_DISTANCE) {
                mirv(missile);
            }
            return;
        }

        timeLive += amount;

        if (!acquireTarget(amount)) {
            missile.giveCommand(ShipCommand.ACCELERATE);
            return;
        }

        float distance = MathUtils.getDistance(target.getLocation(), missile.getLocation());
        float acceleration = missile.getAcceleration();
        float maxSpeed = missile.getMaxSpeed();

        Vector2f calculationVelocity = new Vector2f(missile.getVelocity());
        if (calculationVelocity.length() <= maxSpeed * 0.5f) {
            if (calculationVelocity.length() <= maxSpeed * 0.25f) {
                calculationVelocity.set(maxSpeed * 0.5f, 0f);
                VectorUtils.rotate(calculationVelocity, missile.getFacing(), calculationVelocity);
            } else {
                calculationVelocity.scale((maxSpeed * 0.5f) / calculationVelocity.length());
            }
        }

        Vector2f guidedTarget = interceptAdvanced(missile.getLocation(), calculationVelocity.length(), acceleration,
                maxSpeed, target.getLocation(),
                target.getVelocity());
        if (guidedTarget == null) {
            Vector2f projection = new Vector2f(target.getVelocity());
            float scalar = distance / (calculationVelocity.length() + 1f);
            projection.scale(scalar);
            guidedTarget = Vector2f.add(target.getLocation(), projection, null);
        }

        if (distance <= MIRV_DISTANCE && timeLive >= 1f) {
            timeLive = -99999f;
            mirv(missile);
            return;
        }

        float velocityFacing = VectorUtils.getFacing(calculationVelocity);
        float absoluteDistance = MathUtils.getShortestRotation(velocityFacing, VectorUtils.getAngle(
                missile.getLocation(), guidedTarget));
        float angularDistance = MathUtils.getShortestRotation(missile.getFacing(), VectorUtils.getAngle(
                missile.getLocation(), guidedTarget));
        float compensationDifference = MathUtils.getShortestRotation(angularDistance, absoluteDistance);
        if (Math.abs(compensationDifference) <= 75f) {
            angularDistance += 0.5f * compensationDifference;
        }
        float absDAng = Math.abs(angularDistance);

        if (aspectLocked && absDAng > 75f) {
            aspectLocked = false;
        }

        if (!aspectLocked && absDAng <= 30f) {
            aspectLocked = true;
        }

        missile.giveCommand(angularDistance < 0 ? ShipCommand.TURN_RIGHT : ShipCommand.TURN_LEFT);
        float turnRadius = missile.getMaxSpeed() * (360f / missile.getMaxTurnRate()) / (2f * (float) Math.PI);
        if (aspectLocked || distance > 2.5f * turnRadius) {
            missile.giveCommand(ShipCommand.ACCELERATE);
        }
        if (absDAng < 5) {
            float MFlightAng = VectorUtils.getAngle(ZERO, calculationVelocity);
            float MFlightCC = MathUtils.getShortestRotation(missile.getFacing(), MFlightAng);
            if (Math.abs(MFlightCC) > 20) {
                missile.giveCommand(MFlightCC < 0 ? ShipCommand.STRAFE_LEFT : ShipCommand.STRAFE_RIGHT);
            }
        }

        if (absDAng < Math.abs(missile.getAngularVelocity()) * VELOCITY_DAMPING_FACTOR) {
            missile.setAngularVelocity(angularDistance / VELOCITY_DAMPING_FACTOR);
        }
    }

    @Override
    protected boolean acquireTarget(float amount) {
        if (!isTargetValidAlternate(target)) {
            if (retargetTimer > 0f) {
                retargetTimer -= amount;
                return false;
            } else {
                retargetTimer = 1f;
            }
            setTarget(findBestTarget());
            if (target == null) {
                setTarget(findBestTargetAlternate());
            }
            if (target == null) {
                return false;
            }
        } else {
            retargetTimer = 1f;
            if (!isTargetValidAlternate(target)) {
                CombatEntityAPI newTarget = findBestTarget();
                if (newTarget != null) {
                    target = newTarget;
                }
            }
        }
        return true;
    }

    protected ShipAPI findBestTargetAlternate() {
        ShipAPI closest = null;
        float range = getRemainingRange();
        float distance, closestDistance = getRemainingRange() + missile.getMaxSpeed() * 2f;
        List<ShipAPI> ships = AIUtils.getEnemiesOnMap(missile);
        int size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI tmp = ships.get(i);
            float mod = 0f;
            if (tmp.isFighter() || tmp.isDrone()) {
                mod = range / 2f;
            }
            if (!isTargetValidAlternate(tmp)) {
                mod = range;
            }
            distance = MathUtils.getDistance(tmp, missile.getLocation()) + mod;
            if (distance < closestDistance) {
                closest = tmp;
                closestDistance = distance;
            }
        }
        return closest;
    }

    @Override
    protected boolean isTargetValid(CombatEntityAPI target) {
        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (ship.isFighter() || ship.isDrone()) {
                return false;
            }
        }
        return super.isTargetValid(target);
    }

    protected boolean isTargetValidAlternate(CombatEntityAPI target) {
        return super.isTargetValid(target);
    }
}
