package data.scripts.weapons.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.weapons.TEM_TrucidareOnHitEffect;
import java.awt.Color;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_TrucidareAI extends TEM_BaseMissile {

    private static final Color COLOR1 = new Color(50, 230, 255);
    private static final float FIZZLED_MIRV_DISTANCE = 1500f;
    private static final Color GLOW_COLOR = new Color(10, 200, 255, 15);
    private static final float MIRV_DISTANCE = 500f;
    private static final float REAR_MIRV_DISTANCE = 1500f;
    private static final float SWINGAROUND_DISTANCE = 3000f;
    private static final float VELOCITY_DAMPING_FACTOR = 0.5f;
    private static final Vector2f ZERO = new Vector2f();

    public static void mirv(MissileAPI missile, CombatEntityAPI mirvTarget) {
        if (missile.getSource() != null) {
            Global.getCombatEngine().spawnExplosion(missile.getLocation(), ZERO, COLOR1, 220f, 0.5f);
            for (int x = 0; x < 75; x++) {
                float angle = (float) Math.random() * 360f;
                Global.getCombatEngine().addHitParticle(missile.getLocation(),
                        MathUtils.getPointOnCircumference(null,
                                MathUtils.getRandomNumberInRange(
                                        100f, 200f),
                                angle),
                        15f, 1f,
                        MathUtils.getRandomNumberInRange(0.4f, 0.8f),
                        COLOR1);
            }
        }

        Global.getSoundPlayer().playSound("tem_trucidare_burst", 0.75f, 1.25f, missile.getLocation(), ZERO);

        Global.getCombatEngine().applyDamage(missile, missile.getLocation(), missile.getHitpoints() * 100f,
                DamageType.FRAGMENTATION, 0f, false, false, missile, false);
        for (int i = 0; i < 8; i++) {
            float angle = missile.getFacing() + i * 45f;
            if (angle < 0f) {
                angle += 360f;
            } else if (angle >= 360f) {
                angle -= 360f;
            }
            Vector2f location = MathUtils.getPointOnCircumference(missile.getLocation(), 15f, angle);
            Vector2f vel = MathUtils.getPointOnCircumference(null, MathUtils.getRandomNumberInRange(-200f, 200f),
                    (float) Math.random() * 360f);
            CombatEntityAPI crucifix
                    = Global.getCombatEngine().spawnProjectile(missile.getSource(), missile.getWeapon(),
                            "tem_crucifix", new Vector2f(location), angle,
                            vel);
            MissileAPI crucifixMissile = (MissileAPI) crucifix;
            ((GuidedMissileAI) crucifixMissile.getMissileAI()).setTarget(mirvTarget);
            crucifixMissile.setFromMissile(true);
        }
    }

    private final float angleOffset;
    private final IntervalUtil interval = new IntervalUtil(0.3f, 0.5f);
    private final float rearTargetArc;
    private float retargetTimer = 1f;
    private float timeLive = 0f;

    public TEM_TrucidareAI(MissileAPI missile, ShipAPI launchingShip) {
        super(missile, launchingShip);
        missile.setEmpResistance(missile.getEmpResistance() + 10);
        angleOffset = MathUtils.getRandomNumberInRange(-5f, 5f);
        rearTargetArc = MathUtils.getRandomNumberInRange(135f, 150f);
    }

    @Override
    public void advance(float amount) {
        if (missile.getHitpoints() < missile.getMaxHitpoints() * 0.5f) {
            TEM_TrucidareOnHitEffect.explode(missile, null, missile.getLocation(), false, Global.getCombatEngine());
            Global.getCombatEngine().applyDamage(missile, missile.getLocation(), missile.getHitpoints() * 10f,
                    DamageType.FRAGMENTATION, 0f, false, false, missile, false);
        } else {
            interval.advance(amount);
            if (interval.intervalElapsed()) {
                boolean detonate = false;
                List<ShipAPI> ships = CombatUtils.getShipsWithinRange(missile.getLocation(),
                        missile.getCollisionRadius());
                for (ShipAPI ship : ships) {
                    if (ship.getOwner() != missile.getOwner() && ship.isAlive()) {
                        detonate = true;
                        break;
                    }
                }
                if (detonate) {
                    missile.setArmingTime(0f);
                } else {
                    missile.setArmingTime(30f);
                }
            }
        }

        if (missile.isFizzling() || missile.isFading()) {
            if (target == null) {
                return;
            }
            float distance = MathUtils.getDistance(target, missile);
            if (distance <= FIZZLED_MIRV_DISTANCE) {
                mirv(missile, target);
            }
            return;
        }

        if (Math.random() > 0.75) {
            Global.getCombatEngine().addSmoothParticle(missile.getLocation(), missile.getVelocity(), 150f, 1f,
                    amount * 6f, GLOW_COLOR);
        }

        timeLive += amount;

        if (!acquireTarget(amount)) {
            missile.giveCommand(ShipCommand.ACCELERATE);
            return;
        }

        float distance = MathUtils.getDistance(target.getLocation(), missile.getLocation());
        float acceleration = missile.getAcceleration();
        float maxSpeed = missile.getMaxSpeed();

        Vector2f calculationVelocity = new Vector2f(missile.getVelocity());
        if (calculationVelocity.length() <= maxSpeed * 0.5f) {
            if (calculationVelocity.length() <= maxSpeed * 0.25f) {
                calculationVelocity.set(maxSpeed * 0.5f, 0f);
                VectorUtils.rotate(calculationVelocity, missile.getFacing(), calculationVelocity);
            } else {
                calculationVelocity.scale((maxSpeed * 0.5f) / calculationVelocity.length());
            }
        }

        Vector2f guidedTarget = interceptAdvanced(missile.getLocation(), calculationVelocity.length(), acceleration,
                maxSpeed, target.getLocation(), target.getVelocity());
        if (guidedTarget == null) {
            Vector2f projection = new Vector2f(target.getVelocity());
            float scalar = distance / (calculationVelocity.length() + 1f);
            projection.scale(scalar);
            guidedTarget = Vector2f.add(target.getLocation(), projection, null);
        }
        float timeFrac = missile.getFlightTime() / missile.getMaxFlightTime();

        if (distance <= MIRV_DISTANCE && timeLive >= 2f) {
            timeLive = -99999f;
            mirv(missile, target);
            return;
        }

        float targetAngle
                = MathUtils.clampAngle(VectorUtils.getAngle(missile.getLocation(), guidedTarget) + angleOffset);
        /* Negative - clockwise */
        float targetArc = MathUtils.getShortestRotation(target.getFacing(), VectorUtils.getAngle(
                target.getLocation(), missile.getLocation()));
        if (distance <= SWINGAROUND_DISTANCE * Math.min(1f, 1.25f - timeFrac)) {
            if (Math.abs(targetArc) < rearTargetArc) {
                targetAngle += -Math.signum(targetArc) * 90f * Math.min(1f, 1.25f - timeFrac);
            }
        }

        if (distance <= REAR_MIRV_DISTANCE / Math.min(1f, 1.5f - timeFrac) && Math.abs(targetArc) >= rearTargetArc
                && timeLive >= 2f) {
            timeLive = -99999f;
            mirv(missile, target);
            return;
        }

        float velocityFacing = VectorUtils.getFacing(calculationVelocity);
        float absoluteDistance = MathUtils.getShortestRotation(velocityFacing, VectorUtils.getAngle(
                missile.getLocation(), guidedTarget));
        float angularDistance = MathUtils.getShortestRotation(missile.getFacing(), targetAngle);
        float compensationDifference = MathUtils.getShortestRotation(angularDistance, absoluteDistance);
        if (Math.abs(compensationDifference) <= 75f) {
            angularDistance += 0.5f * compensationDifference;
        }

        missile.giveCommand(angularDistance < 0 ? ShipCommand.TURN_RIGHT : ShipCommand.TURN_LEFT);
        missile.giveCommand(ShipCommand.ACCELERATE);

        float absDAng = Math.abs(angularDistance);
        if (absDAng < Math.abs(missile.getAngularVelocity()) * VELOCITY_DAMPING_FACTOR) {
            missile.setAngularVelocity(angularDistance / VELOCITY_DAMPING_FACTOR);
        }
    }

    @Override
    protected boolean acquireTarget(float amount) {
        if (!isTargetValidAlternate(target)) {
            if (retargetTimer > 0f) {
                retargetTimer -= amount;
                return false;
            } else {
                retargetTimer = 1f;
            }
            setTarget(findBestTarget());
            if (target == null) {
                return false;
            }
        } else {
            retargetTimer = 1f;
        }
        return true;
    }

    @Override
    protected boolean isTargetValid(CombatEntityAPI target) {
        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (ship.isFighter() || ship.isDrone()) {
                return false;
            }
        }
        return super.isTargetValid(target);
    }

    protected boolean isTargetValidAlternate(CombatEntityAPI target) {
        return super.isTargetValid(target);
    }
}
