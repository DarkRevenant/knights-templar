package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.everyframe.TEM_WeaponScriptPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_SecaceOnHitEffect implements OnHitEffectPlugin {

    private static final Color EXPLOSION_COLOR = new Color(100, 255, 255, 150);
    private static final float FLUX_DAMAGE = 250f;
    private static final int NUM_PARTICLES = 12;
    private static final Color PARTICLE_COLOR = new Color(50, 255, 255, 150);
    private static final Vector2f ZERO = new Vector2f();

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult,
            CombatEngineAPI engine) {
        boolean hitShields = shieldHit;
        if (target == null || point == null) {
            return;
        }
        engine.spawnExplosion(point, new Vector2f(target.getVelocity().x * 0.45f, target.getVelocity().y * 0.45f),
                EXPLOSION_COLOR, 70f, 0.15f);
        float speed = projectile.getVelocity().length();
        for (int x = 0; x < NUM_PARTICLES; x++) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null, MathUtils.getRandomNumberInRange(
                    speed * 0.05f, speed * 0.3f),
                    (float) Math.random() * 360f), 8f, 1f,
                    MathUtils.getRandomNumberInRange(0.2f, 0.4f),
                    PARTICLE_COLOR);
        }

        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;

            if (!hitShields) {
                if (ship.getVariant().getHullMods().contains("tem_latticeshield")
                        && TEM_LatticeShield.shieldLevel(ship) > 0f) {
                    hitShields = true;
                }
            }

            ship.getFluxTracker().increaseFlux(FLUX_DAMAGE, hitShields);
        }

        float angle = (float) Math.random() * 360f;
        float distance = (float) Math.random() * 25f + 25f;
        Vector2f point1 = MathUtils.getPointOnCircumference(point, distance, angle);
        Vector2f point2 = new Vector2f(point);
        engine.spawnEmpArc(projectile.getSource(), point1, new SimpleEntity(point1), new SimpleEntity(point2),
                DamageType.ENERGY, 0f, 0f, 1000f, null, 15f,
                new Color(75, 225, 255), new Color(200, 225, 255));

        float timeAfterFade = Math.max(0f,
                projectile.getElapsed() - (projectile.getWeapon().getRange() / projectile.getWeapon().getProjectileSpeed()));
        float alpha = (0.15f - timeAfterFade) / 0.15f;
        TEM_WeaponScriptPlugin.genSecaceBlast(point, new Vector2f(target.getVelocity().x * 0.45f, target.getVelocity().y * 0.45f), alpha);

        Global.getSoundPlayer().playSound("tem_secace_impact", 1f, 1f, point, ZERO);
    }
}
