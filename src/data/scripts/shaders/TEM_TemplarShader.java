package data.scripts.shaders;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEngineLayers;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.everyframe.TEM_AlmaceBurstPlugin;
import data.scripts.everyframe.TEM_PriwenBurstPlugin;
import data.scripts.shipsystems.TEM_AegisShieldStats;
import data.scripts.shipsystems.TEM_DivineReachStats;
import data.scripts.shipsystems.TEM_HeavySchismDriveStats;
import data.scripts.shipsystems.TEM_HolyChargeStats;
import data.scripts.shipsystems.TEM_RiastradStats;
import data.scripts.shipsystems.TEM_SchismDriveStats;
import data.scripts.shipsystems.TEM_WarpSpasmStats;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Level;
import org.dark.shaders.util.ShaderAPI;
import org.dark.shaders.util.ShaderLib;
import org.json.JSONException;
import org.json.JSONObject;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.opengl.ARBFramebufferObject;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;

public class TEM_TemplarShader implements ShaderAPI {

    private static final Color COLOR_BOSS_ARCHBISHOP = new Color(0, 255, 255);
    private static final Color COLOR_BOSS_PALADIN = new Color(255, 255, 100);
    private static final Color COLOR_CHEVALIER = new Color(255, 235, 75);
    private static final Color COLOR_CRUCIFIX = new Color(50, 230, 255);
    private static final Color COLOR_CRUSADER = new Color(50, 225, 255);
    private static final Color COLOR_JESUIT = new Color(0, 255, 255);
    private static final Color COLOR_MARTYR = new Color(246, 238, 139);
    private static final Color COLOR_MISSILE = new Color(255, 75, 75);
    private static final Color COLOR_PALADIN = new Color(255, 255, 100);
    private static final Color COLOR_RED = new Color(255, 20, 20);
    private static final Color COLOR_TEMPLAR = new Color(51, 140, 255);

    private static final String DATA_KEY = "TEM_TemplarShader";

    private static final String SETTINGS_FILE = "TEMPLAR_OPTIONS.ini";

    private boolean enabled = false;
    private int flareBufferId;
    private int flareTex;
    private final int index[] = new int[10];
    private final int indexAux[] = new int[4];
    private int program = 0;
    private int programAux = 0;
    private boolean validated = false;
    private boolean validatedAux = false;
    private boolean activated = false;
    private final IntervalUtil inactiveInterval = new IntervalUtil(1f, 2f);

    public TEM_TemplarShader() {
        if (!ShaderLib.areShadersAllowed() || !ShaderLib.areBuffersAllowed()) {
            enabled = false;
            return;
        }

        Global.getLogger(TEM_LatticeShieldShader.class).setLevel(Level.ERROR);

        try {
            loadSettings();
        } catch (IOException | JSONException e) {
            Global.getLogger(TEM_LatticeShieldShader.class).log(Level.ERROR, "Failed to load shader settings: " + e.getMessage());
            enabled = false;
            return;
        }

        if (!enabled) {
            return;
        }

        String vertShader;
        String fragShader;

        try {
            vertShader = Global.getSettings().loadText("data/shaders/templar.vert");
            fragShader = Global.getSettings().loadText("data/shaders/templar.frag");
        } catch (IOException ex) {
            enabled = false;
            return;
        }

        program = ShaderLib.loadShader(vertShader, fragShader);

        if (program == 0) {
            enabled = false;
            return;
        }

        try {
            vertShader = Global.getSettings().loadText("data/shaders/templarBlur.vert");
            fragShader = Global.getSettings().loadText("data/shaders/templarBlur.frag");
        } catch (IOException ex) {
            enabled = false;
            return;
        }

        programAux = ShaderLib.loadShader(vertShader, fragShader);

        if (programAux == 0) {
            enabled = false;
            return;
        }

        flareTex = GL11.glGenTextures();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, flareTex);
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, ShaderLib.getInternalWidth() / 8, ShaderLib.getInternalHeight(),
                0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, (ByteBuffer) null);
        if (ShaderLib.useBufferCore()) {
            GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
        } else if (ShaderLib.useBufferARB()) {
            ARBFramebufferObject.glGenerateMipmap(GL11.GL_TEXTURE_2D);
        } else {
            EXTFramebufferObject.glGenerateMipmapEXT(GL11.GL_TEXTURE_2D);
        }
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);
        if (ShaderLib.useBufferCore()) {
            flareBufferId = ShaderLib.makeFramebuffer(GL30.GL_COLOR_ATTACHMENT0, flareTex, ShaderLib.getInternalWidth() / 8, ShaderLib.getInternalHeight(), 0);
        } else if (ShaderLib.useBufferARB()) {
            flareBufferId = ShaderLib.makeFramebuffer(ARBFramebufferObject.GL_COLOR_ATTACHMENT0, flareTex, ShaderLib.getInternalWidth() / 8, ShaderLib.getInternalHeight(), 0);
        } else {
            flareBufferId = ShaderLib.makeFramebuffer(EXTFramebufferObject.GL_COLOR_ATTACHMENT0_EXT, flareTex, ShaderLib.getInternalWidth() / 8, ShaderLib.getInternalHeight(), 0);
        }

        if (flareBufferId == 0) {
            enabled = false;
            return;
        }

        GL20.glUseProgram(program);
        index[0] = GL20.glGetUniformLocation(program, "tex");
        index[1] = GL20.glGetUniformLocation(program, "degree");
        index[2] = GL20.glGetUniformLocation(program, "hueShift");
        index[3] = GL20.glGetUniformLocation(program, "hueMult");
        index[4] = GL20.glGetUniformLocation(program, "saturationShift");
        index[5] = GL20.glGetUniformLocation(program, "saturationMult");
        index[6] = GL20.glGetUniformLocation(program, "lightnessShift");
        index[7] = GL20.glGetUniformLocation(program, "lightnessMult");
        index[8] = GL20.glGetUniformLocation(program, "noiseLevel");
        index[9] = GL20.glGetUniformLocation(program, "time");
        GL20.glUniform1i(index[0], 0);
        GL20.glUniform1f(index[1], 1f);
        GL20.glUniform1f(index[3], 0f);
        GL20.glUniform1f(index[5], 0f);
        GL20.glUseProgram(programAux);
        indexAux[0] = GL20.glGetUniformLocation(programAux, "tex");
        indexAux[1] = GL20.glGetUniformLocation(programAux, "screen");
        indexAux[2] = GL20.glGetUniformLocation(programAux, "scale");
        indexAux[3] = GL20.glGetUniformLocation(programAux, "gain");
        GL20.glUniform1i(indexAux[0], 0);
        GL20.glUniform2f(indexAux[1], ShaderLib.getInternalWidth() / 8, ShaderLib.getVisibleU());
        GL20.glUniform1f(indexAux[3], 4.5f);
        GL20.glUseProgram(0);

        enabled = true;
    }

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
    }

    @Override
    public void destroy() {
        if (program != 0) {
            ByteBuffer countbb = ByteBuffer.allocateDirect(4);
            ByteBuffer shadersbb = ByteBuffer.allocateDirect(8);
            IntBuffer count = countbb.asIntBuffer();
            IntBuffer shaders = shadersbb.asIntBuffer();
            GL20.glGetAttachedShaders(program, count, shaders);
            for (int i = 0; i < 2; i++) {
                GL20.glDeleteShader(shaders.get());
            }
            GL20.glDeleteProgram(program);
        }
        if (programAux != 0) {
            ByteBuffer countbb = ByteBuffer.allocateDirect(4);
            ByteBuffer shadersbb = ByteBuffer.allocateDirect(8);
            IntBuffer count = countbb.asIntBuffer();
            IntBuffer shaders = shadersbb.asIntBuffer();
            GL20.glGetAttachedShaders(programAux, count, shaders);
            for (int i = 0; i < 2; i++) {
                GL20.glDeleteShader(shaders.get());
            }
            GL20.glDeleteProgram(programAux);
        }
        if (flareTex != 0) {
            GL11.glDeleteTextures(flareTex);
        }
        if (flareBufferId != 0) {
            if (ShaderLib.useBufferCore()) {
                GL30.glDeleteFramebuffers(flareBufferId);
            } else if (ShaderLib.useBufferARB()) {
                ARBFramebufferObject.glDeleteFramebuffers(flareBufferId);
            } else {
                EXTFramebufferObject.glDeleteFramebuffersEXT(flareBufferId);
            }
        }
    }

    @Override
    public RenderOrder getRenderOrder() {
        return RenderOrder.WORLD_SPACE;
    }

    @Override
    public void initCombat() {
        Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void renderInScreenCoords(ViewportAPI viewport) {
    }

    @Override
    public void renderInWorldCoords(ViewportAPI viewport) {
        CombatEngineAPI engine = Global.getCombatEngine();

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Set<ShipAPI> keepTheseShips = localData.keepTheseShips;
        final Set<ShipAPI> skipTheseShips = localData.skipTheseShips;

        if (!activated) {
            inactiveInterval.advance(engine.getElapsedInLastFrame());
            if (!inactiveInterval.intervalElapsed()) {
                return;
            }
        }

        int objectCount = 0;
        List<ShipAPI> ships = engine.getShips();
        int size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.isAlive() || skipTheseShips.contains(ship)) {
                continue;
            }

            boolean shouldBeSkipped = true;
            if (ship.getHullSpec().getHullId().startsWith("tem_")) {
                activated = true;

                shouldBeSkipped = false;
                float cooldown;
                if (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_teuton") || TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_martyr")) {
                    cooldown = TEM_AlmaceBurstPlugin.cooldownTime(ship);
                } else {
                    cooldown = TEM_PriwenBurstPlugin.cooldownTime(ship);
                }
                if (((ship.getSystem() != null) && ship.getSystem().isActive()) || (cooldown > 0f)) {
                    objectCount++;
                }
                switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                    case "tem_paladin":
                        cooldown = TEM_HolyChargeStats.cooldownTime(ship);
                        if (cooldown > 0f && cooldown < 27f) {
                            objectCount++;
                        }
                        break;
                    case "tem_boss_paladin":
                        cooldown = TEM_HeavySchismDriveStats.cooldownTime(ship);
                        if (cooldown > 0f && cooldown < 15f) {
                            objectCount++;
                        }
                        break;
                }
            }

            if ((objectCount == 0) || (shouldBeSkipped && !keepTheseShips.contains(ship))) {
                List<WeaponAPI> weapons = ship.getAllWeapons();
                int weaponsSize = weapons.size();
                for (int j = 0; j < weaponsSize; j++) {
                    WeaponAPI weapon = weapons.get(j);
                    if (weapon.getId().contentEquals("tem_rhon") || weapon.getId().contentEquals("tem_longinus")
                            || weapon.getId().contentEquals("tem_lancelot")) {
                        if (shouldBeSkipped) {
                            keepTheseShips.add(ship);
                        }
                        shouldBeSkipped = false;
                        if (weapon.isFiring()) {
                            objectCount++;
                        }
                    }
                }
            }

            if (shouldBeSkipped) {
                skipTheseShips.add(ship);
            }
        }
        if (objectCount == 0) {
            List<DamagingProjectileAPI> projectiles = engine.getProjectiles();
            size = projectiles.size();
            for (int i = 0; i < size; i++) {
                DamagingProjectileAPI projectile = projectiles.get(i);
                if ((projectile.getProjectileSpecId() != null)
                        && (projectile.getProjectileSpecId().contentEquals("tem_joyeuse_fake_shot")
                        || projectile.getProjectileSpecId().contentEquals("tem_clarent_srm")
                        || projectile.getProjectileSpecId().contentEquals("tem_crucifix_mrm")
                        || projectile.getProjectileSpecId().contentEquals("tem_trucidare_lrm")
                        || projectile.getProjectileSpecId().contentEquals("tem_roland_mirv"))) {
                    objectCount++;
                }
            }
        }

        if (!enabled) {
            return;
        }

        if (objectCount > 0) {
            drawFlares(viewport);
        }
    }

    private void drawFlares(ViewportAPI viewport) {
        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        if (ShaderLib.useBufferCore()) {
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, ShaderLib.getAuxiliaryBufferId());
        } else if (ShaderLib.useBufferARB()) {
            ARBFramebufferObject.glBindFramebuffer(ARBFramebufferObject.GL_FRAMEBUFFER, ShaderLib.getAuxiliaryBufferId());
        } else {
            EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT,
                    ShaderLib.getAuxiliaryBufferId());
        }

        GL11.glViewport(0, 0, (int) (Global.getSettings().getScreenWidthPixels() * Display.getPixelScaleFactor()),
                (int) (Global.getSettings().getScreenHeightPixels() * Display.getPixelScaleFactor()));

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glOrtho(viewport.getLLX(), viewport.getLLX() + viewport.getVisibleWidth(), viewport.getLLY(), viewport.getLLY() + viewport.getVisibleHeight(), -2000, 2000);

        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPushMatrix();

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();

        GL11.glColorMask(true, true, true, true);
        GL11.glClearColor(0f, 0f, 0f, 0f);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

        CombatEngineAPI engine = Global.getCombatEngine();

        List<DamagingProjectileAPI> projectiles = engine.getProjectiles();
        int size = projectiles.size();
        for (int i = 0; i < size; i++) {
            DamagingProjectileAPI projectile = projectiles.get(i);
            if (projectile.getProjectileSpecId() != null) {
                SpriteAPI sprite;
                MissileAPI missile;
                float offset;
                switch (projectile.getProjectileSpecId()) {
                    case "tem_joyeuse_fake_shot":
                        sprite = Global.getSettings().getSprite("flareShader", "tem_joyeuse");
                        sprite.setColor(COLOR_RED);
                        sprite.setAlphaMult(0.5f);
                        offset = 0f;
                        break;
                    case "tem_clarent_srm":
                        missile = (MissileAPI) projectile;
                        if (missile.isFizzling()) {
                            continue;
                        }
                        sprite = Global.getSettings().getSprite("flareShader", "tem_clarent");
                        sprite.setColor(COLOR_MISSILE);
                        offset = -9f;
                        break;
                    case "tem_crucifix_mrm":
                        sprite = Global.getSettings().getSprite("flareShader", "tem_crucifix");
                        sprite.setColor(COLOR_CRUCIFIX);
                        offset = 5f;
                        break;
                    case "tem_trucidare_lrm":
                        sprite = Global.getSettings().getSprite("flareShader", "tem_trucidare");
                        sprite.setColor(COLOR_CRUCIFIX);
                        offset = 0f;
                        break;
                    case "tem_roland_mirv":
                        missile = (MissileAPI) projectile;
                        if (missile.isFizzling()) {
                            continue;
                        }
                        sprite = Global.getSettings().getSprite("flareShader", "tem_roland");
                        sprite.setColor(COLOR_MISSILE);
                        offset = -20f;
                        break;
                    default:
                        continue;
                }

                Vector2f location = new Vector2f(projectile.getLocation());
                location = MathUtils.getPointOnCircumference(location, offset, projectile.getFacing());

                if (!ShaderLib.isOnScreen(location, 100f)) {
                    continue;
                }

                sprite.setAngle(projectile.getFacing() - 90f);
                sprite.setBlendFunc(GL11.GL_SRC_ALPHA_SATURATE, GL11.GL_ONE);

                sprite.renderAtCenter(location.x, location.y);
            }
        }

        List<ShipAPI> ships = engine.getShips();
        size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.isAlive()) {
                continue;
            }

            List<WeaponAPI> weapons = ship.getAllWeapons();
            int weaponsSize = weapons.size();
            for (int j = 0; j < weaponsSize; j++) {
                WeaponAPI weapon = weapons.get(j);
                if (!weapon.isFiring()) {
                    continue;
                }

                SpriteAPI sprite;
                float offset;
                float level = weapon.getChargeLevel();
                switch (weapon.getId()) {
                    case "tem_rhon":
                        if (weapon.getSlot().isHardpoint()) {
                            sprite = Global.getSettings().getSprite("flareShader", "tem_rhon_hardpoint");
                            offset = 0f;
                        } else {
                            sprite = Global.getSettings().getSprite("flareShader", "tem_rhon_turret");
                            offset = 0f;
                        }
                        sprite.setColor(COLOR_RED);
                        if (!weapon.getSlot().isHidden()) {
                            sprite.setSize(weapon.getSprite().getWidth(), weapon.getSprite().getHeight());
                            sprite.setCenter(weapon.getSprite().getCenterX(), weapon.getSprite().getCenterY());
                        }
                        break;
                    case "tem_longinus":
                        if (weapon.getSlot().isHardpoint()) {
                            sprite = Global.getSettings().getSprite("flareShader", "tem_longinus_hardpoint");
                            offset = 0f;
                        } else {
                            sprite = Global.getSettings().getSprite("flareShader", "tem_longinus_turret");
                            offset = 0f;
                        }
                        sprite.setColor(COLOR_RED);
                        if (!weapon.getSlot().isHidden()) {
                            sprite.setSize(weapon.getSprite().getWidth(), weapon.getSprite().getHeight());
                            sprite.setCenter(weapon.getSprite().getCenterX(), weapon.getSprite().getCenterY());
                        }
                        break;
                    case "tem_lancelot":
                        level *= 1f - ship.getFluxTracker().getFluxLevel() * 0.5f;
                        sprite = Global.getSettings().getSprite("flareShader", "tem_lancelot");
                        offset = 32f;
                        sprite.setColor(COLOR_RED);
                        break;
                    default:
                        continue;
                }

                Vector2f location = new Vector2f(weapon.getLocation());
                Vector2f.add(location, MathUtils.getPointOnCircumference(null, offset, weapon.getCurrAngle()), location);

                if (!ShaderLib.isOnScreen(location, 100f)) {
                    continue;
                }

                sprite.setAngle(weapon.getCurrAngle() - 90f);
                sprite.setAlphaMult(level);
                sprite.setBlendFunc(GL11.GL_SRC_ALPHA_SATURATE, GL11.GL_ONE);

                sprite.renderAtCenter(location.x, location.y);
            }
        }

        size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.getHullSpec().getHullId().startsWith("tem_") || !ship.isAlive()) {
                continue;
            }

            float cooldown;
            switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                case "tem_paladin":
                    cooldown = TEM_HolyChargeStats.cooldownTime(ship);
                    break;
                case "tem_boss_paladin":
                    cooldown = TEM_HeavySchismDriveStats.cooldownTime(ship);
                    break;
                default:
                    cooldown = 0f;
                    break;
            }

            if ((ship.getSystem() == null) || (!ship.getSystem().isActive() && (cooldown <= 0f))) {
                continue;
            }

            float level;

            Vector2f shipLocation = new Vector2f(ship.getLocation());

            if (!ShaderLib.isOnScreen(shipLocation, 1.25f * ship.getCollisionRadius())) {
                continue;
            }

            SpriteAPI sprite;
            switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                case "tem_jesuit":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_jesuit");
                    sprite.setColor(COLOR_JESUIT);
                    level = TEM_SchismDriveStats.effectLevel(ship);
                    break;
                case "tem_martyr":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_martyr1");
                    sprite.setColor(COLOR_MARTYR);
                    level = TEM_RiastradStats.effectLevel(ship);
                    break;
                case "tem_crusader":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_crusader1");
                    sprite.setColor(COLOR_CRUSADER);
                    level = TEM_AegisShieldStats.effectLevel(ship);
                    break;
                case "tem_paladin":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_paladin1");
                    sprite.setColor(COLOR_PALADIN);
                    level = Math.max(TEM_HolyChargeStats.effectLevel(ship), cooldown > 0f ? (27f - cooldown) / 27f : 0f);
                    break;
                case "tem_boss_paladin":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_boss_paladin1");
                    sprite.setColor(COLOR_BOSS_PALADIN);
                    level = Math.max(TEM_HeavySchismDriveStats.effectLevel(ship),
                            cooldown > 0f ? (15f - cooldown) / 15f : 0f);
                    break;
                case "tem_chevalier":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_chevalier1");
                    sprite.setColor(COLOR_CHEVALIER);
                    level = TEM_DivineReachStats.effectLevel(ship);
                    break;
                case "tem_boss_archbishop":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_boss_archbishop1");
                    sprite.setColor(COLOR_BOSS_ARCHBISHOP);
                    level = TEM_WarpSpasmStats.effectLevel(ship);
                    break;
                default:
                    continue;
            }
            sprite.setAngle(ship.getFacing() - 90f);
            sprite.setAlphaMult(level);
            sprite.setCenter(ship.getSpriteAPI().getCenterX(), ship.getSpriteAPI().getCenterY());
            sprite.setBlendFunc(GL11.GL_SRC_ALPHA_SATURATE, GL11.GL_ONE);

            sprite.renderAtCenter(shipLocation.x + (float) Math.random() * 4f - 2f, shipLocation.y + (float) Math.random() * 4f - 2f);
            sprite.renderAtCenter(shipLocation.x + 4f + (float) Math.random() * 4f - 2f, shipLocation.y + (float) Math.random() * 4f - 2f);
            sprite.renderAtCenter(shipLocation.x - 4f + (float) Math.random() * 4f - 2f, shipLocation.y + (float) Math.random() * 4f - 2f);
            sprite.renderAtCenter(shipLocation.x + (float) Math.random() * 4f - 2f, shipLocation.y + 4f + (float) Math.random() * 4f - 2f);
            sprite.renderAtCenter(shipLocation.x + (float) Math.random() * 4f - 2f, shipLocation.y - 4f + (float) Math.random() * 4f - 2f);
        }

        size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.getHullSpec().getHullId().startsWith("tem_") || !ship.isAlive()) {
                continue;
            }
            float cooldown;
            if (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_teuton") || TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_martyr")) {
                cooldown = TEM_AlmaceBurstPlugin.cooldownTime(ship);
            } else {
                cooldown = TEM_PriwenBurstPlugin.cooldownTime(ship);
            }
            if (cooldown <= 0f) {
                continue;
            }

            float level;
            switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                case "tem_teuton":
                case "tem_martyr":
                    level = Math.max(TEM_AlmaceBurstPlugin.effectLevel(ship), cooldown > 0f ? (ship.getFluxTracker().getFluxLevel() * 0.75f + 0.25f) * (9.3f - cooldown) / 9.3f : 0f);
                    break;
                case "tem_boss_paladin":
                    level = Math.max(TEM_PriwenBurstPlugin.effectLevel(ship), cooldown > 0f ? (ship.getFluxTracker().getFluxLevel() * 0.75f + 0.25f) * (4.3f - cooldown) / 4.3f : 0f);
                    break;
                default:
                    level = Math.max(TEM_PriwenBurstPlugin.effectLevel(ship), cooldown > 0f ? (ship.getFluxTracker().getFluxLevel() * 0.75f + 0.25f) * (9.3f - cooldown) / 9.3f : 0f);
                    break;
            }

            Vector2f shipLocation = new Vector2f(ship.getLocation());

            if (!ShaderLib.isOnScreen(shipLocation, 1.25f * ship.getCollisionRadius())) {
                continue;
            }

            SpriteAPI sprite;
            switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                case "tem_jesuit":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_jesuit");
                    break;
                case "tem_martyr":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_martyr2");
                    break;
                case "tem_crusader":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_crusader2");
                    break;
                case "tem_paladin":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_paladin2");
                    break;
                case "tem_chevalier":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_chevalier2");
                    break;
                case "tem_archbishop":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_archbishop");
                    break;
                case "tem_boss_paladin":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_boss_paladin2");
                    break;
                case "tem_boss_archbishop":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_boss_archbishop2");
                    break;
                default:
                    continue;
            }
            sprite.setAngle(ship.getFacing() - 90f);
            sprite.setAlphaMult(level);
            sprite.setCenter(ship.getSpriteAPI().getCenterX(), ship.getSpriteAPI().getCenterY());
            sprite.setBlendFunc(GL11.GL_SRC_ALPHA_SATURATE, GL11.GL_ONE);
            sprite.setColor(COLOR_TEMPLAR);

            sprite.renderAtCenter(shipLocation.x + level * ((float) Math.random() * 8f - 4f), shipLocation.y + level * ((float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x + level * (4f + (float) Math.random() * 8f - 4f), shipLocation.y + level * ((float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x - level * (4f + (float) Math.random() * 8f - 4f), shipLocation.y + level * ((float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x + level * ((float) Math.random() * 8f - 4f), shipLocation.y + level * (4f + (float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x + level * ((float) Math.random() * 8f - 4f), shipLocation.y - level * (4f + (float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x + level * (8f + (float) Math.random() * 8f - 4f), shipLocation.y + level * (8f + (float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x + level * (8f + (float) Math.random() * 8f - 4f), shipLocation.y - level * (8f + (float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x - level * (8f + (float) Math.random() * 8f - 4f), shipLocation.y + level * (8f + (float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x - level * (8f + (float) Math.random() * 8f - 4f), shipLocation.y - level * (8f + (float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x + level * (16f + (float) Math.random() * 8f - 4f), shipLocation.y + level * ((float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x - level * (16f + (float) Math.random() * 8f - 4f), shipLocation.y + level * ((float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x + level * ((float) Math.random() * 8f - 4f), shipLocation.y + level * (16f + (float) Math.random() * 8f - 4f));
            sprite.renderAtCenter(shipLocation.x + level * ((float) Math.random() * 8f - 4f), shipLocation.y - level * (16f + (float) Math.random() * 8f - 4f));
        }

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();
        if (ShaderLib.useBufferCore()) {
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
        } else if (ShaderLib.useBufferARB()) {
            ARBFramebufferObject.glBindFramebuffer(ARBFramebufferObject.GL_FRAMEBUFFER, 0);
        } else {
            EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, 0);
        }
        GL11.glPopAttrib();

        GL20.glUseProgram(program);

        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        GL11.glViewport(0, 0, (int) (Global.getSettings().getScreenWidthPixels() * Display.getPixelScaleFactor()),
                (int) (Global.getSettings().getScreenHeightPixels() * Display.getPixelScaleFactor()));

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glOrtho(viewport.getLLX(), viewport.getLLX() + viewport.getVisibleWidth(), viewport.getLLY(),
                viewport.getLLY() + viewport.getVisibleHeight(), -2000, 2000);

        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPushMatrix();

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();

        size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.getHullSpec().getHullId().startsWith("tem_") || !ship.isAlive()) {
                continue;
            }

            float cooldown;
            switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                case "tem_paladin":
                    cooldown = TEM_HolyChargeStats.cooldownTime(ship);
                    break;
                case "tem_boss_paladin1":
                    cooldown = TEM_HeavySchismDriveStats.cooldownTime(ship);
                    break;
                default:
                    cooldown = 0f;
                    break;
            }

            if ((ship.getSystem() == null) || (!ship.getSystem().isActive() && (cooldown <= 0f))) {
                continue;
            }

            float level;

            Vector2f shipLocation = new Vector2f(ship.getLocation());

            if (!ShaderLib.isOnScreen(shipLocation, 1.25f * ship.getCollisionRadius())) {
                continue;
            }

            SpriteAPI sprite;
            float degree;
            switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                case "tem_jesuit":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_jesuit");
                    level = TEM_SchismDriveStats.effectLevel(ship);
                    degree = 4f;

                    GL20.glUniform1f(index[2], (float) Math.random() * 0.05f + 0.075f); // hueShift
                    GL20.glUniform1f(index[4], (float) Math.random() * 0.2f + 0.8f); // saturationShift
                    GL20.glUniform1f(index[6], ((float) Math.random() * 0.1f - 0.05f) * level); // lightnessShift
                    GL20.glUniform1f(index[7], ((float) Math.random() * 0.4f + 0.4f) * level); // lightnessMult
                    GL20.glUniform1f(index[8], 0.5f); // noiseLevel
                    GL20.glUniform1f(index[9], Global.getCombatEngine().getTotalElapsedTime(true)); // time
                    break;
                case "tem_martyr":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_martyr1");
                    level = TEM_RiastradStats.effectLevel(ship);
                    degree = 3f;

                    GL20.glUniform1f(index[2], (float) Math.random() * 0.01f + 0.15f); // hueShift
                    GL20.glUniform1f(index[4], (float) Math.random() * 0.1f + 0.8f); // saturationShift
                    GL20.glUniform1f(index[6], ((float) Math.random() * 0.1f - 0.05f) * level); // lightnessShift
                    GL20.glUniform1f(index[7], ((float) Math.random() * 0.2f + 0.6f) * level); // lightnessMult
                    GL20.glUniform1f(index[8], 0.25f); // noiseLevel
                    GL20.glUniform1f(index[9], Global.getCombatEngine().getTotalElapsedTime(true)); // time
                    break;
                case "tem_crusader":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_crusader1");
                    level = TEM_AegisShieldStats.effectLevel(ship);
                    degree = 2f;

                    GL20.glUniform1f(index[2], (float) Math.random() * 0.03f + 0.485f); // hueShift
                    GL20.glUniform1f(index[4], (float) Math.random() * 0.05f + 0.775f); // saturationShift
                    GL20.glUniform1f(index[6], 0f); // lightnessShift
                    GL20.glUniform1f(index[7], ((float) Math.random() * 0.4f + 0.4f) * level); // lightnessMult
                    GL20.glUniform1f(index[8], 0.5f); // noiseLevel
                    GL20.glUniform1f(index[9], Global.getCombatEngine().getTotalElapsedTime(true)); // time
                    break;
                case "tem_paladin":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_paladin1");
                    level = Math.max(TEM_HolyChargeStats.effectLevel(ship), cooldown > 0f ? (27f - cooldown) / 27f : 0f);
                    degree = 2f * level;

                    GL20.glUniform1f(index[2], (float) Math.random() * 0.04f + 0.135f); // hueShift
                    GL20.glUniform1f(index[4], (float) Math.random() * 0.2f + 0.6f); // saturationShift
                    GL20.glUniform1f(index[6], ((float) Math.random() * 0.1f - 0.05f) * level); // lightnessShift
                    GL20.glUniform1f(index[7], ((float) Math.random() * 0.2f + 0.4f) * level); // lightnessMult
                    GL20.glUniform1f(index[8], 0.5f * level); // noiseLevel
                    GL20.glUniform1f(index[9], Global.getCombatEngine().getTotalElapsedTime(true)); // time
                    break;
                case "tem_chevalier":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_chevalier1");
                    level = TEM_DivineReachStats.effectLevel(ship);
                    degree = 2f * level;

                    GL20.glUniform1f(index[2], (float) Math.random() * 0.03f + 0.14f); // hueShift
                    GL20.glUniform1f(index[4], (float) Math.random() * 0.2f + 0.8f); // saturationShift
                    GL20.glUniform1f(index[6], ((float) Math.random() * 0.1f - 0.05f) * level); // lightnessShift
                    GL20.glUniform1f(index[7], ((float) Math.random() * 0.25f + 0.3f) * level); // lightnessMult
                    GL20.glUniform1f(index[8], 0.25f * level); // noiseLevel
                    GL20.glUniform1f(index[9], Global.getCombatEngine().getTotalElapsedTime(true)); // time
                    break;
                case "tem_boss_paladin":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_boss_paladin1");
                    level = Math.max(TEM_HeavySchismDriveStats.effectLevel(ship), cooldown > 0f ? (15f - cooldown) / 15f : 0f);
                    degree = 2f * level;

                    GL20.glUniform1f(index[2], (float) Math.random() * 0.04f + 0.135f); // hueShift
                    GL20.glUniform1f(index[4], (float) Math.random() * 0.2f + 0.6f); // saturationShift
                    GL20.glUniform1f(index[6], ((float) Math.random() * 0.1f - 0.05f) * level); // lightnessShift
                    GL20.glUniform1f(index[7], ((float) Math.random() * 0.2f + 0.4f) * level); // lightnessMult
                    GL20.glUniform1f(index[8], 0.5f * level); // noiseLevel
                    GL20.glUniform1f(index[9], Global.getCombatEngine().getTotalElapsedTime(true)); // time
                    break;
                case "tem_boss_archbishop":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_boss_archbishop1");
                    level = TEM_WarpSpasmStats.effectLevel(ship);
                    degree = 5f;

                    GL20.glUniform1f(index[2], (float) Math.random() * 0.05f + 0.55f); // hueShift
                    GL20.glUniform1f(index[4], (float) Math.random() * 0.1f + 0.8f); // saturationShift
                    GL20.glUniform1f(index[6], ((float) Math.random() * 0.1f - 0.05f) * level); // lightnessShift
                    GL20.glUniform1f(index[7], ((float) Math.random() * 0.25f + 0.5f) * level); // lightnessMult
                    GL20.glUniform1f(index[8], 0.5f); // noiseLevel
                    GL20.glUniform1f(index[9], Global.getCombatEngine().getTotalElapsedTime(true)); // time
                    break;
                default:
                    continue;
            }
            sprite.setAngle(ship.getFacing() - 90f);
            sprite.setCenter(ship.getSpriteAPI().getCenterX(), ship.getSpriteAPI().getCenterY());
            sprite.setAlphaMult(level);
            sprite.setAdditiveBlend();

            if (!validated) {
                validated = true;

                // This stuff here is for AMD compatability, normally it would be way back in the shader loader
                GL20.glValidateProgram(program);
                if (GL20.glGetProgrami(program, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
                    Global.getLogger(ShaderLib.class).log(Level.ERROR, ShaderLib.getProgramLogInfo(program));
                    GL11.glMatrixMode(GL11.GL_MODELVIEW);
                    GL11.glPopMatrix();
                    GL11.glMatrixMode(GL11.GL_TEXTURE);
                    GL11.glPopMatrix();
                    GL11.glMatrixMode(GL11.GL_PROJECTION);
                    GL11.glPopMatrix();
                    GL11.glPopAttrib();
                    enabled = false;
                    return;
                }
            }

            sprite.renderAtCenter(shipLocation.x + degree * level * ((float) Math.random() - 0.5f), shipLocation.y + degree * level * ((float) Math.random() - 0.5f));
        }

        size = ships.size();
        for (int i = 0; i < size; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.getHullSpec().getHullId().startsWith("tem_") || !ship.isAlive()) {
                continue;
            }
            float cooldown;
            if (TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_teuton") || TEM_Util.getNonDHullId(ship.getHullSpec()).contentEquals("tem_martyr")) {
                cooldown = TEM_AlmaceBurstPlugin.cooldownTime(ship);
            } else {
                cooldown = TEM_PriwenBurstPlugin.cooldownTime(ship);
            }
            if (cooldown <= 0f) {
                continue;
            }

            float level;
            switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                case "tem_teuton":
                case "tem_martyr":
                    level = Math.max(TEM_AlmaceBurstPlugin.effectLevel(ship), cooldown > 0f ? (ship.getFluxTracker().getFluxLevel() * 0.75f + 0.25f) * (9.3f - cooldown) / 9.3f : 0f);
                    break;
                case "tem_boss_paladin":
                    level = Math.max(TEM_PriwenBurstPlugin.effectLevel(ship), cooldown > 0f ? (ship.getFluxTracker().getFluxLevel() * 0.75f + 0.25f) * (4.3f - cooldown) / 4.3f : 0f);
                    break;
                default:
                    level = Math.max(TEM_PriwenBurstPlugin.effectLevel(ship), cooldown > 0f ? (ship.getFluxTracker().getFluxLevel() * 0.75f + 0.25f) * (9.3f - cooldown) / 9.3f : 0f);
                    break;
            }

            Vector2f shipLocation = new Vector2f(ship.getLocation());

            if (!ShaderLib.isOnScreen(shipLocation, 1.25f * ship.getCollisionRadius())) {
                continue;
            }

            SpriteAPI sprite;
            float degree;
            switch (TEM_Util.getNonDHullId(ship.getHullSpec())) {
                case "tem_jesuit":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_jesuit");
                    degree = 8f;
                    break;
                case "tem_martyr":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_martyr2");
                    degree = 6f;
                    break;
                case "tem_crusader":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_crusader2");
                    degree = 4f;
                    break;
                case "tem_paladin":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_paladin2");
                    degree = 6f;
                    break;
                case "tem_chevalier":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_chevalier2");
                    degree = 6f;
                    break;
                case "tem_archbishop":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_archbishop");
                    degree = 10f;
                    break;
                case "tem_boss_paladin":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_boss_paladin2");
                    degree = 6f;
                    break;
                case "tem_boss_archbishop":
                    sprite = Global.getSettings().getSprite("flareShader", "tem_boss_archbishop2");
                    degree = 14f;
                    break;
                default:
                    continue;
            }
            sprite.setAngle(ship.getFacing() - 90f);
            sprite.setAlphaMult(level);
            sprite.setCenter(ship.getSpriteAPI().getCenterX(), ship.getSpriteAPI().getCenterY());
            sprite.setAdditiveBlend();

            GL20.glUniform1f(index[2], (float) Math.random() * 0.1f * level + 0.45f); // hueShift
            GL20.glUniform1f(index[4], (float) Math.random() * 0.5f * level + 0.5f); // saturationShift
            GL20.glUniform1f(index[6], 0f); // lightnessShift
            GL20.glUniform1f(index[7], ((float) Math.random() * 0.4f + 0.4f) * level); // lightnessMult
            GL20.glUniform1f(index[8], 1f * level); // noiseLevel
            GL20.glUniform1f(index[9], Global.getCombatEngine().getTotalElapsedTime(true)); // time

            if (!validated) {
                validated = true;

                // This stuff here is for AMD compatability, normally it would be way back in the shader loader
                GL20.glValidateProgram(program);
                if (GL20.glGetProgrami(program, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
                    Global.getLogger(ShaderLib.class).log(Level.ERROR, ShaderLib.getProgramLogInfo(program));
                    GL11.glMatrixMode(GL11.GL_MODELVIEW);
                    GL11.glPopMatrix();
                    GL11.glMatrixMode(GL11.GL_TEXTURE);
                    GL11.glPopMatrix();
                    GL11.glMatrixMode(GL11.GL_PROJECTION);
                    GL11.glPopMatrix();
                    GL11.glPopAttrib();
                    GL11.glViewport(0, 0, (int) (Global.getSettings().getScreenWidthPixels() * Display.getPixelScaleFactor()),
                            (int) (Global.getSettings().getScreenHeightPixels() * Display.getPixelScaleFactor()));
                    enabled = false;
                    return;
                }
            }

            sprite.renderAtCenter(shipLocation.x + degree * level * ((float) Math.random() - 0.5f), shipLocation.y + degree * level * ((float) Math.random() - 0.5f));
        }

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();
        GL11.glPopAttrib();

        GL20.glUseProgram(programAux);

        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        if (ShaderLib.useBufferCore()) {
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, flareBufferId);
        } else if (ShaderLib.useBufferARB()) {
            ARBFramebufferObject.glBindFramebuffer(ARBFramebufferObject.GL_FRAMEBUFFER, flareBufferId);
        } else {
            EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, flareBufferId);
        }

        GL11.glViewport(0, 0, (int) (Global.getSettings().getScreenWidthPixels() * Display.getPixelScaleFactor()),
                (int) (Global.getSettings().getScreenHeightPixels() * Display.getPixelScaleFactor()));

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glOrtho(0, Global.getSettings().getScreenWidth(), 0, Global.getSettings().getScreenHeight(), -2000, 2000);

        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPushMatrix();

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        GL11.glColorMask(true, true, true, true);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

        GL11.glColor4f(1f, 1f, 1f, 1f);

        GL20.glUniform1f(indexAux[2], (float) Math.random() * 0.1f + 0.9f); // scale

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, ShaderLib.getAuxiliaryBufferTexture());

        if (!validatedAux) {
            validatedAux = true;

            // This stuff here is for AMD compatability, normally it would be way back in the shader loader
            GL20.glValidateProgram(programAux);
            if (GL20.glGetProgrami(programAux, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
                Global.getLogger(ShaderLib.class).log(Level.ERROR, ShaderLib.getProgramLogInfo(programAux));
                ShaderLib.exitDraw();
                enabled = false;
                return;
            }
        }

        GL11.glBegin(GL11.GL_QUADS);
        GL11.glTexCoord2d(0.0, ShaderLib.getVisibleV());
        GL11.glVertex2d(0.0, Global.getSettings().getScreenHeight());
        GL11.glTexCoord2d(ShaderLib.getVisibleU(), ShaderLib.getVisibleV());
        GL11.glVertex2d(Global.getSettings().getScreenWidth() / 8f, Global.getSettings().getScreenHeight());
        GL11.glTexCoord2d(ShaderLib.getVisibleU(), 0.0);
        GL11.glVertex2d(Global.getSettings().getScreenWidth() / 8f, 0.0);
        GL11.glTexCoord2d(0.0, 0.0);
        GL11.glVertex2d(0.0, 0.0);
        GL11.glEnd();

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();
        if (ShaderLib.useBufferCore()) {
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
        } else if (ShaderLib.useBufferARB()) {
            ARBFramebufferObject.glBindFramebuffer(ARBFramebufferObject.GL_FRAMEBUFFER, 0);
        } else {
            EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, 0);
        }
        GL11.glPopAttrib();

        ShaderLib.beginDraw(0);

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);

        GL11.glColor4f(1f, 1f, 1f, 1f);

        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, flareTex);
        ShaderLib.drawScreenQuad(1f);

        ShaderLib.exitDraw();
    }

    private void loadSettings() throws IOException, JSONException {
        JSONObject settings = Global.getSettings().loadJSON(SETTINGS_FILE);

        enabled = settings.getBoolean("enableFlares");
    }

    @Override
    public CombatEngineLayers getCombatLayer() {
        return CombatEngineLayers.JUST_BELOW_WIDGETS;
    }

    @Override
    public boolean isCombat() {
        return true;
    }

    private static final class LocalData {

        final Set<ShipAPI> keepTheseShips = new HashSet<>(50);
        final Set<ShipAPI> skipTheseShips = new HashSet<>(50);
    }
}
