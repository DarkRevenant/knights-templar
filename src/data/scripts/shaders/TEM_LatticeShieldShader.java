package data.scripts.shaders;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEngineLayers;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.hullmods.TEM_LatticeShield;
import data.scripts.shipsystems.TEM_AegisShieldStats;
import data.scripts.util.TEM_Util;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Level;
import org.dark.shaders.util.ShaderAPI;
import org.dark.shaders.util.ShaderLib;
import org.json.JSONException;
import org.json.JSONObject;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.entities.AnchoredEntity;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBFramebufferObject;
import org.lwjgl.opengl.ARBTextureRg;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;

public class TEM_LatticeShieldShader implements ShaderAPI {

    private static final String DATA_KEY = "TEM_LatticeShieldShader";

    private static final String HULL_MOD_ID = "tem_latticeshield";
    private static final int MAX_HITS = 1024;
    private static final String SETTINGS_FILE = "TEMPLAR_OPTIONS.ini";
    private static final Vector2f ZERO = new Vector2f();

    private final FloatBuffer dataBuffer = BufferUtils.createFloatBuffer(4096);
    private final FloatBuffer dataBufferPre = BufferUtils.createFloatBuffer(4096);

    private boolean enabled = false;
    private final int[] index = new int[7];
    private int latticeTex = 0;
    private int maxHits = 100;
    private int program = 0;
    private boolean validated = false;
    private boolean activated = false;
    private final IntervalUtil inactiveInterval = new IntervalUtil(1f, 2f);

    public TEM_LatticeShieldShader() {
        if (!ShaderLib.areShadersAllowed() || !ShaderLib.areBuffersAllowed()) {
            enabled = false;
            return;
        }

        Global.getLogger(TEM_LatticeShieldShader.class).setLevel(Level.ERROR);

        try {
            loadSettings();
        } catch (IOException | JSONException e) {
            Global.getLogger(TEM_LatticeShieldShader.class).log(Level.ERROR, "Failed to load shader settings: " + e.getMessage());
            enabled = false;
            return;
        }

        if (!enabled) {
            return;
        }

        String vertShader;
        String fragShader;

        try {
            vertShader = Global.getSettings().loadText("data/shaders/lattice/lattice.vert");
            fragShader = Global.getSettings().loadText("data/shaders/lattice/lattice.frag");
        } catch (IOException ex) {
            enabled = false;
            return;
        }

        program = ShaderLib.loadShader(vertShader, fragShader);

        if (program == 0) {
            enabled = false;
            return;
        }

        latticeTex = GL11.glGenTextures();
        GL11.glBindTexture(GL11.GL_TEXTURE_1D, latticeTex);
        if (ShaderLib.useBufferCore()) {
            GL11.glTexImage1D(GL11.GL_TEXTURE_1D, 0, GL30.GL_R32F, 4096, 0, GL11.GL_RED, GL11.GL_FLOAT, (ByteBuffer) null);
        } else {
            GL11.glTexImage1D(GL11.GL_TEXTURE_1D, 0, ARBTextureRg.GL_R32F, 4096, 0, GL11.GL_RED, GL11.GL_FLOAT, (ByteBuffer) null);
        }

        GL20.glUseProgram(program);
        index[0] = GL20.glGetUniformLocation(program, "tex");
        index[1] = GL20.glGetUniformLocation(program, "buf");
        index[2] = GL20.glGetUniformLocation(program, "data");
        index[3] = GL20.glGetUniformLocation(program, "trans");
        index[4] = GL20.glGetUniformLocation(program, "size");
        index[5] = GL20.glGetUniformLocation(program, "norm1");
        index[6] = GL20.glGetUniformLocation(program, "norm2");
        GL20.glUniform1i(index[0], 0);
        GL20.glUniform1i(index[1], 1);
        GL20.glUniform1i(index[2], 2);
        GL20.glUniform1f(index[3], ShaderLib.getSquareTransform());
        GL20.glUseProgram(0);

        enabled = true;
    }

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        CombatEngineAPI engine = Global.getCombatEngine();

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<Object, LatticeHit> hits = localData.hits;

        if (!engine.isPaused()) {
            Iterator<Map.Entry<Object, LatticeHit>> iter = hits.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<Object, LatticeHit> entry = iter.next();
                LatticeHit hit = entry.getValue();

                float mult = 1f;
                if (hit.attachment.getAnchor() instanceof ShipAPI) {
                    mult = ((ShipAPI) hit.attachment.getAnchor()).getMutableStats().getTimeMult().getModifiedValue();
                }

                hit.life += amount * mult;
                if (hit.life >= hit.lifetime) {
                    iter.remove();
                }
            }
        }
    }

    @Override
    public void destroy() {
        if (program != 0) {
            ByteBuffer countbb = ByteBuffer.allocateDirect(4);
            ByteBuffer shadersbb = ByteBuffer.allocateDirect(8);
            IntBuffer count = countbb.asIntBuffer();
            IntBuffer shaders = shadersbb.asIntBuffer();
            GL20.glGetAttachedShaders(program, count, shaders);
            for (int i = 0; i < 2; i++) {
                GL20.glDeleteShader(shaders.get());
            }
            GL20.glDeleteProgram(program);
        }
        if (latticeTex != 0) {
            GL11.glDeleteTextures(latticeTex);
        }
    }

    @Override
    public RenderOrder getRenderOrder() {
        return RenderOrder.WORLD_SPACE;
    }

    @Override
    public void initCombat() {
        Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void renderInScreenCoords(ViewportAPI viewport) {
    }

    @Override
    public void renderInWorldCoords(ViewportAPI viewport) {
        CombatEngineAPI engine = Global.getCombatEngine();

        if (!activated) {
            inactiveInterval.advance(engine.getElapsedInLastFrame());
            if (!inactiveInterval.intervalElapsed()) {
                return;
            }
        }

        int objectCount = 0;
        int systemCount = 0;
        List<ShipAPI> ships = engine.getShips();
        int listSize = ships.size();
        for (int i = 0; i < listSize; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.getVariant().getHullMods().contains(HULL_MOD_ID)) {
                continue;
            }

            activated = true;

            if (TEM_AegisShieldStats.effectLevel(ship) > 0f) {
                systemCount++;
            }

            objectCount++;
        }

        if (objectCount <= 0) {
            return;
        }

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Set<BeamAPI> beams = localData.beams;
        final Map<Object, LatticeHit> hits = localData.hits;
        final Map<DamagingProjectileAPI, Float> projectiles = localData.projectiles;

        List<DamagingProjectileAPI> allProjectiles = engine.getProjectiles();
        listSize = allProjectiles.size();
        for (int i = 0; i < listSize; i++) {
            DamagingProjectileAPI proj = allProjectiles.get(i);
            if (proj.didDamage()) {
                continue;
            }

            if (!projectiles.containsKey(proj)) {
                projectiles.put(proj, proj.getDamageAmount());
            }
        }

        Iterator<Map.Entry<DamagingProjectileAPI, Float>> iter1 = projectiles.entrySet().iterator();
        while (iter1.hasNext()) {
            Map.Entry<DamagingProjectileAPI, Float> entry = iter1.next();
            DamagingProjectileAPI proj = entry.getKey();
            float baseDamage = entry.getValue();
            if (Math.random() > 0.8) {
                entry.setValue(Math.max(baseDamage, proj.getDamageAmount()));
            }

            if (proj.didDamage()) {
                if (proj.getDamageTarget() instanceof ShipAPI) {
                    ShipAPI ship = (ShipAPI) proj.getDamageTarget();
                    if (ship.getVariant().getHullMods().contains(HULL_MOD_ID)) {
                        float damageReduction = TEM_LatticeShield.shieldLevel(ship);
                        if (damageReduction > 0f) {
                            float damage = baseDamage;
                            if (proj.getDamageType() == DamageType.KINETIC) {
                                damage *= 1.25f;
                            }
                            if (proj.getDamageType() == DamageType.HIGH_EXPLOSIVE) {
                                damage *= 0.9f;
                            }
                            if (proj.getDamageType() == DamageType.FRAGMENTATION) {
                                damage *= 0.63f;
                            }
                            float soundDamage = baseDamage;
                            if (proj.getDamageType() == DamageType.KINETIC) {
                                soundDamage *= 1.5f;
                            }
                            if (proj.getDamageType() == DamageType.HIGH_EXPLOSIVE) {
                                soundDamage *= 0.67f;
                            }
                            if (proj.getDamageType() == DamageType.FRAGMENTATION) {
                                soundDamage *= 0.4f;
                            }
                            float fader = 1f;
                            if (!(proj instanceof MissileAPI) && proj.getWeapon() != null) {
                                fader = Math.max(1f - Math.max(proj.getElapsed() / (proj.getWeapon().getRange() / proj.getWeapon().getProjectileSpeed()) - 1f, 0f)
                                        / (400f / proj.getWeapon().getProjectileSpeed()), 0.25f);
                            }
                            float factor = ship.getMutableStats().getShieldDamageTakenMult().getModifiedValue();
                            damage *= fader * factor;
                            float volume = 0.7f;
                            if (soundDamage >= 200f) {
                                Global.getSoundPlayer().playSound("tem_latticeshield_heavy", 1f, volume, proj.getLocation(), ship.getVelocity());
                            } else if (soundDamage >= 70f) {
                                Global.getSoundPlayer().playSound("tem_latticeshield_solid", 1f, volume, proj.getLocation(), ship.getVelocity());
                            } else {
                                volume = Math.max(Math.min(volume * soundDamage / 70f, 1f), 0.35f);
                                Global.getSoundPlayer().playSound("tem_latticeshield_light", 1f, volume, proj.getLocation(), ship.getVelocity());
                            }
                            if (damage > 25f) {
                                float lifetime = (float) Math.pow(damage, 0.25) * 0.3f * damageReduction;
                                float size = (float) Math.pow(damage, 0.33) * 35f;
                                float intensity = (float) Math.pow(damage, 0.33) * 0.35f * damageReduction;
                                hits.put(proj, new LatticeHit(new AnchoredEntity(ship, proj.getLocation()), size, lifetime, intensity, null));

                                Vector2f tempVec = new Vector2f();
                                float baseAngle = VectorUtils.getAngle(ship.getLocation(), proj.getLocation());
                                int limit = (int) size / 20;
                                for (int i = 0; i < limit; i++) {
                                    tempVec.set(intensity * 50f * ((float) Math.random() + 1f), 0f);
                                    float angle = baseAngle + ((float) Math.random() - 0.5f) * 225f;
                                    if (angle >= 360f) {
                                        angle -= 360f;
                                    } else if (angle < 0f) {
                                        angle += 360f;
                                    }
                                    VectorUtils.rotate(tempVec, angle, tempVec);
                                    Vector2f.add(tempVec, ship.getVelocity(), tempVec);
                                    engine.addHitParticle(proj.getLocation(), tempVec, (float) Math.random() * 5f + 5f,
                                            intensity, lifetime * ((float) Math.random() + 1f) / 4f, TEM_LatticeShield.VISUAL_SHIELD_COLOR);
                                }
                            }
                        }
                    }
                }
                iter1.remove();
                continue;
            }

            if (!engine.isEntityInPlay(proj)) {
                iter1.remove();
            }
        }

        List<BeamAPI> allBeams = engine.getBeams();
        listSize = allBeams.size();
        for (int i = 0; i < listSize; i++) {
            BeamAPI beam = allBeams.get(i);
            if (beam.getBrightness() <= 0f) {
                continue;
            }

            if (!beams.contains(beam)) {
                beams.add(beam);
            }
        }

        Iterator<BeamAPI> iter2 = beams.iterator();
        while (iter2.hasNext()) {
            BeamAPI beam = iter2.next();

            if (beam.getDamageTarget() instanceof ShipAPI) {
                ShipAPI ship = (ShipAPI) beam.getDamageTarget();
                if (ship.getVariant().getHullMods().contains(HULL_MOD_ID)) {
                    float damageReduction = TEM_LatticeShield.shieldLevel(ship);
                    if (damageReduction > 0f) {
                        float damage = beam.getWeapon().getDerivedStats().getDps() * beam.getBrightness();
                        if (beam.getWeapon().getDamageType() == DamageType.FRAGMENTATION) {
                            damage *= 0.63f;
                        }
                        if (beam.getWeapon().getDamageType() == DamageType.HIGH_EXPLOSIVE) {
                            damage *= 0.83f;
                        }
                        if (beam.getWeapon().getDamageType() == DamageType.KINETIC) {
                            damage *= 1.25f;
                        }
                        if (damage > 0f) {
                            float lifetime = (float) Math.pow(damage, 0.25) * 0.3f * damageReduction;
                            float size = (float) Math.pow(damage, 0.33) * 25f;
                            float intensity = (float) Math.pow(damage, 0.33) * 0.25f * damageReduction;
                            LatticeHit existingHit = hits.get(beam);
                            if (existingHit != null) {
                                existingHit.attachment.reanchor(ship, beam.getTo());
                                existingHit.size = size;
                                existingHit.lifetime = lifetime;
                                existingHit.life = 0f;
                                existingHit.intensity = intensity;
                            } else {
                                hits.put(beam, new LatticeHit(new AnchoredEntity(ship, beam.getTo()), size, lifetime, intensity, beam));
                            }
                        }
                    }
                }
            }

            if ((beam.getBrightness() <= 0f) || ((Math.random() > 0.9) && !allBeams.contains(beam))) {
                iter2.remove();
            }
        }

        if (!enabled) {
            return;
        }

        if (!hits.isEmpty() || (systemCount > 0)) {
            drawHits(viewport);
        }
    }

    private void drawHits(ViewportAPI viewport) {
        CombatEngineAPI engine = Global.getCombatEngine();

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<Object, LatticeHit> hits = localData.hits;

        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        if (ShaderLib.useBufferCore()) {
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, ShaderLib.getAuxiliaryBufferId());
        } else if (ShaderLib.useBufferARB()) {
            ARBFramebufferObject.glBindFramebuffer(ARBFramebufferObject.GL_FRAMEBUFFER, ShaderLib.getAuxiliaryBufferId());
        } else {
            EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, ShaderLib.getAuxiliaryBufferId());
        }

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glOrtho(viewport.getLLX(), viewport.getLLX() + viewport.getVisibleWidth(), viewport.getLLY(), viewport.getLLY() + viewport.getVisibleHeight(), -2000, 2000);

        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPushMatrix();

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();

        GL11.glColorMask(true, true, true, true);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

        int objectCount = 0;
        int systemCount = 0;
        List<ShipAPI> ships = engine.getShips();
        int listSize = ships.size();
        for (int i = 0; i < listSize; i++) {
            ShipAPI ship = ships.get(i);
            if (!ship.getVariant().getHullMods().contains(HULL_MOD_ID)) {
                continue;
            }

            Vector2f shipLocation = new Vector2f(ship.getLocation());

            if (!ShaderLib.isOnScreen(shipLocation, 1.25f * ship.getCollisionRadius())) {
                continue;
            }

            SpriteAPI sprite = Global.getSettings().getSprite("latticeShield", TEM_Util.getNonDHullId(ship.getHullSpec()));
            if (sprite == null) {
                continue;
            }

            float aegis = TEM_AegisShieldStats.effectLevel(ship);

            sprite.setAngle(ship.getFacing() - 90f);
            sprite.setCenter(ship.getSpriteAPI().getCenterX(), ship.getSpriteAPI().getCenterY());
            if (aegis > 0f) {
                systemCount++;
                sprite.setColor(TEM_LatticeShield.AEGIS_SHIELD_COLOR);
                sprite.setAlphaMult(aegis * ((float) Math.random() * 0.1f + 0.65f));
            } else {
                float emptyThreshold, criticalThreshold, warningThreshold;
                switch (ship.getHullSize()) {
                    case FRIGATE:
                    case FIGHTER:
                        emptyThreshold = TEM_LatticeShield.EMPTY_THRESHOLD_FRIGATE;
                        criticalThreshold = TEM_LatticeShield.CRITICAL_THRESHOLD_FRIGATE;
                        warningThreshold = TEM_LatticeShield.WARNING_THRESHOLD_FRIGATE;
                        break;
                    case DESTROYER:
                        emptyThreshold = TEM_LatticeShield.EMPTY_THRESHOLD_DESTROYER;
                        criticalThreshold = TEM_LatticeShield.CRITICAL_THRESHOLD_DESTROYER;
                        warningThreshold = TEM_LatticeShield.WARNING_THRESHOLD_DESTROYER;
                        break;
                    case CRUISER:
                        emptyThreshold = TEM_LatticeShield.EMPTY_THRESHOLD_CRUISER;
                        criticalThreshold = TEM_LatticeShield.CRITICAL_THRESHOLD_CRUISER;
                        warningThreshold = TEM_LatticeShield.WARNING_THRESHOLD_CRUISER;
                        break;
                    case CAPITAL_SHIP:
                    default:
                        emptyThreshold = TEM_LatticeShield.EMPTY_THRESHOLD_CAPITAL;
                        criticalThreshold = TEM_LatticeShield.CRITICAL_THRESHOLD_CAPITAL;
                        warningThreshold = TEM_LatticeShield.WARNING_THRESHOLD_CAPITAL;
                        break;
                }
                if ((ship.getFluxTracker().getFluxLevel() >= emptyThreshold) || ship.getFluxTracker().isOverloadedOrVenting() || !ship.isAlive()) {
                    //sprite.setColor(Color.BLACK);
                    continue;
                } else {
                    boolean shieldActive = false;
                    for (LatticeHit hit : hits.values()) {
                        if (hit.attachment.getAnchor() == ship) {
                            shieldActive = true;
                        }
                    }

                    if (!shieldActive) {
                        continue;
                    }

                    if (ship.getFluxTracker().getFluxLevel() > criticalThreshold) {
                        sprite.setColor(TEM_LatticeShield.CRITICAL_SHIELD_COLOR);
                    } else if (ship.getFluxTracker().getFluxLevel() > warningThreshold) {
                        sprite.setColor(FastTrig.sin(engine.getTotalElapsedTime(false) * 4f * Math.PI) > 0f ? TEM_LatticeShield.CRITICAL_SHIELD_COLOR : TEM_LatticeShield.VISUAL_SHIELD_COLOR);
                    } else {
                        sprite.setColor(TEM_LatticeShield.VISUAL_SHIELD_COLOR);
                    }
                }
                sprite.setAlphaMult(0f);
            }
            sprite.setBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
            sprite.renderAtCenter(shipLocation.x, shipLocation.y);

            objectCount++;
        }

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();
        if (ShaderLib.useBufferCore()) {
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
        } else if (ShaderLib.useBufferARB()) {
            ARBFramebufferObject.glBindFramebuffer(ARBFramebufferObject.GL_FRAMEBUFFER, 0);
        } else {
            EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, 0);
        }
        GL11.glPopAttrib();

        GL11.glViewport(0, 0, (int) (Global.getSettings().getScreenWidthPixels() * Display.getPixelScaleFactor()),
                (int) (Global.getSettings().getScreenHeightPixels() * Display.getPixelScaleFactor()));

        if (objectCount <= 0 && systemCount <= 0) {
            return;
        }

        ShaderLib.beginDraw(program);

        Vector2f maxCoords = null;
        Vector2f minCoords = null;
        float maxSize = 0;
        float maxIntensity = 0;

        int hitCount = 0;
        final float[] bufferPut = new float[4];
        for (LatticeHit hit : hits.values()) {
            float size = Math.max(hit.size * (hit.lifetime - hit.life * 0.5f) / hit.lifetime, 0f);

            if (!ShaderLib.isOnScreen(hit.attachment.getLocation(), size)) {
                continue;
            }

            Vector2f coords = ShaderLib.transformScreenToUV(ShaderLib.transformWorldToScreen(hit.attachment.getLocation()));
            size = ShaderLib.unitsToUV(size);
            float intensity = Math.max(hit.intensity * (hit.lifetime - hit.life) / hit.lifetime, 0f);

            if (maxCoords == null || minCoords == null) {
                maxCoords = new Vector2f(coords);
                minCoords = new Vector2f(coords);
            } else {
                if (coords.x > maxCoords.x) {
                    maxCoords.x = coords.x;
                } else if (coords.x < minCoords.x) {
                    minCoords.x = coords.x;
                }
                if (coords.y > maxCoords.y) {
                    maxCoords.y = coords.y;
                } else if (coords.y < minCoords.y) {
                    minCoords.y = coords.y;
                }
            }
            if (size > maxSize) {
                maxSize = size;
            }
            if (intensity > maxIntensity) {
                maxIntensity = intensity;
            }

            bufferPut[0] = coords.x;
            bufferPut[1] = coords.y;
            bufferPut[2] = size;
            bufferPut[3] = intensity;
            dataBufferPre.put(bufferPut);

            hitCount++;
            if (hitCount >= Math.min(MAX_HITS, maxHits)) {
                break;
            }
        }

        Vector2f normX;
        Vector2f normY;
        Vector2f normS;
        Vector2f normI;

        if (hitCount <= 0 || minCoords == null || maxCoords == null) {
            normX = ZERO;
            normY = ZERO;
            normS = ZERO;
            normI = ZERO;
            hitCount = 0;
        } else {
            normX = ShaderLib.getTextureDataNormalization(minCoords.x, maxCoords.x);
            normY = ShaderLib.getTextureDataNormalization(minCoords.y, maxCoords.y);
            normS = ShaderLib.getTextureDataNormalization(0f, maxSize);
            normI = ShaderLib.getTextureDataNormalization(0f, maxIntensity);

            dataBufferPre.flip();
            final int size = hitCount * 4;
            for (int i = 0; i < size; i++) {
                int pos = i % 4;
                switch (pos) {
                    case 0:
                        dataBuffer.put((dataBufferPre.get() - normX.y) / normX.x);
                        break;
                    case 1:
                        dataBuffer.put((dataBufferPre.get() - normY.y) / normY.x);
                        break;
                    case 2:
                        dataBuffer.put(dataBufferPre.get() / normS.x);
                        break;
                    default:
                        dataBuffer.put(dataBufferPre.get() / normI.x);
                        break;
                }
            }
        }

        dataBuffer.flip();

        GL11.glBindTexture(GL11.GL_TEXTURE_1D, latticeTex);
        GL11.glTexSubImage1D(GL11.GL_TEXTURE_1D, 0, 0, dataBuffer.remaining(), GL11.GL_RED, GL11.GL_FLOAT, dataBuffer);
        GL11.glTexParameteri(GL11.GL_TEXTURE_1D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_1D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_1D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
        GL11.glTexParameteri(GL11.GL_TEXTURE_1D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);

        GL20.glUniform1i(index[4], hitCount); // size
        GL20.glUniform4f(index[5], normX.x, normX.y, normY.x, normY.y); // norm1
        GL20.glUniform2f(index[6], normS.x, normI.x); // norm2

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, ShaderLib.getScreenTexture());
        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, ShaderLib.getAuxiliaryBufferTexture());
        GL13.glActiveTexture(GL13.GL_TEXTURE2);
        GL11.glBindTexture(GL11.GL_TEXTURE_1D, latticeTex);

        if (!validated) {
            validated = true;

            // This stuff here is for AMD compatability, normally it would be way back in the shader loader
            GL20.glValidateProgram(program);
            if (GL20.glGetProgrami(program, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
                Global.getLogger(ShaderLib.class).log(Level.ERROR, ShaderLib.getProgramLogInfo(program));
                ShaderLib.exitDraw();
                dataBuffer.clear();
                dataBufferPre.clear();
                enabled = false;
                return;
            }
        }

        GL11.glDisable(GL11.GL_BLEND);
        ShaderLib.screenDraw(ShaderLib.getScreenTexture(), GL13.GL_TEXTURE0);

        ShaderLib.exitDraw();

        dataBuffer.clear();
        dataBufferPre.clear();
    }

//    private float getAdjustedDamage(DamagingProjectileAPI proj, float baseDamage, boolean shields) {
//        DamageAPI damage = proj.getDamage();
//        MutableShipStatsAPI stats = damage.getStats();
//        WeaponAPI weapon = proj.getWeapon();
//        float dmg = baseDamage;
//
//        if (weapon == null) {
//            return dmg;
//        }
//
//        if (proj instanceof MissileAPI || weapon.getType() == WeaponType.MISSILE) {
//            dmg *= stats.getMissileWeaponDamageMult().getModifiedValue();
//        }
//
//        if (weapon.getType() == WeaponType.BALLISTIC) {
//            dmg *= stats.getBallisticWeaponDamageMult().getModifiedValue();
//        }
//        if (weapon.getType() == WeaponType.ENERGY) {
//            dmg *= stats.getEnergyWeaponDamageMult().getModifiedValue();
//        }
//        if (shields) {
//            dmg *= stats.getDamageToTargetShieldsMult().getModifiedValue();
//        }
//
//        return dmg;
//    }
    private void loadSettings() throws IOException, JSONException {
        JSONObject settings = Global.getSettings().loadJSON(SETTINGS_FILE);

        enabled = settings.getBoolean("enableShieldOverlay");
        maxHits = settings.getInt("maximumShieldHits");
    }

    @Override
    public CombatEngineLayers getCombatLayer() {
        return CombatEngineLayers.ABOVE_SHIPS_AND_MISSILES_LAYER;
    }

    @Override
    public boolean isCombat() {
        return true;
    }

    private static final class LatticeHit {

        final AnchoredEntity attachment;
        final BeamAPI beam;
        float intensity = 0f;
        float life = 0f;
        float lifetime = 0f;
        float size = 0f;

        private LatticeHit(AnchoredEntity attachment, float size, float lifetime, float intensity, BeamAPI beam) {
            this.lifetime = lifetime;
            this.size = size;
            this.attachment = attachment;
            this.intensity = intensity;
            this.beam = beam;
        }
    }

    private static final class LocalData {

        final Set<BeamAPI> beams = new LinkedHashSet<>(200);
        final Map<Object, LatticeHit> hits = new LinkedHashMap<>(200);
        final Map<DamagingProjectileAPI, Float> projectiles = new LinkedHashMap<>(2000);
    }
}
