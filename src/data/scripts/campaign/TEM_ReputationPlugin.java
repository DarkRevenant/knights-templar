package data.scripts.campaign;

import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin;

public class TEM_ReputationPlugin extends CoreReputationPlugin {

    @Override
    public ReputationAdjustmentResult handlePlayerReputationAction(Object actionObject, String factionId) {
        return new ReputationAdjustmentResult(0);
    }
}
