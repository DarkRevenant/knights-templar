package data.scripts.campaign.econ;

import com.fs.starfarer.api.campaign.econ.Industry;
import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.util.Misc;

public class TEM_Avalon extends BaseMarketConditionPlugin {

    private static final float STABILITY_BONUS = 10f;

    @Override
    public void apply(String id) {
        super.apply(id);

        /* Create supply in the Population industry */
        Industry population = market.getIndustry(Industries.POPULATION);
        if (population != null) {
            if (market.getCommodityData(Commodities.SUPPLIES) != null) {
                population.supply(id, Commodities.SUPPLIES, market.getCommodityData(Commodities.SUPPLIES).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.FUEL) != null) {
                population.supply(id, Commodities.FUEL, market.getCommodityData(Commodities.FUEL).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.FOOD) != null) {
                population.supply(id, Commodities.FOOD, market.getCommodityData(Commodities.FOOD).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.DOMESTIC_GOODS) != null) {
                population.supply(id, Commodities.DOMESTIC_GOODS, market.getCommodityData(Commodities.DOMESTIC_GOODS).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.DRUGS) != null) {
                population.supply(id, Commodities.DRUGS, market.getCommodityData(Commodities.DRUGS).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.HAND_WEAPONS) != null) {
                population.supply(id, Commodities.HAND_WEAPONS, market.getCommodityData(Commodities.HAND_WEAPONS).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.MARINES) != null) {
                population.supply(id, Commodities.MARINES, market.getCommodityData(Commodities.MARINES).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.ORGANICS) != null) {
                population.supply(id, Commodities.ORGANICS, market.getCommodityData(Commodities.ORGANICS).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.VOLATILES) != null) {
                population.supply(id, Commodities.VOLATILES, market.getCommodityData(Commodities.VOLATILES).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.HEAVY_MACHINERY) != null) {
                population.supply(id, Commodities.HEAVY_MACHINERY, market.getCommodityData(Commodities.HEAVY_MACHINERY).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.METALS) != null) {
                population.supply(id, Commodities.METALS, market.getCommodityData(Commodities.METALS).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.RARE_METALS) != null) {
                population.supply(id, Commodities.RARE_METALS, market.getCommodityData(Commodities.RARE_METALS).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
            if (market.getCommodityData(Commodities.SHIPS) != null) {
                population.supply(id, Commodities.SHIPS, market.getCommodityData(Commodities.SHIPS).getMaxDemand(), Misc.ucFirst(condition.getName().toLowerCase()));
            }
        }

        market.getStability().modifyFlat(id, STABILITY_BONUS, "Avalon");
    }

    @Override
    public void unapply(String id) {
        super.unapply(id);

        Industry population = market.getIndustry(Industries.POPULATION);
        if (population != null) {
            population.getSupply(Commodities.SUPPLIES).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.FUEL).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.FOOD).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.DOMESTIC_GOODS).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.DRUGS).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.HAND_WEAPONS).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.MARINES).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.ORGANICS).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.VOLATILES).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.HEAVY_MACHINERY).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.METALS).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.RARE_METALS).getQuantity().unmodifyFlat(id);
            population.getSupply(Commodities.SHIPS).getQuantity().unmodifyFlat(id);
        }

        market.getStability().unmodify(id);
    }
}
