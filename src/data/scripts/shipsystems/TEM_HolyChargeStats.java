package data.scripts.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_HolyChargeStats extends BaseShipSystemScript {

    private static final Color COLOR1 = new Color(246, 238, 139);
    private static final Color COLOR2 = new Color(246, 238, 139, 100);
    private static final Color COLOR3 = new Color(246, 238, 139, 150);
    private static final Color COLOR4 = new Color(246, 238, 139, 15);

    private static final String DATA_KEY = "TEM_HolyCharge";

    private static final Vector2f ZERO = new Vector2f();

    public static float cooldownTime(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;

        if (coolingDown.containsKey(ship)) {
            return Global.getCombatEngine().getTotalElapsedTime(false) - coolingDown.get(ship);
        } else {
            return 0f;
        }
    }

    public static float effectLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> acting = localData.acting;

        if (acting.containsKey(ship)) {
            return acting.get(ship);
        } else {
            return 0f;
        }
    }

    public static void removeFromCooldown(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return;
        }

        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;

        if (coolingDown.containsKey(ship)) {
            coolingDown.remove(ship);
        }
    }

    private boolean activated = false;
    private boolean ended = false;
    private final IntervalUtil forceInterval = new IntervalUtil(0.035f, 0.035f);
    private final IntervalUtil interval = new IntervalUtil(0.015f, 0.015f);
    private StandardLight light = null;
    private boolean started = false;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (!engine.getCustomData().containsKey(DATA_KEY)) {
            engine.getCustomData().put(DATA_KEY, new LocalData());
        }

        float amount = Global.getCombatEngine().getElapsedInLastFrame();
        float objectiveAmount = amount * Global.getCombatEngine().getTimeMult().getModifiedValue();
        if (Global.getCombatEngine().isPaused()) {
            amount = 0f;
            objectiveAmount = 0f;
        }

        interval.advance(amount);
        forceInterval.advance(objectiveAmount);
        boolean intervalElapsed = interval.intervalElapsed();
        boolean forceElapsed = forceInterval.intervalElapsed();

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, Float> acting = localData.acting;
        final Map<ShipAPI, Float> coolingDown = localData.coolingDown;

        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (ship != null) {
            if (effectLevel > 0f) {
                acting.put(ship, effectLevel);
            }

            if (state == State.IN && effectLevel > 0f) {
                activated = false;
                if (light == null) {
                    light = new StandardLight(ZERO, ZERO, ZERO, ship);
                    light.setColor(COLOR1);
                    light.setIntensity(0.2f);
                    light.setSize(600f);
                    light.fadeIn(2f);
                    LightShader.addLight(light);
                }

                if (!started) {
                    started = true;
                }

                if (intervalElapsed) {
                    float area = 375f * effectLevel + 125f;
                    float angle = (float) Math.random() * 360f;
                    float distance = (float) Math.random() * area * 0.25f + area * 0.25f;
                    Vector2f point1 = MathUtils.getPointOnCircumference(ship.getLocation(), distance, angle);
                    Vector2f point2 = MathUtils.getPointOnCircumference(ship.getLocation(), distance, angle + 45f
                            * (float) Math.random());
                    engine.spawnEmpArc(ship, point1, new SimpleEntity(point1), new SimpleEntity(point2),
                            DamageType.ENERGY,
                            0f, 0f, 1000f, null, (float) Math.random() * 10f + 20f, COLOR1, COLOR1);
                }

                float chargeLevel = Math.min(effectLevel * 2f, 1f);
                stats.getAcceleration().modifyMult(id, 1f - chargeLevel);
                stats.getDeceleration().modifyMult(id, 1f - chargeLevel);
                stats.getTurnAcceleration().modifyMult(id, 1f - chargeLevel);
            } else if (state == State.ACTIVE) {
                if (light != null) {
                    light.setIntensity(0.15f + (float) Math.random() * 0.05f);
                    light.setSize(500f + (float) Math.random() * 100f);
                }

                float area = 500f;
                float force = 1000f * forceInterval.getIntervalDuration();

                if (!activated) {
                    activated = true;
                    Global.getSoundPlayer().playSound("tem_holycharge_activate", 1f, 1f, ship.getLocation(), ZERO);
                    engine.spawnExplosion(ship.getLocation(), ZERO, COLOR2, area * 1.5f, 0.2f);
                    engine.addHitParticle(ship.getLocation(), ZERO, area * 3f, 0.75f, 0.5f, COLOR1);
                    Vector2f vel = new Vector2f(1f, 0f);
                    VectorUtils.rotate(vel, ship.getFacing(), vel);
                    vel.scale(150f);
                    ship.getVelocity().set(vel);
                    force /= forceInterval.getIntervalDuration();
                    coolingDown.put(ship, engine.getTotalElapsedTime(false));
                } else if (!forceElapsed) {
                    force = 0f;
                }

                Global.getSoundPlayer().playLoop("tem_holycharge_loop", ship, 1f, 1f, ship.getLocation(), ZERO);

                if (forceElapsed) {
                    float angle = 0f;
                    float distance;
                    Vector2f point1 = new Vector2f(ship.getLocation());
                    int bound = 100;
                    while (bound > 0) {
                        bound--;
                        angle = (float) Math.random() * 360f;
                        distance = (float) Math.random() * area * 0.75f;
                        point1 = MathUtils.getPointOnCircumference(ship.getLocation(), distance, angle);
                        if (CollisionUtils.isPointWithinBounds(point1, ship)) {
                            break;
                        }
                    }
                    angle = ship.getFacing() + (MathUtils.getShortestRotation(angle, ship.getFacing()) > 0f
                            ? (float) Math.random() * 30f + 10f : (float) Math.random() * -30f - 10f);
                    distance = ((float) Math.random() + 1f) * ship.getVelocity().length() / 7.5f;
                    Vector2f point2 = MathUtils.getPointOnCircumference(point1, -distance, angle);
                    engine.spawnEmpArc(ship, point1, new SimpleEntity(point1), new SimpleEntity(point2),
                            DamageType.ENERGY, 0f, 0f, 2000f, null,
                            (float) Math.random() * 10f + 10f, COLOR3, COLOR1);
                }

                stats.getAcceleration().modifyMult(id, 1f);
                stats.getAcceleration().modifyFlat(id, effectLevel * 100f);
                stats.getAcceleration().modifyPercent(id, effectLevel * 100f);
                stats.getMaxSpeed().modifyFlat(id, effectLevel * 150f);
                stats.getMaxSpeed().modifyPercent(id, effectLevel * 150f);
                stats.getDeceleration().modifyMult(id, 0f);
                stats.getTurnAcceleration().modifyMult(id, 1f);
                stats.getTurnAcceleration().modifyFlat(id, effectLevel * 40f);
                stats.getTurnAcceleration().modifyPercent(id, effectLevel * 100f);
                stats.getMaxTurnRate().modifyFlat(id, effectLevel * 15f);
                stats.getMaxTurnRate().modifyPercent(id, effectLevel * 50f);
                stats.getKineticDamageTakenMult().modifyMult(id, 1f - effectLevel * 0.75f);
                stats.getEnergyDamageTakenMult().modifyMult(id, 1f - effectLevel * 0.25f);

                ship.addAfterimage(COLOR4, 0f, 0f, -ship.getVelocity().x, -ship.getVelocity().y, 0f, 0f, 0.05f, 0.5f, true, false, false);

                if (force > 0f) {
                    List<ShipAPI> nearbyEnemies = CombatUtils.getShipsWithinRange(ship.getLocation(), area);
                    for (ShipAPI thisEnemy : nearbyEnemies) {
                        if (thisEnemy.getCollisionClass() == CollisionClass.NONE || thisEnemy == ship) {
                            continue;
                        }

                        float falloff = 1f - MathUtils.getDistance(ship, thisEnemy) / area;
                        falloff *= Math.abs(MathUtils.getShortestRotation(ship.getFacing(), VectorUtils.getAngle(ship.getLocation(), thisEnemy.getLocation()))) / 180f;
                        if (thisEnemy.getOwner() == ship.getOwner()) {
                            falloff *= 0.5f;
                        }

                        TEM_Util.applyForce(thisEnemy, VectorUtils.getDirectionalVector(ship.getLocation(), thisEnemy.getLocation()), force * falloff);
                    }

                    List<CombatEntityAPI> nearbyAsteroids = CombatUtils.getAsteroidsWithinRange(ship.getLocation(), area);
                    for (CombatEntityAPI asteroid : nearbyAsteroids) {
                        float falloff = 1f - MathUtils.getDistance(ship, asteroid) / area;
                        falloff *= Math.abs(MathUtils.getShortestRotation(ship.getFacing(), VectorUtils.getAngle(ship.getLocation(), asteroid.getLocation()))) / 180f;

                        TEM_Util.applyForce(asteroid, VectorUtils.getDirectionalVector(ship.getLocation(), asteroid.getLocation()), force * falloff);
                    }
                }
            } else {
                if (light != null) {
                    light.fadeOut(1f);
                    light = null;
                }

                if (!ended) {
                    Global.getSoundPlayer().playSound("tem_holycharge_deactivate", 1f, 1f, ship.getLocation(), ZERO);
                    ended = true;
                }

                stats.getMaxSpeed().unmodify(id);
                stats.getMaxTurnRate().unmodify(id);
                stats.getDeceleration().modifyMult(id, 1f);
                stats.getDeceleration().modifyFlat(id, 150f);
                stats.getAcceleration().modifyFlat(id, effectLevel * 100f);
                stats.getAcceleration().modifyPercent(id, effectLevel * 100f);
                stats.getTurnAcceleration().modifyFlat(id, effectLevel * 40f);
                stats.getTurnAcceleration().modifyPercent(id, effectLevel * 100f);
                stats.getMaxTurnRate().modifyFlat(id, effectLevel * 15f);
                stats.getMaxTurnRate().modifyPercent(id, effectLevel * 50f);
                stats.getKineticDamageTakenMult().modifyMult(id, 1f - effectLevel * 0.75f);
                stats.getEnergyDamageTakenMult().modifyMult(id, 1f - effectLevel * 0.25f);
            }
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0 && state == State.IN) {
            return new StatusData("engine stalled", true);
        } else if (index == 0 && state != State.IN) {
            return new StatusData("increased engine power", false);
        } else if (index == 1 && state != State.IN) {
            return new StatusData((int) (effectLevel * 75f) + "% kinetic damage reduction", false);
        } else if (index == 2 && state != State.IN) {
            return new StatusData((int) (effectLevel * 25f) + "% energy damage reduction", false);
        }
        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship = (ShipAPI) stats.getEntity();
        activated = false;
        started = false;
        ended = false;
        if (light != null) {
            light.fadeOut(1f);
            light = null;
        }
        stats.getAcceleration().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
        stats.getDeceleration().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getKineticDamageTakenMult().unmodify(id);
        stats.getEnergyDamageTakenMult().unmodify(id);
        if (ship != null) {
            if (!Global.getCombatEngine().getCustomData().containsKey(DATA_KEY)) {
                Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
            }
            final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
            if (localData != null) {
                final Map<ShipAPI, Float> acting = localData.acting;

                acting.remove(ship);
            }
        }
    }

    private static final class LocalData {

        final Map<ShipAPI, Float> acting = new HashMap<>(20);
        final Map<ShipAPI, Float> coolingDown = new HashMap<>(20);
    }
}
