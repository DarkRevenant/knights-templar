package data.scripts.shipsystems.ai;

import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_DivineReachAI implements ShipSystemAIScript {

    private CombatEngineAPI engine;

    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private float stickiness;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.2f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        tracker.advance(amount);
        stickiness -= amount;

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting() || stickiness > 0) {
                return;
            }

            boolean useSystem = false;

            WeaponAPI lancelot = null;
            for (WeaponAPI weapon : ship.getAllWeapons()) {
                if (weapon.getId().contentEquals("tem_lancelot")) {
                    lancelot = weapon;
                    break;
                }
            }
            if (lancelot == null) {
                return;
            }

            float normalRange = lancelot.getRange();
            if (ship.getSystem().isActive()) {
                normalRange -= 800f;
            }
            float extendedRange = normalRange + 800f - 100f;
            List<ShipAPI> directTargets = CombatUtils.getShipsWithinRange(ship.getLocation(), extendedRange);
            if (!directTargets.isEmpty()) {
                Vector2f endpoint = new Vector2f(extendedRange, 0f);
                VectorUtils.rotate(endpoint, ship.getFacing(), endpoint);
                Vector2f.add(endpoint, ship.getLocation(), endpoint);

                Collections.sort(directTargets, new CollectionUtils.SortEntitiesByDistance(ship.getLocation()));
                ListIterator<ShipAPI> iter = directTargets.listIterator();
                while (iter.hasNext()) {
                    ShipAPI tmp = iter.next();
                    if (tmp != ship && tmp.getCollisionClass() != CollisionClass.NONE &&
                            !tmp.isFighter() && !tmp.isDrone() &&
                            tmp.getLocation() != null && ship.getLocation() != null) {
                        if (CollisionUtils.getCollides(ship.getLocation(), endpoint,
                                                       tmp.getLocation(), tmp.getCollisionRadius())) {
                            if (MathUtils.getDistance(ship, tmp) <= normalRange - 100f) {
                                useSystem = false;
                                break;
                            } else {
                                if (tmp.getOwner() != ship.getOwner() && tmp.isAlive()) {
                                    useSystem = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            useSystem &= !flags.hasFlag(AIFlags.DO_NOT_USE_FLUX) && !flags.hasFlag(AIFlags.TURN_QUICKLY);
            if (useSystem ^ system.isOn()) {
                ship.useSystem();
                stickiness = 2f;
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;
    }
}
