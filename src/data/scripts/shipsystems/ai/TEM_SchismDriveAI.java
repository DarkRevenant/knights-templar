package data.scripts.shipsystems.ai;

import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatAssignmentType;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatFleetManagerAPI.AssignmentInfo;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import data.scripts.hullmods.TEM_LatticeShield;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class TEM_SchismDriveAI implements ShipSystemAIScript {

    private static final float SECONDS_TO_LOOK_AHEAD = 1f;

    private static Vector2f intercept(Vector2f point, float speed, Vector2f target, Vector2f targetVel) {
        Vector2f difference = new Vector2f(target.x - point.x, target.y - point.y);

        float a = targetVel.x * targetVel.x + targetVel.y * targetVel.y - speed * speed;
        float b = 2 * (targetVel.x * difference.x + targetVel.y * difference.y);
        float c = difference.x * difference.x + difference.y * difference.y;

        Vector2f solutionSet = quad(a, b, c);

        Vector2f intercept = null;
        if (solutionSet != null) {
            float bestFit = Math.min(solutionSet.x, solutionSet.y);
            if (bestFit < 0) {
                bestFit = Math.max(solutionSet.x, solutionSet.y);
            }
            if (bestFit > 0) {
                intercept = new Vector2f(target.x + targetVel.x * bestFit, target.y + targetVel.y * bestFit);
            }
        }

        return intercept;
    }

    private static Vector2f quad(float a, float b, float c) {
        Vector2f solution = null;
        if (Float.compare(Math.abs(a), 0) == 0) {
            if (Float.compare(Math.abs(b), 0) == 0) {
                solution = (Float.compare(Math.abs(c), 0) == 0) ? new Vector2f(0, 0) : null;
            } else {
                solution = new Vector2f(-c / b, -c / b);
            }
        } else {
            float d = b * b - 4 * a * c;
            if (d >= 0) {
                d = (float) Math.sqrt(d);
                float e = 2 * a;
                solution = new Vector2f((-b - d) / e, (-b + d) / e);
            }
        }
        return solution;
    }

    private CombatEngineAPI engine;

    private final CollectionUtils.CollectionFilter<DamagingProjectileAPI> filterMisses = new CollectionUtils.CollectionFilter<DamagingProjectileAPI>() {
        @Override
        public boolean accept(DamagingProjectileAPI proj) {
            if (proj.getOwner() == ship.getOwner() && (!(proj instanceof MissileAPI) || !((MissileAPI) proj).isFizzling())) {
                return false;
            }

            if (proj instanceof MissileAPI) {
                MissileAPI missile = (MissileAPI) proj;
                if (missile.isFlare()) {
                    return false;
                }
            }

            return (CollisionUtils.getCollides(proj.getLocation(), Vector2f.add(proj.getLocation(),
                    (Vector2f) new Vector2f(proj.getVelocity()).scale(SECONDS_TO_LOOK_AHEAD), null), ship.getLocation(), ship.getCollisionRadius() + 50f)
                    && Math.abs(MathUtils.getShortestRotation(proj.getFacing(), VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f);
        }
    };

    private ShipwideAIFlags flags;
    private ShipAPI ship;

    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.2f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        if (ship.getSystem().isActive()) {
            flags.setFlag(AIFlags.DO_NOT_VENT);
        }

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (!AIUtils.canUseSystemThisFrame(ship)) {
                return;
            }

            List<DamagingProjectileAPI> nearbyThreats = new ArrayList<>(500);
            for (DamagingProjectileAPI tmp : engine.getProjectiles()) {
                if (MathUtils.isWithinRange(tmp.getLocation(), ship.getLocation(), ship.getCollisionRadius() * 5f)) {
                    nearbyThreats.add(tmp);
                }
            }
            nearbyThreats = CollectionUtils.filter(nearbyThreats, filterMisses);
            List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, ship.getCollisionRadius() * 2.5f);
            for (MissileAPI missile : nearbyMissiles) {
                if (!missile.getEngineController().isTurningLeft() && !missile.getEngineController().isTurningRight()) {
                    continue;
                }

                nearbyThreats.add(missile);
            }

            float decisionLevel = 0f;
            float damageReduction = TEM_LatticeShield.shieldLevel(ship);
            for (DamagingProjectileAPI threat : nearbyThreats) {
                if (threat.getDamageType() == DamageType.FRAGMENTATION) {
                    decisionLevel += Math.pow((1f - damageReduction) * (threat.getDamageAmount() + threat.getEmpAmount() * 0.25f) / 400f, 1.3f);
                } else {
                    decisionLevel += Math.pow((1f - damageReduction) * (threat.getDamageAmount() + threat.getEmpAmount() * 0.25f) / 100f, 1.3f);
                }
            }
            List<BeamAPI> nearbyBeams = engine.getBeams();
            for (BeamAPI beam : nearbyBeams) {
                if (beam.getDamageTarget() == ship) {
                    float damage;
                    float emp = beam.getWeapon().getDerivedStats().getEmpPerSecond();
                    if (beam.getWeapon().getDerivedStats().getSustainedDps() < beam.getWeapon().getDerivedStats().getDps()) {
                        damage = beam.getWeapon().getDerivedStats().getBurstDamage() / beam.getWeapon().getDerivedStats().getBurstFireDuration();
                    } else {
                        damage = beam.getWeapon().getDerivedStats().getDps();
                    }
                    decisionLevel += Math.pow(
                            (1f - damageReduction) * ((damage * ((beam.getWeapon().getDamageType() == DamageType.FRAGMENTATION) ? 0.25f : 1f))
                            + (0.25f * emp)) / 100f, 1.3f);
                }
            }

            float range = 1300f;
            List<ShipAPI> directTargets = CombatUtils.getShipsWithinRange(ship.getLocation(), range);
            if (!directTargets.isEmpty()) {
                Vector2f endpoint = new Vector2f(ship.getVelocity());
                if (endpoint.lengthSquared() <= 5f) {
                    endpoint = ship.getMouseTarget();
                    Vector2f.sub(endpoint, ship.getLocation(), endpoint);
                    if (endpoint.lengthSquared() <= 0.01f) {
                        endpoint = new Vector2f(1f, 0f);
                    }
                }
                Misc.normalise(endpoint);
                endpoint.scale(range);
                Vector2f.add(endpoint, ship.getLocation(), endpoint);

                Collections.sort(directTargets, new CollectionUtils.SortEntitiesByDistance(ship.getLocation()));
                ListIterator<ShipAPI> iter = directTargets.listIterator();
                while (iter.hasNext()) {
                    ShipAPI tmp = iter.next();
                    if (!tmp.isHulk() && tmp != ship && tmp.getCollisionClass() != CollisionClass.NONE && !tmp.isFighter() && !tmp.isDrone()) {
                        Vector2f loc = intercept(ship.getLocation(), 1200f, tmp.getLocation(), tmp.getVelocity());

                        if (loc == null) {
                            Vector2f projection = new Vector2f(tmp.getVelocity());
                            float scalar = MathUtils.getDistance(tmp.getLocation(), ship.getLocation()) / 1200f;
                            projection.scale(scalar);
                            Vector2f.add(tmp.getLocation(), projection, loc);
                        }

                        if (loc != null && ship.getLocation() != null) {
                            float areaChange = 1f;
                            if (tmp.getOwner() == ship.getOwner()) {
                                areaChange *= 1.5f;
                            }
                            float scalar;
                            if (range - 100f >= MathUtils.getDistance(ship.getLocation(), loc) + tmp.getCollisionRadius()) {
                                scalar = 1f;
                            } else {
                                scalar = 0.25f;

                                if (tmp.isCapital() || tmp.isStation() || tmp.isStationModule()) {
                                    decisionLevel -= 30f;
                                }
                            }
                            if (ship.getShipTarget() != null && ship.getShipTarget() == tmp) {
                                scalar *= 1.5f;
                            }
                            if (CollisionUtils.getCollides(ship.getLocation(), endpoint, loc, tmp.getCollisionRadius() * 0.5f + ship.getCollisionRadius() * 0.75f * areaChange)) {
                                if (tmp.isFrigate()) {
                                    if (tmp.getOwner() == ship.getOwner()) {
                                        decisionLevel -= 3f * scalar;
                                    } else {
                                        decisionLevel += 3f * scalar;
                                    }
                                } else if (tmp.isDestroyer()) {
                                    if (tmp.getOwner() == ship.getOwner()) {
                                        decisionLevel -= 5f * scalar;
                                    } else {
                                        decisionLevel += 5f * scalar;
                                    }
                                } else if (tmp.isCruiser()) {
                                    if (tmp.getOwner() == ship.getOwner()) {
                                        decisionLevel -= 7f * scalar;
                                    } else {
                                        decisionLevel += 7f * scalar;
                                    }
                                } else {
                                    if (tmp.getOwner() == ship.getOwner()) {
                                        decisionLevel -= 9f * scalar;
                                    } else {
                                        decisionLevel += 9f * scalar;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Vector2f tmp = new Vector2f(ship.getVelocity());
            if (tmp.lengthSquared() <= 0.01f) {
                tmp = ship.getMouseTarget();
                Vector2f.sub(tmp, ship.getLocation(), tmp);
                if (tmp.lengthSquared() <= 0.01f) {
                    tmp = new Vector2f(1f, 0f);
                }
            }
            float shipDir = VectorUtils.getFacing(tmp);

            AssignmentInfo assignment = engine.getFleetManager(ship.getOwner()).getTaskManager(ship.isAlly()).getAssignmentFor(ship);
            Vector2f targetSpot;
            if (assignment != null && assignment.getTarget() != null) {
                targetSpot = assignment.getTarget().getLocation();
            } else {
                targetSpot = null;
            }

            if (assignment != null && assignment.getType() == CombatAssignmentType.RETREAT) {
                float retreatDirection = (ship.getOwner() == 0) ? 270f : 90f;
                if (Math.abs(MathUtils.getShortestRotation(shipDir, retreatDirection)) <= 60f) {
                    decisionLevel += 25f;
                } else if (Math.abs(MathUtils.getShortestRotation(shipDir, retreatDirection)) > 90f) {
                    decisionLevel -= 25f;
                }
            }

            if (flags.hasFlag(AIFlags.RUN_QUICKLY)) {
                decisionLevel += 10f;
            } else if (flags.hasFlag(AIFlags.PURSUING)) {
                decisionLevel *= 1.25f;
                decisionLevel += 5f;
            } else if (targetSpot != null && Math.abs(MathUtils.getShortestRotation(shipDir, VectorUtils.getAngle(ship.getLocation(), targetSpot))) <= 15f
                    && Math.abs(MathUtils.getShortestRotation(ship.getFacing(), VectorUtils.getAngle(ship.getLocation(), targetSpot))) <= 15f
                    && MathUtils.getDistance(ship, targetSpot) >= 1500f) {
                decisionLevel += 12.5f;
            } else if (ship.getShipTarget() != null && MathUtils.getDistance(ship.getShipTarget(), ship) >= 2000f) {
                decisionLevel += 5f;
                if (Math.abs(MathUtils.getShortestRotation(shipDir, VectorUtils.getAngle(ship.getLocation(), ship.getShipTarget().getLocation()))) <= 15f) {
                    decisionLevel += 5f;
                }
            }
            decisionLevel *= 1f - 0.5f * (ship.getFluxTracker().getCurrFlux() + ship.getFluxTracker().getHardFlux()) / ship.getFluxTracker().getMaxFlux();
            if (flags.hasFlag(AIFlags.TURN_QUICKLY)) {
                decisionLevel *= 0.5f;
            }
            if (flags.hasFlag(AIFlags.BACK_OFF) || flags.hasFlag(AIFlags.BACK_OFF_MIN_RANGE) || flags.hasFlag(AIFlags.BACKING_OFF)) {
                decisionLevel *= 0.75f;
            }
            if (flags.hasFlag(AIFlags.DO_NOT_USE_FLUX)) {
                decisionLevel *= 0.25f;
            }

            if (decisionLevel >= 8f) {
                ship.useSystem();
                flags.setFlag(AIFlags.DO_NOT_VENT);
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
    }
}
