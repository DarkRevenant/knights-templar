package data.scripts.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_SchismDriveStats extends BaseShipSystemScript {

    private static final Color COLOR1 = new Color(50, 255, 255);
    private static final Color COLOR2 = new Color(0, 255, 255, 100);
    private static final Color COLOR3 = new Color(50, 255, 255, 150);
    private static final Color COLOR4 = new Color(150, 255, 255);

    private static final String DATA_KEY = "TEM_SchismDrive";

    private static final Vector2f ZERO = new Vector2f();

    public static float effectLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> acting = localData.acting;

        if (acting.containsKey(ship)) {
            return acting.get(ship);
        } else {
            return 0f;
        }
    }

    private final IntervalUtil interval = new IntervalUtil(0.033f, 0.033f);
    private StandardLight light = null;
    private boolean started = false;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (!engine.getCustomData().containsKey(DATA_KEY)) {
            engine.getCustomData().put(DATA_KEY, new LocalData());
        }

        float objectiveAmount = engine.getElapsedInLastFrame() * Global.getCombatEngine().getTimeMult().getModifiedValue();
        interval.advance(objectiveAmount);
        boolean intervalElapsed = interval.intervalElapsed();

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, Float> acting = localData.acting;

        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (ship != null) {
            if (effectLevel > 0f) {
                acting.put(ship, effectLevel);
            }

            if (state == State.IN && effectLevel > 0f) {
                if (light == null) {
                    light = new StandardLight(ZERO, ZERO, ZERO, ship);
                    light.setColor(COLOR1);
                    light.setIntensity(2f);
                    light.setSize(350f);
                    light.fadeIn(0.3f);
                    LightShader.addLight(light);
                }

                if (!started) {
                    started = true;
                }

                ship.setCollisionClass(CollisionClass.NONE);
                float speed = ship.getVelocity().length();
                if (speed <= 5f) {
                    ship.getVelocity().set(VectorUtils.getDirectionalVector(ship.getLocation(), ship.getMouseTarget()));
                }
                if (speed < 1200f) {
                    Misc.normalise(ship.getVelocity());
                    ship.getVelocity().scale(Math.min(1200f, speed + objectiveAmount * 3600f));
                }
            } else if (state == State.ACTIVE) {
                if (light != null) {
                    light.setIntensity(1.8f + (float) Math.random() * 0.4f);
                    light.setSize(325f + (float) Math.random() * 50f);
                }

                ship.setCollisionClass(CollisionClass.NONE);
                float speed = ship.getVelocity().length();
                if (speed < 1200f) {
                    if (speed < 1f) {
                        ship.getVelocity().set(MathUtils.getPointOnCircumference(null, 1f, ship.getFacing()));
                    }
                    Misc.normalise(ship.getVelocity());
                    ship.getVelocity().scale(Math.min(1200f, speed + objectiveAmount * 3600f));
                }

                if (intervalElapsed) {
                    Vector2f point1a = new Vector2f(-4f, 21f);
                    VectorUtils.rotate(point1a, ship.getFacing(), point1a);
                    Vector2f.add(point1a, ship.getLocation(), point1a);
                    Vector2f point1b = new Vector2f(-70f, 0f);
                    VectorUtils.rotate(point1b,
                            VectorUtils.getFacing(ship.getVelocity()) + (float) Math.random() * 20f
                            - 10f, point1b);
                    Vector2f.add(point1a, point1b, point1b);
                    Vector2f point2a = new Vector2f(-4f, -21f);
                    VectorUtils.rotate(point2a, ship.getFacing(), point2a);
                    Vector2f.add(point2a, ship.getLocation(), point2a);
                    Vector2f point2b = new Vector2f(-70f, 0f);
                    VectorUtils.rotate(point2b,
                            VectorUtils.getFacing(ship.getVelocity()) + (float) Math.random() * 20f
                            - 10f, point2b);
                    Vector2f.add(point2a, point2b, point2b);

                    Global.getCombatEngine().spawnEmpArc(ship, point1a, new SimpleEntity(point1a), new SimpleEntity(
                            point1b), DamageType.ENERGY, 0f, 0f, 1000f, null,
                            20f + (float) Math.random() * 10f, COLOR2, COLOR3);
                    Global.getCombatEngine().spawnEmpArc(ship, point2a, new SimpleEntity(point2a), new SimpleEntity(
                            point2b), DamageType.ENERGY, 0f, 0f, 1000f, null,
                            20f + (float) Math.random() * 10f, COLOR2, COLOR3);

                    float shipRadius = TEM_Util.effectiveRadius(ship);

                    List<ShipAPI> targets = CombatUtils.getShipsWithinRange(ship.getLocation(),
                            ship.getCollisionRadius());
                    for (ShipAPI target : targets) {
                        if (target != ship && target.getCollisionClass() != CollisionClass.NONE) {
                            Vector2f damagePoint;
                            int checker = 10;
                            while (true) {
                                damagePoint
                                        = MathUtils.getRandomPointInCircle(ship.getLocation(), ship.getCollisionRadius());
                                if (CollisionUtils.isPointWithinBounds(damagePoint, target) || checker <= 0) {
                                    break;
                                }
                                checker--;
                            }
                            if (checker <= 0) {
                                damagePoint = CollisionUtils.getCollisionPoint(ship.getLocation(), target.getLocation(),
                                        target);
                                if (damagePoint == null) {
                                    continue;
                                }
                            }
                            float damage = interval.getIntervalDuration() * 5000f;
                            float emp = interval.getIntervalDuration() * 10000f;
                            if (target.getOwner() == ship.getOwner()) {
                                damage *= 0.5f;
                            }
                            Global.getCombatEngine().applyDamage(target, damagePoint, damage, DamageType.ENERGY, emp,
                                    false, false, ship, false);
                            Global.getSoundPlayer().playSound("tem_schismdrive_impact", 1f + (float) Math.random()
                                    * 0.1f, 0.4f + (float) Math.random() * 0.2f,
                                    ship.getLocation(), target.getVelocity());
                            Global.getCombatEngine().addHitParticle(ship.getLocation(), ZERO,
                                    shipRadius * 3f,
                                    0.2f, 0.5f, COLOR4);
                        }
                    }
                }
            } else {
                if (light != null) {
                    light.fadeOut(0.3f);
                    light = null;
                }

                ship.setCollisionClass(CollisionClass.NONE);
                float speed = ship.getVelocity().length();
                if (speed > ship.getMutableStats().getMaxSpeed().getModifiedValue()) {
                    Misc.normalise(ship.getVelocity());
                    ship.getVelocity().scale(Math.max(ship.getMutableStats().getMaxSpeed().getModifiedValue(), speed - objectiveAmount * 3600f));
                }
            }
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("ripping through space", false);
        }
        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (light != null) {
            light.fadeOut(0.3f);
            light = null;
        }
        started = false;
        if (ship != null) {
            if (!Global.getCombatEngine().getCustomData().containsKey(DATA_KEY)) {
                Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
            }
            final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
            if (localData != null) {
                final Map<ShipAPI, Float> acting = localData.acting;

                acting.remove(ship);
            }
            ship.setCollisionClass(CollisionClass.SHIP);
            float speed = ship.getVelocity().length();
            if (speed > ship.getMutableStats().getMaxSpeed().getModifiedValue()) {
                Misc.normalise(ship.getVelocity());
                ship.getVelocity().scale(ship.getMutableStats().getMaxSpeed().getModifiedValue());
            }
        }
    }

    private static final class LocalData {

        final Map<ShipAPI, Float> acting = new HashMap<>(50);
    }
}
