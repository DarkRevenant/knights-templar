package data.scripts.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import java.util.HashMap;
import java.util.Map;

public class TEM_DivineReachStats extends BaseShipSystemScript {

    private static final float ACCEL_PENALTY_MULT = 0.5f;
    private static final String DATA_KEY = "TEM_DivineReach";
    private static final float RANGE_BONUS_PERCENT = 100f;
    private static final float SPEED_PENALTY_MULT = 0.75f;
    private static final float TURN_PENALTY_MULT = 0.25f;

    public static float effectLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> acting = localData.acting;

        if (acting.containsKey(ship)) {
            return acting.get(ship);
        } else {
            return 0f;
        }
    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (!engine.getCustomData().containsKey(DATA_KEY)) {
            engine.getCustomData().put(DATA_KEY, new LocalData());
        }

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, Float> acting = localData.acting;

        stats.getAcceleration().modifyMult(id, 1f - effectLevel * (1f - ACCEL_PENALTY_MULT));
        stats.getTurnAcceleration().modifyMult(id, 1f - effectLevel * (1f - ACCEL_PENALTY_MULT));
        stats.getMaxSpeed().modifyMult(id, 1f - effectLevel * (1f - SPEED_PENALTY_MULT));
        stats.getMaxTurnRate().modifyMult(id, 1f - effectLevel * (1f - TURN_PENALTY_MULT));
        stats.getBallisticWeaponRangeBonus().modifyPercent(id, effectLevel * RANGE_BONUS_PERCENT);
        stats.getEnergyWeaponRangeBonus().modifyPercent(id, effectLevel * RANGE_BONUS_PERCENT);

        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (ship != null) {
            if (effectLevel > 0f) {
                acting.put(ship, effectLevel);
            }
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("+" + (int) (effectLevel * RANGE_BONUS_PERCENT) + "% weapon range", false);
        } else if (index == 1) {
            return new StatusData("reduced speed and maneuverability", true);
        }
        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getAcceleration().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getBallisticWeaponRangeBonus().unmodify(id);
        stats.getEnergyWeaponRangeBonus().unmodify(id);

        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (ship != null) {
            if (!Global.getCombatEngine().getCustomData().containsKey(DATA_KEY)) {
                Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
            }
            final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
            if (localData != null) {
                final Map<ShipAPI, Float> acting = localData.acting;

                acting.remove(ship);
            }
        }
    }

    private static final class LocalData {

        final Map<ShipAPI, Float> acting = new HashMap<>(50);
    }
}
