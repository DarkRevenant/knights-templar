package data.scripts.shipsystems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lwjgl.util.vector.Vector2f;

public class TEM_RiastradStats extends BaseShipSystemScript {

    private static final Color COLOR1 = new Color(246, 238, 139);

    private static final float DAMAGE_BONUS_PERCENT = 50f;
    private static final String DATA_KEY = "TEM_Riastrad";
    private static final float RANGE_BONUS_PERCENT = 50f;
    private static final float SPEED_BONUS_PERCENT = 50f;

    private static final Vector2f ZERO = new Vector2f();

    public static float effectLevel(ShipAPI ship) {
        final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
        if (localData == null) {
            return 0f;
        }

        final Map<ShipAPI, Float> acting = localData.acting;

        if (acting.containsKey(ship)) {
            return acting.get(ship);
        } else {
            return 0f;
        }
    }

    private StandardLight light = null;
    private boolean started = false;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (!engine.getCustomData().containsKey(DATA_KEY)) {
            engine.getCustomData().put(DATA_KEY, new LocalData());
        }

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, Float> acting = localData.acting;

        stats.getEnergyWeaponDamageMult().modifyPercent(id, effectLevel * DAMAGE_BONUS_PERCENT);
        stats.getEnergyWeaponRangeBonus().modifyPercent(id, effectLevel * RANGE_BONUS_PERCENT);
        stats.getBallisticWeaponDamageMult().modifyPercent(id, effectLevel * DAMAGE_BONUS_PERCENT);
        stats.getBallisticWeaponRangeBonus().modifyPercent(id, effectLevel * RANGE_BONUS_PERCENT);
        stats.getAcceleration().modifyPercent(id, effectLevel * SPEED_BONUS_PERCENT);
        stats.getDeceleration().modifyPercent(id, effectLevel * SPEED_BONUS_PERCENT);
        stats.getMaxSpeed().modifyPercent(id, effectLevel * SPEED_BONUS_PERCENT);
        stats.getTurnAcceleration().modifyPercent(id, effectLevel * SPEED_BONUS_PERCENT);
        stats.getMaxTurnRate().modifyPercent(id, effectLevel * SPEED_BONUS_PERCENT);

        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (ship != null) {
            if (effectLevel > 0f) {
                acting.put(ship, effectLevel);
            }

            if (state == State.IN && effectLevel > 0f) {
                if (light == null) {
                    light = new StandardLight(ZERO, ZERO, ZERO, ship);
                    light.setColor(COLOR1);
                    light.setIntensity(0.75f);
                    light.setSize(250f);
                    light.fadeIn(0.25f);
                    LightShader.addLight(light);
                }

                if (!started) {
                    started = true;
                }
            } else if (state == State.ACTIVE) {
                if (light != null) {
                    light.setIntensity(0.6f + (float) Math.random() * 0.3f);
                    light.setSize(225f + (float) Math.random() * 50f);
                }
            } else {
                if (light != null) {
                    light.fadeOut(0.5f);
                    light = null;
                }
            }
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("+" + (int) (effectLevel * 50f) + "% energy weapon damage", false);
        } else if (index == 1) {
            return new StatusData("+" + (int) (effectLevel * 50f) + "% energy weapon range", false);
        } else if (index == 2) {
            return new StatusData("+" + (int) (effectLevel * 50f) + "% speed and maneuverability", false);
        }
        return null;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getEnergyWeaponDamageMult().unmodify(id);
        stats.getEnergyWeaponRangeBonus().unmodify(id);
        stats.getBallisticWeaponDamageMult().unmodify(id);
        stats.getBallisticWeaponRangeBonus().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);

        ShipAPI ship = (ShipAPI) stats.getEntity();
        if (light != null) {
            light.fadeOut(0.5f);
            light = null;
        }
        started = false;
        if (ship != null) {
            if (!Global.getCombatEngine().getCustomData().containsKey(DATA_KEY)) {
                Global.getCombatEngine().getCustomData().put(DATA_KEY, new LocalData());
            }
            final LocalData localData = (LocalData) Global.getCombatEngine().getCustomData().get(DATA_KEY);
            if (localData != null) {
                final Map<ShipAPI, Float> acting = localData.acting;

                acting.remove(ship);
            }
        }
    }

    private static final class LocalData {

        final Map<ShipAPI, Float> acting = new HashMap<>(50);
    }
}
