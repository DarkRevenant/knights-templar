package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.campaign.CampaignPlugin.PickPriority;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAIConfig;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Personalities;
import com.thoughtworks.xstream.XStream;
import data.scripts.campaign.TEM_CampaignPluginImpl;
import data.scripts.campaign.TEM_FleetCheating;
import data.scripts.campaign.econ.TEM_Avalon;
import data.scripts.everyframe.TEM_BlockedHullmodDisplayScript;
import data.scripts.everyframe.TEM_PriwenBurstPlugin;
import data.scripts.weapons.ai.TEM_ClarentAI;
import data.scripts.weapons.ai.TEM_CrucifixAI;
import data.scripts.weapons.ai.TEM_RolandAI;
import data.scripts.weapons.ai.TEM_TrucidareAI;
import data.scripts.world.templars.TEM_Antioch;
import java.io.IOException;
import java.util.Set;
import org.json.JSONException;

public class TEMModPlugin extends BaseModPlugin {

    public static final String CLARENT_SRM_ID = "tem_clarent_srm";
    public static final String CRUCIFIX_MRM_ID = "tem_crucifix_mrm";
    public static final String MARTYR_ID = "tem_martyr";
    public static final String ROLAND_MIRV_ID = "tem_roland_mirv";
    public static final String TRUCIDARE_LRM_ID = "tem_trucidare_lrm";

    public static boolean hasGraphicsLib = false;
    public static boolean hasMagicLib = false;
    public static boolean isExerelin = false;

    @Override
    public void configureXStream(XStream x) {
        x.alias("TEM_Antioch", TEM_Antioch.class);
        x.alias("TEM_Avalon", TEM_Avalon.class);
        x.alias("TEM_FleetCheating", TEM_FleetCheating.class);
        x.alias("TEM_CampaignPluginImpl", TEM_CampaignPluginImpl.class);
    }

    @Override
    public void onApplicationLoad() throws IOException, JSONException {
        isExerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        hasGraphicsLib = Global.getSettings().getModManager().isModEnabled("shaderLib");
        hasMagicLib = Global.getSettings().getModManager().isModEnabled("MagicLib");

        if (hasGraphicsLib) {
            TEM_ModPluginAlt.initShaderLib();
        }

        TEM_PriwenBurstPlugin.reloadSettings();
    }

    @Override
    public void onGameLoad(boolean newGame) {
        Global.getSector().addTransientScript(new TEM_BlockedHullmodDisplayScript());

        TEM_ModPluginAlt.reloadTEM();

        /* Prevent memory/save file buildup */
        if (Global.getSector().getPersistentData().containsKey("tem_excaliburcore_explosion")) {
            Set<FleetMemberAPI> explodedMembers = (Set<FleetMemberAPI>) Global.getSector().getPersistentData().get("tem_excaliburcore_explosion");
            explodedMembers.clear();
        }
    }

    @Override
    public void onNewGame() {
        TEM_ModPluginAlt.initTEM();
    }

    @Override
    public void onNewGameAfterProcGen() {
        TEM_ModPluginAlt.genTEM();
    }

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        switch (missile.getProjectileSpecId()) {
            case CLARENT_SRM_ID:
                return new PluginPick<MissileAIPlugin>(new TEM_ClarentAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case CRUCIFIX_MRM_ID:
                return new PluginPick<MissileAIPlugin>(new TEM_CrucifixAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case TRUCIDARE_LRM_ID:
                return new PluginPick<MissileAIPlugin>(new TEM_TrucidareAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case ROLAND_MIRV_ID:
                return new PluginPick<MissileAIPlugin>(new TEM_RolandAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            default:
        }
        return null;
    }

    @Override
    public PluginPick<ShipAIPlugin> pickShipAI(FleetMemberAPI member, ShipAPI ship) {
        switch (ship.getHullSpec().getHullId()) {
            case MARTYR_ID: {
                ShipAIConfig config = new ShipAIConfig();
                config.alwaysStrafeOffensively = true;
                if ((ship.getOriginalOwner() != 0) || ship.isAlly()) {
                    config.personalityOverride = Personalities.RECKLESS;
                }

                return new PluginPick<>(Global.getSettings().createDefaultShipAI(ship, config), PickPriority.MOD_SET);
            }
            default:
        }
        return null;
    }
}
