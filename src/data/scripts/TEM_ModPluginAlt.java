package data.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.procgen.ProcgenUsedNames;
import data.scripts.campaign.TEM_CampaignPluginImpl;
import data.scripts.campaign.TEM_FleetCheating;
import data.scripts.shaders.TEM_LatticeShieldShader;
import data.scripts.shaders.TEM_TemplarShader;
import data.scripts.world.templars.TEM_Antioch;
import exerelin.campaign.SectorManager;
import org.dark.shaders.light.LightData;
import org.dark.shaders.util.ShaderLib;
import org.dark.shaders.util.TextureData;

public class TEM_ModPluginAlt {

    private static void initFactionRelationships(SectorAPI sector) {
        FactionAPI kol = sector.getFaction(Factions.KOL);
        FactionAPI church = sector.getFaction(Factions.LUDDIC_CHURCH);
        FactionAPI path = sector.getFaction(Factions.LUDDIC_PATH);
        FactionAPI player = sector.getFaction(Factions.PLAYER);
        FactionAPI templars = sector.getFaction("templars");

        for (FactionAPI faction : sector.getAllFactions()) {
            if (faction != templars && !faction.isNeutralFaction()) {
                templars.setRelationship(faction.getId(), RepLevel.HOSTILE);
            }
        }

        templars.setRelationship(kol.getId(), RepLevel.VENGEFUL);
        templars.setRelationship(church.getId(), RepLevel.VENGEFUL);
        templars.setRelationship(path.getId(), RepLevel.VENGEFUL);
        templars.setRelationship(player.getId(), RepLevel.INHOSPITABLE);
    }

    static void genTEM() {
        if (TEMModPlugin.isExerelin && !SectorManager.getManager().isCorvusMode()) {
            return;
        }
        TEM_Antioch.generatePt2(Global.getSector());
    }

    static void initShaderLib() {
        ShaderLib.init();

        if (ShaderLib.areShadersAllowed() && ShaderLib.areBuffersAllowed()) {
            LightData.readLightDataCSV("data/lights/tem_light_data.csv");
            TextureData.readTextureDataCSV("data/lights/tem_texture_data.csv");
            ShaderLib.addShaderAPI(new TEM_LatticeShieldShader());
            ShaderLib.addShaderAPI(new TEM_TemplarShader());
        }
    }

    static void initTEM() {
        // This script includes the Templar free loot remover; amake sure it runs even in Nex's random sector
        Global.getSector().addScript(new TEM_FleetCheating());

        if (TEMModPlugin.isExerelin && !SectorManager.getManager().isCorvusMode()) {
            return;
        }

        ProcgenUsedNames.notifyUsed("Antioch");
        ProcgenUsedNames.notifyUsed("Ascalon");

        initFactionRelationships(Global.getSector());

        new TEM_Antioch().generate(Global.getSector());
    }

    static void reloadTEM() {
        if (TEMModPlugin.isExerelin && !SectorManager.getManager().isCorvusMode()) {
            return;
        }
        Global.getSector().registerPlugin(new TEM_CampaignPluginImpl());
    }
}
