package data.scripts.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ArmorGridAPI;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.MutableStat;
import com.fs.starfarer.api.combat.MutableStat.StatMod;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.campaign.DModManager;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import data.scripts.everyframe.TEM_BlockedHullmodDisplayScript;
import data.scripts.everyframe.TEM_PriwenBurstPlugin;
import data.scripts.shipsystems.TEM_AegisShieldStats;
import data.scripts.util.TEM_Util;
import java.awt.Color;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

public class TEM_LatticeShield extends BaseHullMod {

    public static final Color AEGIS_SHIELD_COLOR = new Color(50, 255, 255);
    public static final float CORONA_EFFECT_REDUCTION = 0.25f;
    public static final Color CRITICAL_SHIELD_COLOR = new Color(255, 100, 100);
    public static final float CRITICAL_THRESHOLD_CAPITAL = 0.55f;
    public static final float CRITICAL_THRESHOLD_CRUISER = 0.55f;
    public static final float CRITICAL_THRESHOLD_DESTROYER = 0.6f;
    public static final float CRITICAL_THRESHOLD_FRIGATE = 0.65f;
    public static final float DAMAGE_REDUCTION_CRITICAL_CAPITAL = 0.4f;
    public static final float DAMAGE_REDUCTION_CRITICAL_CRUISER = 0.45f;
    public static final float DAMAGE_REDUCTION_CRITICAL_DESTROYER = 0.5f;
    public static final float DAMAGE_REDUCTION_CRITICAL_FRIGATE = 0.6f;
    public static final float DAMAGE_REDUCTION_FULL_CAPITAL = 0.8f;
    public static final float DAMAGE_REDUCTION_FULL_CRUISER = 0.8f;
    public static final float DAMAGE_REDUCTION_FULL_DESTROYER = 0.9f;
    public static final float DAMAGE_REDUCTION_FULL_FRIGATE = 0.95f;
    public static final float EMPTY_THRESHOLD_CAPITAL = 0.7f;
    public static final float EMPTY_THRESHOLD_CRUISER = 0.7f;
    public static final float EMPTY_THRESHOLD_DESTROYER = 0.75f;
    public static final float EMPTY_THRESHOLD_FRIGATE = 0.8f;
    public static final float FLUX_PER_DAMAGE_CAPITAL = 1f;
    public static final float FLUX_PER_DAMAGE_CRUISER = 1f;
    public static final float FLUX_PER_DAMAGE_DESTROYER = 0.8f;
    public static final float FLUX_PER_DAMAGE_FRIGATE = 0.6f;
    public static final float HARD_FLUX_DISSIPATION_RATE_CAPITAL = 0.1f;
    public static final float HARD_FLUX_DISSIPATION_RATE_CRUISER = 0.2f;
    public static final float HARD_FLUX_DISSIPATION_RATE_DESTROYER = 0.3f;
    public static final float HARD_FLUX_DISSIPATION_RATE_FRIGATE = 0.4f;
    public static final float PIERCE_MULT = 0.5f;
    public static final Color VISUAL_SHIELD_COLOR = new Color(100, 200, 255);
    public static final float WARNING_THRESHOLD_CAPITAL = 0.4f;
    public static final float WARNING_THRESHOLD_CRUISER = 0.4f;
    public static final float WARNING_THRESHOLD_DESTROYER = 0.45f;
    public static final float WARNING_THRESHOLD_FRIGATE = 0.5f;
    public static final String id = "tem_latticeshield";

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(1);
    private static final String DATA_KEY = "TEM_LatticeShield";

    private static final Map<HullSize, Float> pitchBend = new EnumMap<>(HullSize.class);
    private static final Map<HullSize, Float> priwenVulnerabilityTime = new EnumMap<>(HullSize.class);
    private static final Map<ShipAPI, HealthState> shipHealth = new WeakHashMap<>(100);
    private static final Map<HullSize, Float> volume = new EnumMap<>(HullSize.class);

    static {
        BLOCKED_HULLMODS.add("frontshield");
    }

    static {
        priwenVulnerabilityTime.put(HullSize.FIGHTER, 3f);
        priwenVulnerabilityTime.put(HullSize.FRIGATE, 3f);
        priwenVulnerabilityTime.put(HullSize.DESTROYER, 4f);
        priwenVulnerabilityTime.put(HullSize.CRUISER, 5f);
        priwenVulnerabilityTime.put(HullSize.CAPITAL_SHIP, 6f);
        priwenVulnerabilityTime.put(HullSize.DEFAULT, 6f);
    }

    static {
        pitchBend.put(HullSize.FIGHTER, 1.125f);
        pitchBend.put(HullSize.FRIGATE, 1f);
        pitchBend.put(HullSize.DESTROYER, 0.9f);
        pitchBend.put(HullSize.CRUISER, 0.825f);
        pitchBend.put(HullSize.CAPITAL_SHIP, 0.775f);
        pitchBend.put(HullSize.DEFAULT, 0.775f);
    }

    static {
        volume.put(HullSize.FIGHTER, 0.4f);
        volume.put(HullSize.FRIGATE, 0.75f);
        volume.put(HullSize.DESTROYER, 1f);
        volume.put(HullSize.CRUISER, 1.22f);
        volume.put(HullSize.CAPITAL_SHIP, 1.4f);
        volume.put(HullSize.DEFAULT, 1.4f);
    }

    public static float computeStat(MutableStat stat, float base) {
        float flatMod = 0;
        float percentMod = 0;
        float mult = 1f;
        for (StatMod mod : stat.getPercentMods().values()) {
            percentMod += mod.value;
        }
        for (StatMod mod : stat.getFlatMods().values()) {
            flatMod += mod.value;
        }
        for (StatMod mod : stat.getMultMods().values()) {
            mult *= mod.value;
        }

        float modified = base + base * percentMod / 100f + flatMod;
        if (modified < 0) {
            modified = 0;
        }
        modified *= mult;
        return modified;
    }

    public static float getDModScale(ShipAPI ship) {
        if (ship == null) {
            return 1f;
        }
        int dmods = DModManager.getNumDMods(ship.getVariant());
        if (dmods > 0) {
            return Math.max(0.25f, 0.9f - dmods * 0.1f);
        } else {
            return 1f;
        }
    }

    public static float shieldLevel(ShipAPI ship) {
        if (shipHealth.containsKey(ship)) {
            return shipHealth.get(ship).damageReduction;
        } else {
            return 0f;
        }
    }

    private static float[][] armorArray(ShipAPI ship) {
        if (ship == null || !Global.getCombatEngine().isEntityInPlay(ship)) {
            return null;
        }
        ArmorGridAPI armorGrid = ship.getArmorGrid();
        float[][] armorArray = new float[ship.getArmorGrid().getGrid().length][ship.getArmorGrid().getGrid()[0].length];
        for (int x = 0; x < armorGrid.getGrid().length; x++) {
            for (int y = 0; y < armorGrid.getGrid()[x].length; y++) {
                armorArray[x][y] = armorGrid.getArmorValue(x, y);
            }
        }
        return armorArray;
    }

    private static float armorLevel(float armorArray[][]) {
        if (armorArray == null) {
            return 0f;
        }
        float total = 0f;
        for (float[] row : armorArray) {
            for (float cell : row) {
                total += cell;
            }
        }
        return total;
    }

    private static float armorLevelFrac(ShipAPI ship) {
        if (ship == null || !Global.getCombatEngine().isEntityInPlay(ship)) {
            return 0f;
        }
        float current = 0f;
        float total = 0f;
        ArmorGridAPI armorGrid = ship.getArmorGrid();
        for (int x = 0; x < armorGrid.getGrid().length; x++) {
            for (int y = 0; y < armorGrid.getGrid()[x].length; y++) {
                current += armorGrid.getArmorFraction(x, y);
                total += 1f;
            }
        }
        return current / total;
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (!engine.getCustomData().containsKey(DATA_KEY)) {
            engine.getCustomData().put(DATA_KEY, new LocalData());
        }

        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, Object[]> uiKey = localData.uiKey;

        if (!Global.getCombatEngine().isEntityInPlay(ship)) {
            return;
        }

        float shipRadius = TEM_Util.effectiveRadius(ship);

        float scale = getDModScale(ship);

        float aegis = TEM_AegisShieldStats.effectLevel(ship);

        float priwenLevel = TEM_PriwenBurstPlugin.powerLevel(ship);
        float priwenCooldown = TEM_PriwenBurstPlugin.trueCooldownTime(ship);
        boolean priwenVulnerable = (priwenCooldown <= priwenVulnerabilityTime.get(ship.getHullSize()) * (priwenLevel
                + 0.5f) / 1.5f)
                && priwenCooldown >= 0f;

        float emptyThreshold, criticalThreshold, warningThreshold;
        switch (ship.getHullSize()) {
            case FRIGATE:
            case FIGHTER:
                emptyThreshold = EMPTY_THRESHOLD_FRIGATE * scale;
                criticalThreshold = CRITICAL_THRESHOLD_FRIGATE * scale;
                warningThreshold = WARNING_THRESHOLD_FRIGATE * scale;
                break;
            case DESTROYER:
                emptyThreshold = EMPTY_THRESHOLD_DESTROYER * scale;
                criticalThreshold = CRITICAL_THRESHOLD_DESTROYER * scale;
                warningThreshold = WARNING_THRESHOLD_DESTROYER * scale;
                break;
            case CRUISER:
                emptyThreshold = EMPTY_THRESHOLD_CRUISER * scale;
                criticalThreshold = CRITICAL_THRESHOLD_CRUISER * scale;
                warningThreshold = WARNING_THRESHOLD_CRUISER * scale;
                break;
            case CAPITAL_SHIP:
            default:
                emptyThreshold = EMPTY_THRESHOLD_CAPITAL * scale;
                criticalThreshold = CRITICAL_THRESHOLD_CAPITAL * scale;
                warningThreshold = WARNING_THRESHOLD_CAPITAL * scale;
                break;
        }

        float damageReduction;
        if (ship.getFluxTracker().getFluxLevel() >= emptyThreshold || ship.getFluxTracker().isOverloadedOrVenting()
                || !ship.isAlive() || priwenVulnerable
                || (ship.getCurrentCR() <= 0f && !ship.isFighter())) {
            damageReduction = 0f;
        } else {
            switch (ship.getHullSize()) {
                case FRIGATE:
                case FIGHTER:
                    damageReduction = (DAMAGE_REDUCTION_FULL_FRIGATE * scale)
                            - (ship.getFluxTracker().getFluxLevel() * ((DAMAGE_REDUCTION_FULL_FRIGATE * scale)
                            - (DAMAGE_REDUCTION_CRITICAL_FRIGATE * scale))
                            / emptyThreshold);
                    break;
                case DESTROYER:
                    damageReduction = (DAMAGE_REDUCTION_FULL_DESTROYER * scale)
                            - (ship.getFluxTracker().getFluxLevel() * ((DAMAGE_REDUCTION_FULL_DESTROYER * scale)
                            - (DAMAGE_REDUCTION_CRITICAL_DESTROYER * scale))
                            / emptyThreshold);
                    break;
                case CRUISER:
                    damageReduction = (DAMAGE_REDUCTION_FULL_CRUISER * scale)
                            - (ship.getFluxTracker().getFluxLevel() * ((DAMAGE_REDUCTION_FULL_CRUISER * scale)
                            - (DAMAGE_REDUCTION_CRITICAL_CRUISER * scale))
                            / emptyThreshold);
                    break;
                case CAPITAL_SHIP:
                default:
                    damageReduction = (DAMAGE_REDUCTION_FULL_CAPITAL * scale)
                            - (ship.getFluxTracker().getFluxLevel() * ((DAMAGE_REDUCTION_FULL_CAPITAL * scale)
                            - (DAMAGE_REDUCTION_CRITICAL_CAPITAL * scale))
                            / emptyThreshold);
                    break;
            }
        }

        if (aegis > 0f) {
            switch (ship.getHullSize()) {
                case FRIGATE:
                case FIGHTER:
                    damageReduction = (1f - aegis) * damageReduction + aegis
                            * (1f - ((1f - (DAMAGE_REDUCTION_FULL_FRIGATE * scale)) * 0.5f));
                    break;
                case DESTROYER:
                    damageReduction = (1f - aegis) * damageReduction + aegis
                            * (1f - ((1f - (DAMAGE_REDUCTION_FULL_DESTROYER * scale)) * 0.5f));
                    break;
                case CRUISER:
                    damageReduction = (1f - aegis) * damageReduction + aegis
                            * (1f - ((1f - (DAMAGE_REDUCTION_FULL_CRUISER * scale)) * 0.5f));
                    break;
                case CAPITAL_SHIP:
                default:
                    damageReduction = (1f - aegis) * damageReduction + aegis
                            * (1f - ((1f - (DAMAGE_REDUCTION_FULL_CAPITAL * scale)) * 0.5f));
                    break;
            }
        }

        if (shipHealth.containsKey(ship)) {
            HealthState healthState = shipHealth.get(ship);
            float[][] armorArray = armorArray(ship);

            boolean shieldsWentOffline = ship.getFluxTracker().getFluxLevel() >= emptyThreshold
                    && healthState.damageReduction > 0f;

            if (healthState.offlineTimer >= 0f && aegis <= 0f) {
                damageReduction = 0f;
                healthState.offlineTimer -= amount;
            }

            boolean shieldsWentOnline = healthState.damageReduction <= 0f && damageReduction > 0f;

            if (healthState.onlineTimer >= 0f) {
                healthState.onlineTimer -= amount;
            }

            boolean shieldsWentRed = ship.getFluxTracker().getFluxLevel() >= criticalThreshold
                    && healthState.previousFlux / ship.getFluxTracker().getMaxFlux() < criticalThreshold
                    && damageReduction > 0f;

            if (healthState.warningTimer >= 0f) {
                healthState.warningTimer -= amount;
            }

            float lastFrameHull = healthState.hull;
            float lastFrameArmor = armorLevel(healthState.armor);
            float thisFrameHull = ship.getHitpoints();
            float thisFrameArmor = armorLevel(armorArray);

            float damage = lastFrameHull - thisFrameHull;
            damage += (lastFrameArmor - thisFrameArmor) / (1f - damageReduction);
            damage = damage / (1f - damageReduction) - damage;

            WeaponAPI shield = null;
            List<WeaponAPI> weapons = ship.getAllWeapons();
            for (WeaponAPI weapon : weapons) {
                if (weapon.getId().endsWith("_shield") && weapon.getId().startsWith("tem_")) {
                    shield = weapon;
                }
            }

            float minimum = 0.3f + (float) FastTrig.sin(Global.getCombatEngine().getTotalElapsedTime(false) * 0.5f
                    * Math.PI) * 0.05f;
            if (damage > 0f) {
                if (ship.getHullLevel() < ship.getMutableStats().getMaxCombatHullRepairFraction().getModifiedValue()) {
                    float repaired
                            = (ship.getMutableStats().getHullCombatRepairRatePercentPerSecond().getModifiedValue() / 100f)
                            * amount
                            * ship.getMaxHitpoints();
                    damage = repaired + lastFrameHull - thisFrameHull;
                    damage += (lastFrameArmor - thisFrameArmor) / (1f - damageReduction);
                    damage = damage / (1f - damageReduction) - damage;
                }

                float fluxPerDamage;
                switch (ship.getHullSize()) {
                    case FRIGATE:
                    case FIGHTER:
                        fluxPerDamage = computeStat(ship.getMutableStats().getShieldDamageTakenMult(),
                                FLUX_PER_DAMAGE_FRIGATE / scale);
                        break;
                    case DESTROYER:
                        fluxPerDamage = computeStat(ship.getMutableStats().getShieldDamageTakenMult(),
                                FLUX_PER_DAMAGE_DESTROYER / scale);
                        break;
                    case CRUISER:
                        fluxPerDamage = computeStat(ship.getMutableStats().getShieldDamageTakenMult(),
                                FLUX_PER_DAMAGE_CRUISER / scale);
                        break;
                    case CAPITAL_SHIP:
                    default:
                        fluxPerDamage = computeStat(ship.getMutableStats().getShieldDamageTakenMult(),
                                FLUX_PER_DAMAGE_CAPITAL / scale);
                        break;
                }
                ship.getFluxTracker().increaseFlux((1f - aegis * 0.9f) * damage * fluxPerDamage, true);
                Vector2f upperLoc = MathUtils.getPointOnCircumference(ship.getLocation(), ship.getCollisionRadius(),
                        ship.getFacing());
                Global.getCombatEngine().addFloatingDamageText(upperLoc, (1f - aegis * 0.9f) * damage * fluxPerDamage,
                        VISUAL_SHIELD_COLOR, ship, ship);
                healthState.glow += (float) Math.sqrt((1f - aegis * 0.5f) * damage / 10f)
                        / (ship.getFluxTracker().getMaxFlux() * 0.001f);
            }

            if (damageReduction > 0f) {
                if (healthState.glow < minimum) {
                    healthState.glow = Math.min(healthState.glow + amount * 2f, minimum);
                } else {
                    healthState.glow = Math.max(healthState.glow - amount * Math.max(healthState.glow, 1f) * 0.75f,
                            minimum);
                }
            } else {
                healthState.glow = Math.max(healthState.glow - amount * 100f, 0f);
            }

            if (damageReduction > 0f && aegis <= 0f) {
                float skillBonus = computeStat(ship.getMutableStats().getHardFluxDissipationFraction(), 1f);
                float fluxIncrease = ship.getMutableStats().getFluxDissipation().getModifiedValue();
                switch (ship.getHullSize()) {
                    case FRIGATE:
                    case FIGHTER:
                        fluxIncrease *= (1f - HARD_FLUX_DISSIPATION_RATE_FRIGATE * scale * skillBonus) * amount;
                        break;
                    case DESTROYER:
                        fluxIncrease *= (1f - HARD_FLUX_DISSIPATION_RATE_DESTROYER * scale * skillBonus) * amount;
                        break;
                    case CRUISER:
                        fluxIncrease *= (1f - HARD_FLUX_DISSIPATION_RATE_CRUISER * scale * skillBonus) * amount;
                        break;
                    case CAPITAL_SHIP:
                    default:
                        fluxIncrease *= (1f - HARD_FLUX_DISSIPATION_RATE_CAPITAL * scale * skillBonus) * amount;
                        break;
                }
                if (fluxIncrease + ship.getFluxTracker().getHardFlux() > healthState.previousHardFlux) {
                    fluxIncrease = Math.min(Math.max(healthState.previousHardFlux - ship.getFluxTracker().getHardFlux(),
                            0f), fluxIncrease);
                }
                if (ship.getFluxTracker().getHardFlux() + fluxIncrease > ship.getFluxTracker().getMaxFlux()
                        * emptyThreshold) {
                    ship.getFluxTracker().setHardFlux(ship.getFluxTracker().getMaxFlux() * emptyThreshold);
                } else {
                    ship.getFluxTracker().increaseFlux(fluxIncrease, true);
                }
            }

            if (shield != null && shield.getAnimation() != null) {
                if (healthState.glow > 0f) {
                    shield.getAnimation().setFrame(1);
                } else {
                    shield.getAnimation().setFrame(0);
                }
                shield.getAnimation().setAlphaMult(Math.min(healthState.glow, 1f));
                shield.getSprite().setAdditiveBlend();
                if (aegis > 0f) {
                    shield.getSprite().setColor(AEGIS_SHIELD_COLOR);
                } else {
                    if (ship.getFluxTracker().getFluxLevel() > criticalThreshold) {
                        shield.getSprite().setColor(CRITICAL_SHIELD_COLOR);
                    } else if (ship.getFluxTracker().getFluxLevel() > warningThreshold) {
                        shield.getSprite().setColor(FastTrig.sin(Global.getCombatEngine().getTotalElapsedTime(false)
                                * 4f * Math.PI) > 0f
                                        ? CRITICAL_SHIELD_COLOR : VISUAL_SHIELD_COLOR);
                    } else {
                        shield.getSprite().setColor(VISUAL_SHIELD_COLOR);
                    }
                }
            }

            if (shieldsWentOffline) {
                if (healthState.offlineTimer < 0f) {
                    Global.getSoundPlayer().playSound("tem_latticeshield_down", pitchBend.get(ship.getHullSize()),
                            volume.get(ship.getHullSize()),
                            ship.getLocation(), ship.getVelocity());
                    healthState.offlineTimer = priwenVulnerabilityTime.get(ship.getHullSize()) + 1f;
                    for (int i = 0; i <= ship.getCollisionRadius() * 0.03f; i++) {
                        Vector2f point1 = MathUtils.getRandomPointInCircle(ship.getLocation(),
                                shipRadius
                                * ((float) Math.random() + 1f));
                        Vector2f point2
                                = MathUtils.getRandomPointInCircle(ship.getLocation(), ship.getCollisionRadius());
                        int bound = 100;
                        while (bound > 0) {
                            bound--;
                            point2 = MathUtils.getRandomPointInCircle(ship.getLocation(), ship.getCollisionRadius());
                            if (CollisionUtils.isPointWithinBounds(point2, ship)) {
                                break;
                            }
                        }
                        Global.getCombatEngine().spawnEmpArc(ship, point2, new SimpleEntity(point2), new SimpleEntity(
                                point1), DamageType.ENERGY, 0f, 0f, 1000f,
                                null, 30f, VISUAL_SHIELD_COLOR, VISUAL_SHIELD_COLOR);
                    }
                    for (int i = 0; i <= (int) (ship.getCollisionRadius()); i++) {
                        float radius = 0.5f * shipRadius * ((float) Math.random() * 0.5f + 0.5f);
                        Vector2f direction = MathUtils.getRandomPointOnCircumference(null, shipRadius
                                * ((float) Math.random() * 1f + 0.5f));
                        Vector2f point = MathUtils.getPointOnCircumference(ship.getLocation(), radius,
                                VectorUtils.getFacing(direction));
                        Vector2f.add(ship.getVelocity(), direction, direction);
                        Global.getCombatEngine().addSmoothParticle(point, direction, 10f, 1f, 1f / pitchBend.get(
                                ship.getHullSize()), VISUAL_SHIELD_COLOR);
                    }
                }
            }

            if (shieldsWentOnline) {
                if (healthState.onlineTimer < 0f) {
                    healthState.onlineTimer = 3f;
                    Global.getSoundPlayer().playSound("tem_latticeshield_up", pitchBend.get(ship.getHullSize()),
                            volume.get(ship.getHullSize()),
                            ship.getLocation(), ship.getVelocity());
                }
            }

            if (shieldsWentRed) {
                if (healthState.warningTimer < 0f) {
                    healthState.warningTimer = 3f;
                    Global.getSoundPlayer().playSound("tem_latticeshield_warning", pitchBend.get(ship.getHullSize()),
                            volume.get(ship.getHullSize()),
                            ship.getLocation(), ship.getVelocity());
                }
            }

            healthState.armor = armorArray;
            healthState.hull = thisFrameHull;
            healthState.previousHardFlux = ship.getFluxTracker().getHardFlux();
            healthState.previousFlux = ship.getFluxTracker().getCurrFlux();
            healthState.damageReduction = damageReduction;
        } else {
            shipHealth.put(ship,
                    new HealthState(ship.getHitpoints(), armorArray(ship), ship.getFluxTracker().getHardFlux(),
                            ship.getFluxTracker().getCurrFlux(),
                            damageReduction));
        }

        if (ship == Global.getCombatEngine().getPlayerShip()) {
            if (!uiKey.containsKey(ship)) {
                Object[] array = new Object[2];
                for (int i = 0; i < array.length; i++) {
                    array[i] = new Object();
                }
                uiKey.put(ship, array);
            }
        }

        if (damageReduction > 0f) {
            if (ship == Global.getCombatEngine().getPlayerShip()) {
                Global.getCombatEngine().maintainStatusForPlayerShip(uiKey.get(ship)[0],
                        "graphics/icons/hullsys/fortress_shield.png",
                        "Lattice Shield Matrix", "" + Math.round(
                                damageReduction * 100f)
                        + "% damage absorption", false);
                if (ship.getFluxTracker().getFluxLevel() > warningThreshold) {
                    float integrity = 1f - (ship.getFluxTracker().getFluxLevel() / emptyThreshold);
                    Global.getCombatEngine().maintainStatusForPlayerShip(uiKey.get(ship)[1],
                            "graphics/icons/hullsys/fortress_shield.png",
                            "Lattice Shield Matrix", "" + Math.round(
                                    integrity * 100f)
                            + "% shield integrity", true);
                }
            }

            float armorLevel = armorLevelFrac(ship);
            damageReduction = 1f / (1f - damageReduction) * computeStat(ship.getMutableStats().getShieldAbsorptionMult(), 1f);
            damageReduction = 1f / damageReduction;
            ship.getMutableStats().getHighExplosiveDamageTakenMult().modifyMult(id, damageReduction / (1f + armorLevel * 2f));
            ship.getMutableStats().getKineticDamageTakenMult().modifyMult(id, damageReduction * (1f + armorLevel * 1.5f));
            ship.getMutableStats().getEnergyDamageTakenMult().modifyMult(id, damageReduction);
            ship.getMutableStats().getFragmentationDamageTakenMult().modifyMult(id, damageReduction);
            ship.getMutableStats().getEmpDamageTakenMult().modifyMult(id, damageReduction);
            ship.getMutableStats().getBeamDamageTakenMult().modifyMult(id, 0.9f * (float) Math.sqrt(damageReduction));
        } else {
            if (ship == Global.getCombatEngine().getPlayerShip()) {
                Global.getCombatEngine().maintainStatusForPlayerShip(uiKey.get(ship)[1],
                        "graphics/icons/hullsys/fortress_shield.png",
                        "Lattice Shield Matrix", "Shields offline", true);
            }

            ship.getMutableStats().getHighExplosiveDamageTakenMult().unmodify(id);
            ship.getMutableStats().getKineticDamageTakenMult().unmodify(id);
            ship.getMutableStats().getEnergyDamageTakenMult().unmodify(id);
            ship.getMutableStats().getFragmentationDamageTakenMult().unmodify(id);
            ship.getMutableStats().getEmpDamageTakenMult().unmodify(id);
            ship.getMutableStats().getBeamDamageTakenMult().unmodify(id);
        }
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getNonBuiltInHullmods().contains(tmp) && !ship.getVariant().getSMods().contains(tmp)) {
                ship.getVariant().removeMod(tmp);
                TEM_BlockedHullmodDisplayScript.showBlocked(ship);
            }
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        float damageReduction;
        switch (hullSize) {
            case FRIGATE:
            case FIGHTER:
                damageReduction = (EMPTY_THRESHOLD_FRIGATE / 2f) * (DAMAGE_REDUCTION_FULL_FRIGATE
                        + DAMAGE_REDUCTION_CRITICAL_FRIGATE);
                break;
            case DESTROYER:
                damageReduction = (EMPTY_THRESHOLD_DESTROYER / 2f) * (DAMAGE_REDUCTION_FULL_DESTROYER
                        + DAMAGE_REDUCTION_CRITICAL_DESTROYER);
                break;
            case CRUISER:
                damageReduction = (EMPTY_THRESHOLD_CRUISER / 2f) * (DAMAGE_REDUCTION_FULL_CRUISER
                        + DAMAGE_REDUCTION_CRITICAL_CRUISER);
                break;
            case CAPITAL_SHIP:
            default:
                damageReduction = (EMPTY_THRESHOLD_CAPITAL / 2f) * (DAMAGE_REDUCTION_FULL_CAPITAL
                        + DAMAGE_REDUCTION_CRITICAL_CAPITAL);
                break;
        }
        stats.getHighExplosiveDamageTakenMult().modifyMult(TEM_LatticeShield.id, (1f - damageReduction) / 3f);
        stats.getKineticDamageTakenMult().modifyMult(TEM_LatticeShield.id, (1f - damageReduction) * 2.5f);
        stats.getEnergyDamageTakenMult().modifyMult(TEM_LatticeShield.id, 1f - damageReduction);
        stats.getFragmentationDamageTakenMult().modifyMult(TEM_LatticeShield.id, 1f - damageReduction);
        stats.getEmpDamageTakenMult().modifyMult(TEM_LatticeShield.id, 1f - damageReduction);
        stats.getBeamDamageTakenMult().modifyMult(TEM_LatticeShield.id, 0.9f * (float) Math.sqrt(1f - damageReduction));
        stats.getDynamic().getStat(Stats.CORONA_EFFECT_MULT).modifyMult(id, CORONA_EFFECT_REDUCTION);
        stats.getDynamic().getStat(Stats.SHIELD_PIERCED_MULT).modifyMult(id, PIERCE_MULT);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize, ShipAPI ship) {
        float scale = getDModScale(ship);
        switch (index) {
            case 0:
                switch (ship.getHullSize()) {
                    case FRIGATE:
                    case FIGHTER:
                        return "" + Math.round(DAMAGE_REDUCTION_FULL_FRIGATE * 100f * scale) + "%";
                    case DESTROYER:
                        return "" + Math.round(DAMAGE_REDUCTION_FULL_DESTROYER * 100f * scale) + "%";
                    case CRUISER:
                        return "" + Math.round(DAMAGE_REDUCTION_FULL_CRUISER * 100f * scale) + "%";
                    case CAPITAL_SHIP:
                    default:
                        return "" + Math.round(DAMAGE_REDUCTION_FULL_CAPITAL * 100f * scale) + "%";
                }
            case 1:
                switch (ship.getHullSize()) {
                    case FRIGATE:
                    case FIGHTER:
                        return "" + Math.round(DAMAGE_REDUCTION_CRITICAL_FRIGATE * 100f * scale) + "%";
                    case DESTROYER:
                        return "" + Math.round(DAMAGE_REDUCTION_CRITICAL_DESTROYER * 100f * scale) + "%";
                    case CRUISER:
                        return "" + Math.round(DAMAGE_REDUCTION_CRITICAL_CRUISER * 100f * scale) + "%";
                    case CAPITAL_SHIP:
                    default:
                        return "" + Math.round(DAMAGE_REDUCTION_CRITICAL_CAPITAL * 100f * scale) + "%";
                }
            case 2:
                switch (ship.getHullSize()) {
                    case FRIGATE:
                    case FIGHTER:
                        return "" + Math.round(EMPTY_THRESHOLD_FRIGATE * 100f * scale) + "%";
                    case DESTROYER:
                        return "" + Math.round(EMPTY_THRESHOLD_DESTROYER * 100f * scale) + "%";
                    case CRUISER:
                        return "" + Math.round(EMPTY_THRESHOLD_CRUISER * 100f * scale) + "%";
                    case CAPITAL_SHIP:
                    default:
                        return "" + Math.round(EMPTY_THRESHOLD_CAPITAL * 100f * scale) + "%";
                }
            case 3:
                switch (ship.getHullSize()) {
                    case FRIGATE:
                    case FIGHTER:
                        return String.format("%.2f", FLUX_PER_DAMAGE_FRIGATE / scale);
                    case DESTROYER:
                        return String.format("%.2f", FLUX_PER_DAMAGE_DESTROYER / scale);
                    case CRUISER:
                        return String.format("%.2f", FLUX_PER_DAMAGE_CRUISER / scale);
                    case CAPITAL_SHIP:
                    default:
                        return String.format("%.2f", FLUX_PER_DAMAGE_CAPITAL / scale);
                }
            case 4:
                switch (ship.getHullSize()) {
                    case FRIGATE:
                    case FIGHTER:
                        return "" + Math.round(HARD_FLUX_DISSIPATION_RATE_FRIGATE * 100f * scale) + "%";
                    case DESTROYER:
                        return "" + Math.round(HARD_FLUX_DISSIPATION_RATE_DESTROYER * 100f * scale) + "%";
                    case CRUISER:
                        return "" + Math.round(HARD_FLUX_DISSIPATION_RATE_CRUISER * 100f * scale) + "%";
                    case CAPITAL_SHIP:
                    default:
                        return "" + Math.round(HARD_FLUX_DISSIPATION_RATE_CAPITAL * 100f * scale) + "%";
                }
            case 5: {
                int dmods = DModManager.getNumDMods(ship.getVariant());
                if (dmods == 0) {
                    return "";
                } else {
                    return "\n\n";
                }
            }
            case 6: {
                int dmods = DModManager.getNumDMods(ship.getVariant());
                if (dmods == 0) {
                    return "";
                } else {
                    return "Lasting damage reduces shield performance by " + Math.round((1f - scale) * 100f) + "%!";
                }
            }
            default:
                return null;
        }
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && ship.getHullSpec().getHullId().startsWith("tem_");
    }

    private static final class HealthState {

        float armor[][];
        float damageReduction;
        float glow = 0f;
        float hull;
        float offlineTimer = -1f;
        float onlineTimer = -1f;
        float previousFlux;
        float previousHardFlux;
        float warningTimer = -1f;

        private HealthState(float hull, float armor[][], float previousHardFlux, float previousFlux,
                float damageReduction) {
            this.hull = hull;
            this.armor = armor;
            this.previousHardFlux = previousHardFlux;
            this.previousFlux = previousFlux;
            this.damageReduction = damageReduction;
        }
    }

    private static final class LocalData {

        final Map<ShipAPI, Object[]> uiKey = new HashMap<>(50);
    }
}
