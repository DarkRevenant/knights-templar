{
    "id":"templars",
    "color":[0,255,255,255],

    "baseUIColor":[0,195,195,255],
    "darkUIColor":[95,15,10,175],
    "gridUIColor":[15,75,140,75],
    "brightUIColor":[100,220,220,255],
    "secondaryUIColor":[210,0,0,255],
    "secondarySegments":4,

    "displayName":"Knights Templar",
    "displayNameWithArticle":"the Knights Templar",
    "displayNameLong":"Knights Templar",
    "displayNameLongWithArticle":"the Knights Templar",
    "displayNameIsOrAre":"are",
    "logo":"graphics/templars/factions/knights_templar.png",
    "crest":"graphics/templars/factions/crest_knights_templar.png",
    "shipNamePrefix":"",
    "shipNameSources":{
        "TEMPLARS":2,
        "KNIGHTS":2,
        "ABRAHAMIC":1,
        "ROMAN":1
    },
    "names":{
        "templars":3,
        "old english":1,
        "luddic":1,
    },
    # variantOverrides restricts hulls to listed variants and adjusts their probability
    "variantOverrides":{
    },
    # multiplier for how often hulls show up in the faction's fleets
    "hullFrequency":{
        "tags":{
        },
        "hulls":{
        },
    },
    # ships the faction gets access to when importing S&W out-of-faction
    "shipsWhenImporting":{
        "tags":[],
        "hulls":[
        ],
    },
    "knownShips":{
        "tags":["templar"],
        "hulls":[
            "tem_jesuit",
            "tem_martyr",

            "tem_crusader",

            "tem_chevalier",
            "tem_paladin",

            "tem_archbishop",
        ],
    },
    # listing ships here will make the faction mostly use them even if other hulls become available
    "priorityShips":{
        "tags":[],
        "hulls":[
            "tem_archbishop", # Carrier, needs to appear even though there's no carriers in doctrine
        ],
    },
    "knownFighters":{
        "tags":["templar"],
        "fighters":[
            "tem_teuton_assault_wing",
            "tem_teuton_bomber_wing",
            "tem_teuton_fighter_wing",
            "tem_teuton_support_wing",
        ],
    },
    "priorityFighters":{
        "tags":[],
        "fighters":[
        ],
    },
    "knownWeapons":{
        "tags":["templar"],
        "weapons":[
            "tem_secace",

            "tem_galatine",

            "tem_arondight",

            "tem_clarent_single",
            "tem_clarent",
            "tem_crucifix",

            "tem_clarent_tube",
            "tem_crucifix_array",
            "tem_trucidare",

            "tem_roland",

            "tem_rhon",
            "tem_merced",
            "tem_pax",

            "tem_longinus",
            "tem_sentenia",

            "tem_joyeuse",
            "tem_juger",
        ],
    },
    "priorityWeapons":{
        "tags":[],
        "weapons":[
        ],
    },
    "knownHullMods":{
        "tags":["standard", "templar"],
        "hullMods":[
        ],
    },
    "factionDoctrine":{
        "warships":5,
        "carriers":0,
        "phaseShips":0,

        "officerQuality":5,
        "shipQuality":5,
        "numShips":5,

        "shipSize":4,

        "aggression":4,

        "combatFreighterProbability":0,                   # instead of some portion of the freighters in a fleet
        "combatFreighterCombatUseFraction":0,             # as part of the normal combat lineup
        "combatFreighterCombatUseFractionWhenPriority":0, # as part of normal combat lineup, when marked as priority ship
        "autofitRandomizeProbability":0.1,

        "commanderSkillsShuffleProbability":0.25,
        "commanderSkills":[
            "officer_training",
            "flux_regulation",
            "coordinated_maneuvers",
            "electronic_warfare",
        ],
    },
    "internalComms":"templars_internal",
    "illegalCommodities":[
        "organs",
        "luxury_goods",
        "lobster",
        "tem_fluxcore",
        "ai_cores",
    ],
    "ranks":{
        "ranks":{
            "spaceSailor":{"name":"Page"},
            "spaceChief":{"name":"Chief"},
            "spaceEnsign":{"name":"Squire"},
            "spaceLieutenant":{"name":"Knight"},
            "spaceCommander":{"name":"Knight-Commander"},
            "spaceCaptain":{"name":"Bishop-at-Arms"},
            "spaceAdmiral":{"name":"Archbishop of War"},
            "spaceMarshal":{"name":"High Templar"},
            "factionLeader":{"name":"Pope"},

            "citizen":{"name":"Faithful"},
        },
        "posts":{
            "factionLeader":{"name":"Pope"},
            "patrolCommander":{"name":"Deacon"},
            "fleetCommander":{"name":"Bishop"},
            "baseCommander":{"name":"Archbishop"},
            "stationCommander":{"name":"Templar"},
            "outpostCommander":{"name":"Priest-Commander"},
            "portmaster":{"name":"Warden"},

            "supplyOfficer":{"name":"Master of Material"},
            "supplyManager":{"name":"Shepherd"},
            "medicalSupplier":{"name":"Healer"},

            "citizen":{"name":"Faithful"},
        },
    },
    "voices":{
        "LOW":{
            "soldier":10,
        },
        "MEDIUM":{
            "soldier":5,
            "official":10,
        },
        "HIGH":{
            "soldier":5,
            "official":5,
            "aristo":10,
        },
    },
    "custom":{
        "exemptFromFoodShortages":true,
        "ignoreTradeWithEnemiesForReputation":true,
        "postsNoBounties":true,
        "allowsTransponderOffTrade":false,
        "engageWhenEvenStrength":true,
        "offerMissionsWhenHostile":false,
        "offersCommissions":true,
        "buysAICores":false,
        "fightToTheLast":true, # order a full assault instead of retreating, in most cases
        "punitiveExpeditionData":{
            "vsCompetitors":false,
            "vsFreePort":false,
            "canBombard":true,
            "territorial":true,
            "canSendWithoutMilitaryBase":true,
        },
    },
    "fleetTypeNames":{
        "patrolSmall":"Patrouille",
        "patrolMedium":"Crux",
        "patrolLarge":"Palatinus",
    },
    "portraits":{
        "standard_male":[
            "graphics/templars/portraits/tem_portrait1.png",
            "graphics/templars/portraits/tem_portrait1b.png",
            "graphics/templars/portraits/tem_portrait2.png",
            "graphics/templars/portraits/tem_portrait2.png",
            "graphics/templars/portraits/tem_portrait2b.png",
            "graphics/templars/portraits/tem_portrait2b.png",
        ],
        "standard_female":[
            "graphics/templars/portraits/tem_portrait1.png",
            "graphics/templars/portraits/tem_portrait1b.png",
        ],
    },
    "music":{
        "theme":"tem_music_templars_encounter_hostile",
        "market_neutral":"tem_music_templars_market_neutral",
        "market_hostile":"tem_music_templars_market_hostile",
        "market_friendly":"tem_music_templars_market_neutral",
        "encounter_neutral":"tem_music_templars_encounter_hostile",
        "encounter_hostile":"tem_music_templars_encounter_hostile",
        "encounter_friendly":"tem_music_templars_encounter_hostile",
    },
}