Location: Orbiting Jackson V
Date: 149.2.4

Following your rebirth into The Knights Templar two dozens of terran cycles ago, you took the name Roland, in remembrance of the legendary paladin of old. More recently - just last night, in fact - you joined the Sacred Order of Paladins and earned the right to command the Durandal, your homeship.

The reason you have become a Paladin is that there must always be Twelve; when your old superior was killed in battle two nights ago, you were chosen to replace him. Of course, the reason that he was killed is the same reason that your fleet is destroyed and that you now face an entire Hegemony System Defense Fleet on your own.

As you don your armor and take the helm, you are hailed by your opponent, told that there is no chance of escape, that the Hegemony has you completely surrounded. You laugh and cut the communcation device. You smile at the vidscreen, basking in the radiance of the Divine Gift you now pilot, knowing that the only thing you will be escaping from is a sea of the corpses of heathens.