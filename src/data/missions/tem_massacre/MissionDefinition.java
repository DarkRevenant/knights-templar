package data.missions.tem_massacre;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    @SuppressWarnings("SuspiciousNameCombination")
    public void defineMission(MissionDefinitionAPI api) {
        api.initFleet(FleetSide.PLAYER, "", FleetGoal.ATTACK, false, 0);
        api.initFleet(FleetSide.ENEMY, "TTS", FleetGoal.ATTACK, true, 7);

        api.setFleetTagline(FleetSide.PLAYER, "Templar seeker unit");
        api.setFleetTagline(FleetSide.ENEMY, "Tri-Tachyon task force");

        api.addBriefingItem("Defeat all enemy forces");

        api.addToFleet(FleetSide.PLAYER, "tem_jesuit_est", FleetMemberType.SHIP, "Gawain", true);
        api.addToFleet(FleetSide.PLAYER, "tem_jesuit_def", FleetMemberType.SHIP, "Galahad", false);

        api.addToFleet(FleetSide.ENEMY, "astral_Elite", FleetMemberType.SHIP, "TTS Ephemeral", true);
        api.addToFleet(FleetSide.ENEMY, "medusa_PD", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "omen_PD", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "wolf_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "wolf_CS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "tempest_Attack", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "tempest_Attack", FleetMemberType.SHIP, false);

        float width = 24000f;
        float height = 18000f;
        api.initMap(-width / 2f, width / 2f, -height / 2f, height / 2f);

        float minX = -width / 2;
        float minY = -height / 2;

        for (int i = 0; i < 25; i++) {
            float x = (float) Math.random() * width - width / 2;
            float y = (float) Math.random() * height - height / 2;
            float radius = 1000f + (float) Math.random() * 1000f;
            api.addNebula(x, y, radius);
        }

        api.addNebula(minX + width * 0.8f - 2000, minY + height * 0.4f, 2000);
        api.addNebula(minX + width * 0.8f - 2000, minY + height * 0.5f, 2000);
        api.addNebula(minX + width * 0.8f - 2000, minY + height * 0.6f, 2000);

        api.addObjective(minX + width * 0.15f + 3000, minY + height * 0.2f + 1000, "nav_buoy");
        api.addObjective(minX + width * 0.8f - 2000, minY + height * 0.2f + 1000, "comm_relay");

        api.addObjective(minX + width * 0.85f - 3000, minY + height * 0.8f - 1000, "sensor_array");
        api.addObjective(minX + width * 0.2f + 2000, minY + height * 0.8f - 1000, "sensor_array");

        api.addAsteroidField(minX, minY + height * 0.5f, 0, height, 20f, 70f, 50);

        api.addPlanet(0, 0, 350f, "barren", 200f, true);
    }
}
