package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableStat;
import com.fs.starfarer.api.combat.ShipAPI;

public class TEM_LatticeShield extends BaseHullMod {

    public static float computeStat(MutableStat stat, float base) {
        return data.scripts.hullmods.TEM_LatticeShield.computeStat(stat, base);
    }

    public static float shieldLevel(ShipAPI ship) {
        return data.scripts.hullmods.TEM_LatticeShield.shieldLevel(ship);
    }
}
