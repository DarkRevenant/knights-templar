package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.ShipAPI;

public class TEM_ExcaliburDrive extends BaseHullMod {

    public static boolean shipIsInBurst(ShipAPI ship) {
        return data.scripts.hullmods.TEM_ExcaliburDrive.shipIsInBurst(ship);
    }
}
